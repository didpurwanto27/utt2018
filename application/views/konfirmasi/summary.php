<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Konfirmasi Pembayaran</title>
    <link href="<?php echo base_url("public/template")?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url("public/template")?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" src="<?php echo base_url('public')?>/validation/gen_validatorv4.js" type="text/javascript" xml:space="preserve">
    </script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" align="center">
                        <h3> PENGISIAN DATA SUKSES !! </b>
                        </h3>
                        <h5><b>Cek Data Anda, Pastikan Sudah Benar Semua. Jika Ada yang salah, silahkan konfirmasi ulang dan beri pesan di kolom "Pesan Kepada Bendahara"</b>
                        </h5>
                    </div>
                    <div class="panel-body" align="left">
                    
					<?php $this->load->view($isi); ?>  <!--LOAD PTG-->
					
					
					</div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url("public/")?>js/jquery-1.11.0.js"></script>
    <script src="<?php echo base_url("public/")?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url("public/")?>js/sb-admin-2.js"></script>
</body>
</html>

