<?php

class Pembayaran extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->model('momodel','modelp');	
		$this->load->library('excel');		
		
	}

	function checkSession(){
	    if(!$this->session->userdata('email'))
	    {
	    	redirect(base_url(),'refresh');
	   	}
	}
	
	function login_action(){
		$email = $this->input->post('email');
		$passwd = $this->input->post('passwd');
		
		$previlage = $this->modelp->cekData($email, md5($passwd));
		if($previlage==true){
			$this->session->set_userdata('email', $email);
			redirect('pages/home');
		}
		else{
			redirect(base_url(),'refresh');
		}
	}

	function homebayar(){
		$this->checkSession();
		if($this->session->userdata('email')){	
			$data['result'] = $this->modelp->selectPembayaran();
			foreach ($data['result'] as $rows) {
				$data['bayar_id']	 = $rows['bayar_id'];
				$data['semester_bayar']	 = $rows['semester_bayar'];
				$data['nim']	 = $rows['nim'];
				$data['nama']	 = $rows['nama'];
				$data['jurusan']	 = $rows['jurusan'];
				$data['semester']	 = $rows['semester'];
				$data['metode']	 = $rows['metode'];
				$data['jumlah']	 = $rows['jumlah'];
				$data['status']	 = $rows['status'];
			}
			$data['isi'] = "pembayaran/pembayaran";
			$this->load->view('top2');
			$this->load->view('sidebar2', $data);			
		}	
	}

	
	function konfirmasi_bayar(){
		$this->load->view('konfirmasi/konf');
	}

	function konf_page(){
		

		$data['result'] = $this->modelp->cekAngkatan();
		foreach($data['result'] as $rows)
		{
			$data['ANGKATAN_MHS'] 	= $rows['ANGKATAN_MHS'];
		}

		//$data['isi'] = "konfirmasi/show_konf";				
		$this->load->view('konfirmasi/konf_page', $data);	

	}
	function show_konf(){
		$this->load->view('konfirmasi/show_konf');
	}
	
	function insert_data_konfirmasi(){
		$folderbayar = './data_pembayaran/';
		$config['upload_path'] = $folderbayar;
		$config['allowed_types'] = 'png|jpg|jpeg';
		$config['max_size']	= '20480';
		$this->load->library('upload', $config);
		if ($this->upload->do_upload())
		{
			$upload_data = $this->upload->data();
			$semester_bayar = '20191'; // change the semester
			$nama = $this->input->post('nama');
			$nim = $this->input->post('nim');
			$jurusan = $this->input->post('jurusan');
			$semester = $this->input->post('semester');
			$metode = $this->input->post('metode');
			$jumlah = $this->input->post('jumlah');
			$atasnama = $this->input->post('atasnama');
			$tgltransfer = $this->input->post('tgltransfer');
			
			$temp_path_file=$upload_data['full_path'];	
			$path_image = basename($temp_path_file);
			//echo $path_image;
			//$path_image = $folderbayar.$path_image1;
			$uk = $this->input->post('uk');
			$uo = $this->input->post('uo');
			$sksujul = $this->input->post('sksujul');
			$semestercuti = $this->input->post('semestercuti');
			$modul_sem1 = $this->input->post('modul_sem1');
			$modul_sem2 = $this->input->post('modul_sem2');
			$modul_sem3 = $this->input->post('modul_sem3');
			$modul_sem4 = $this->input->post('modul_sem4');
			$modul_sem5 = $this->input->post('modul_sem5');
			$modul_sem6 = $this->input->post('modul_sem6');
			$modul_sem7 = $this->input->post('modul_sem7');
			$modul_sem8 = $this->input->post('modul_sem8');	
			$pesan = $this->input->post('pesan');
			//echo $pesan
			
			$this->modelp->insert_konf($semester_bayar, $nama, $nim, $jurusan, $semester, $metode, $jumlah, $tgltransfer, $atasnama, $path_image, $uk, $uo, $sksujul, $semestercuti, $modul_sem1, $modul_sem2, $modul_sem3, $modul_sem4, $modul_sem5, $modul_sem6, $modul_sem7, $modul_sem8, $pesan);
			//echo 'lulu';
			$this->show_resume($semester_bayar, $nim, $path_image);
		}
		else
		{
			echo $this->upload->display_errors();
		}	
	}

	
	
	function show_resume($semester_bayar, $nim, $path_image){
		$data['result'] = $this->modelp->get_konf_data($semester_bayar, $nim, $path_image);
		foreach($data['result'] as $rows)
		{
			$data['bayar_id'] 	= $rows['bayar_id'];		
			$data['semester_bayar'] 	= $rows['semester_bayar'];		
			$data['nim'] 	= $rows['nim'];
			$data['nama'] 	= $rows['nama'];		
			$data['jurusan'] 	= $rows['jurusan'];		
			$data['semester'] 	= $rows['semester'];		
			$data['metode'] 	= $rows['metode'];		
			$data['jumlah'] 	= $rows['jumlah'];		
			$data['atasnama'] 	= $rows['atasnama'];		
			$data['tgltransfer'] 	= $rows['tgltransfer'];		
			$data['path_image'] 	= $rows['path_image'];		
			$data['uk'] 	= $rows['uk'];		
			$data['uo'] 	= $rows['uo'];		
			$data['sksujul'] 	= $rows['sksujul'];				
			$data['semestercuti'] 	= $rows['semestercuti'];		
			$data['modul_sem1'] 	= $rows['modul_sem1'];		
			$data['modul_sem2'] 	= $rows['modul_sem2'];		
			$data['modul_sem3'] 	= $rows['modul_sem3'];		
			$data['modul_sem4'] 	= $rows['modul_sem4'];		
			$data['modul_sem5'] 	= $rows['modul_sem5'];		
			$data['modul_sem6'] 	= $rows['modul_sem6'];		
			$data['modul_sem7'] 	= $rows['modul_sem7'];		
			$data['modul_sem8'] 	= $rows['modul_sem8'];		
			$data['pesan'] 	= $rows['pesan'];		
		}

		$data['isi'] = "konfirmasi/show_konf";				
		$this->load->view('konfirmasi/summary', $data);	
	}
}