<?php
	//header( 'Location: closed.php' ) ;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftran mahasiswa online</title>
   <script language="JavaScript" type="text/javascript">
    function jcap(form){

		 if (form.nama.value == "") {
			alert( "Harap masukkan nama anda." );
			form.nama.focus();
			return false ;
		  }
		  else if (form.alamat.value == "") {
			alert( "Harap masukkan alamat anda." );
			form.alamat.focus();
			return false ;
		  }
		  else if (form.kotataiwan.value == "") {
			alert( "Harap masukkan Kota Taiwan anda." );
			form.kotataiwan.focus();
			return false ;
		  }
		  else if (form.kodepostaiwan.value == "") {
			alert( "Harap masukkan kode pos Taiwan anda" );
			form.kodepostaiwan.focus();
			return false ;
		  }
		  else if (form.telepon.value == "") {
			alert( "Harap masukkan nomor telepon anda." );
			form.telepon.focus();
			return false ;
		  }
		  else if (form.handphone.value == "") {
			alert( "Harap masukkan nomor handphone anda." );
			form.handphone.focus();
			return false ;
		  }
		  else if (form.email.value == "") {
			alert( "Harap masukkan email anda." );
			form.email.focus();
			return false ;
		  }
		  else if (form.tempatlahir.value == "") {
			alert( "Harap masukkan tempatlahir anda." );
			form.tempatlahir.focus();
			return false ;
		  }
		  else if (form.tahunijazah.value == "") {
			alert( "Harap masukkan tahun ijazah anda." );
			form.tahunijazah.focus();
			return false ;
		  }
		  else if (form.namaibu.value == "") {
			alert( "Harap masukkan nama Ibu anda." );
			form.namaibu.focus();
			return false ;
		  }
		  else if (form.norekening.value == "") {
			alert( "Harap masukkan nomor rekening anda." );
			form.norekening.focus();
			return false ;
		  }
		  else if (form.namabank.value == "") {
			alert( "Harap masukkan nama bank anda." );
			form.namabank.focus();
			return false ;
		  }
		  return true;
  }
  </script>
</head>

<body>
<div id="stylized" class="myform">
<form name="1" action="submit.php" method="post" onsubmit="return jcap(this);">
<h1>Formulir Daftar Ulang Mahasiswa Lama (Angkatan I - UT Taiwan)</h1>
<p>Isilah formulir pendaftaran ini dengan data yang benar</p>

<label>Nama
<span class="small">Tulis nama anda</span>
</label>
<input type="text" name="nama" id="nama" />

<label>Alamat
<span class="small">Tulis alamat anda di taiwan</span>
</label>
<input type="text" name="alamat" id="alamat"  maxlength="50" />

<label>Kota
<span class="small">Tulis kota tempat anda di taiwan</span>
</label>
<input type="text" name="kotataiwan" id="kotataiwan"  readonly="readonly" value="99009 (Taiwan)"  />

<label>Kodepos
<span class="small">Tulis kodepos anda </span>
</label>
<input type="text" name="kodepostaiwan" id="kodepostaiwan"/>

<label>No Telepon
<span class="small">Tulis telepon yang bisa dihubungi </span>
</label>
<input type="text" name="telepon" id="telepon" />

<label>No Handphone
<span class="small">Tulis handphone yang bisa dihubungi </span>
</label>
<input type="text" name="handphone" id="handphone" />

<label>Email
<span class="small">Tulis alamat email anda dengan benar </span>
</label>
<input type="text" name="email" id="email" />

<label>Tanggal Lahir
<span class="small">Pilih tanggal lahir anda</span>
</label>
<span class="panjang">
<select name="tanggallahir" id="tanggallahir">
          <option value="1" selected="selected">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
</select>
<select name="bulanlahir" id="bulanlahir">
        <option value="1" selected="selected">Januari</option>
        <option value="2">Februari</option>
        <option value="3">Maret</option>
        <option value="4">April</option>
        <option value="5">Mei</option>
        <option value="6">Juni</option>
        <option value="7">Juli</option>
        <option value="8">Agustus</option>
        <option value="9">September</option>
        <option value="10">Oktober</option>
        <option value="11">Nopember</option>
        <option value="12">Desember</option>
</select>
<select name="tahunlahir" id="tahunlahir">
<?php for($i=1955; $i<= 2000; $i++){ ?>
        <option value="<? echo $i;?>"><? echo $i;?></option>
        <?
	  }?>
</select>
</span>

<label>Tempat Lahir
<span class="small">Tulis kota tempat anda lahir </span>
</label>
<input type="text" name="tempatlahir" id="tempatlahir" />

<label>Agama
<span class="small">Pilih agama yang anda anut</span>
</label>
<span class="panjang">
<select name="agama" id="agama">
        <option value="Islam" selected="selected">Islam</option>
        <option value="Katolik">Katolik</option>
        <option value="Protestan">Protestan</option>
        <option value="Hindu">Hindu</option>
        <option value="Budha">Budha</option>
</select>
</span>

<label>Program Studi
<span class="small">Pilih program studi yang anda minati</span>
</label>
<span class="panjang">
<select name="programstudi" id="programstudi"> 
      <option value="54" selected="selected">Manajemen</option> 
      <option disabled="disabled" value="72">Ilmu Komunikasi</option> 
      <option disabled="disabled" value="87">Sastra Inggris (Penerjemahan)</option>
</select>
</span>

<label>Kode UPBJJ
<span class="small">Kode UPBJJ anda</span>
</label>
<input name="upbjj" type="text" id="upbjj" readonly="readonly" value="021 (Jakarta)" />

<label>Jenis Kelamin
<span class="small">Jenis Kelamin</span>
</label>
<span class="panjang">
<select name="jeniskelamin" id="jeniskelamin">
        <option value="Pria" selected="selected">Pria</option>
        <option value="Wanita">Wanita</option>
</select>
</span>

<label>Kewarganegaraan
<span class="small">Pilih kewarganegaraan anda</span>
</label>
<span class="panjang">
<select name="kewarganegaraan" id="kewarganegaraan">
        <option value="WNI">WNI</option>
        <option value="Asing">Asing</option>
</select>
</span>

<label>Pekerjaan
<span class="small">Pilih pekerjaan anda</span>
</label>
<span class="panjang">
<select name="pekerjaan" id="pekerjaan">
        <option value="Swasta / TKI">Swasta / TKI</option>
        <option value="TNI/Polri">TNI/Polri</option>
        <option value="Wiraswasta">Wiraswasta</option>
        <option value="Tidak Bekerja">Tidak Bekerja</option>
        <option value="PNS">PNS</option>
</select>
</span>

<label>Status Pernikahan
<span class="small">Pilih status pernikahan anda</span>
</label>
<span class="panjang">
<select name="pernikahan" id="pernikahan">
      <option value="Menikah">Menikah</option>
      <option value="Tidak Menikah">Tidak Menikah</option>
</select>
</span>

<div class="spacer"></div>
<H2>Pendidikan Terakhir</H2>
<p>Isi dengan informasi pendidikan terkhir anda</p>

<label>Jenjang
<span class="small">Pilih jenjang pendidikan terkhir anda</span>
</label>
<span class="panjang">
<select name="jenjang" id="jenjang">
        <option value="SLTA" selected="selected">SLTA</option>
        <option value="D-I">D-I</option>
        <option value="D-II">D-II</option>
        <option value="D-III">D-III</option>
</select>
</span>

<label>Jurusan
<span class="small">Pilih jurusan pendidikan terkhir anda</span>
</label>
<span class="panjang">
<select name="jurusan" id="jurusan">
        <option value="SMTA Umum IPA / IPS" selected="selected">SMTA Umum IPA / IPS</option>
        <option value="STM/SMTA">STM/SMTA</option>
        <option value="SMK/SMKA">SMK/SMKA</option>
        <option value="SMEA">SMEA</option>
        <option value="SPMA">SPMA</option>
        <option value="SPG/SPGLB/SGO/SMOA">SPG/SPGLB/SGO/SMOA</option>
</select>
</span>

<label>Tahun Ijazah
<span class="small">Tulis tahun ijazah pendidikan terakhir</span>
</label>
<input name="tahunijazah" type="text" id="tahunijazah" size="4" maxlength="4" />

<label>Nama Ibu
<span class="small">Tulis nama ibu anda</span>
</label>
<input type="text" name="namaibu" id="namaibu" />

<label>No. Rekening
<span class="small">Tulis no. rekening bank anda di Taiwan</span>
</label>
<input type="text" name="norekening" id="norekening" />

<label>Nama Bank
<span class="small">Tulis nama bank anda di Taiwan</span>
</label>
<input type="text" name="namabank" id="namabank" />
<button type="submit">Daftar UT</button>
<div class="spacer"></div>

</form>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
