<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Ulang Mahasiswa Lama (Angkatan I - UT Taiwan)</title>
    <style type="text/css">
<!--
.style1 {
	font-size: 36px;
	font-weight: bold;
}
.style2 {font-size: 24px}
-->
    </style>
</head>

<? $id = $_GET['id'];?>
<body>
<div id="stylized" class="myform">
  <p class="style1">Pendaftaran Ulang Mahasiswa Lama Sukses !</p>
  <p class="style2">Kode Registrasi anda adalah : <strong><? echo $id;?></strong></p>
  <p> Untuk melengkapi persyaratan pendaftaran ulang Mahasiswa Lama (Angkatan I - UT Taiwan) anda wajib :<br/>
  Membayar uang kuliah (6000 NT) dan uang operasional(4500 NT) dengan cara mentransfer ke 
Chungwha Post (Bank Kantor Post) 0001907-0141074 
atas nama Shi jia rui / 施佳銳.<br/>
Kemudian melakukan konfirmasi pembayaran dengan mengirimkan sms ke no +886-92170 7443;<br/><br/> 
Format sms untuk konfirmasi: <br/><br/>

Nama/Angkatan/Kode Pembayaran/Tanggal Transfer/Jumlah Uang  <br/><br/>

note: Isi Angkatan dengan karakter I romawi (I).

contoh : Rani/I/UK/7 Agustus/6000<br/><br/>

Calon mahasiswa yang tercatat adalah <strong>calon mahasiswa yang sudah melunasi pembayaran daftar ulang seperti di atas</strong>.</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
