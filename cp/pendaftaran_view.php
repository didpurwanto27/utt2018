<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mahasiswa</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script></head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li class="current"><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">PENDAFTARAN</div>
			<div>
			<div>
				<table id="background-image" width="92%">
				  
<tr>
				    	  <th width="20%" rowspan="2" scope="col">ID SEMESTER</th>
				    	  <th colspan="2" scope="col">Registrasi Baru</th>
				    	  <th colspan="2" scope="col">Registrasi Lama</th>
						   <th colspan="2" scope="col">Ujian Ulang</th>
				    	  <th width="11%" rowspan="2" scope="col">Aktif (<em>Now</em>)</th>
    	  <th width="13%" rowspan="2" scope="col">Operation</th>
		    	  </tr>
				    	<tr>
				        	<th width="15%" scope="col">Mulai</th>
			              <th width="14%" scope="col">Selesai</th>
			              <th width="13%" scope="col">Mulai</th>
			              <th width="14%" scope="col">Selesai</th>
						  <th width="13%" scope="col">Mulai</th>
			              <th width="14%" scope="col">Selesai</th>
	              </tr>
                        <form name="pendaftaran" action="pendaftaran.php" method="get">
				    	<?php				    	
				    	while ($rowpendaftaran = mysql_fetch_array($pendaftaran, MYSQL_BOTH)) {
				    	?>
				    	<tr>
				        	<td><div align="center"><?php echo $rowpendaftaran['id_semester']; ?></div></td>
                            <td><div align="center"><?php echo $rowpendaftaran['new_reg_begin']; ?></div></td>
				            <td><div align="center"><?php echo $rowpendaftaran['new_reg_end']; ?></div></td>
				            <td><div align="center"><?php echo $rowpendaftaran['re_reg_begin']; ?></div></td>
				            <td><div align="center"><?php echo $rowpendaftaran['re_reg_end']; ?></div></td>
							
							<td><div align="center"><?php echo $rowpendaftaran['ulang_reg_begin']; ?></div></td>
				            <td><div align="center"><?php echo $rowpendaftaran['ulang_reg_end']; ?></div></td>
							
							
				            <td><div align="center">
				              <input type="radio" <? if($rowpendaftaran['active_semester']==1){ echo 'checked="checked"';} ?> name="semester" id="radioaktif" value="<? echo $rowpendaftaran['id_semester'];?>" class="inputr">
			              </div></td>
				            <td>
				            	<div align="center"><a href="pendaftaran-edit.php?id=<?php echo $rowpendaftaran['id_semester'] ?>"><img src="img/edit.png" alt="Edit Data" /></a></div></td>
				        </tr>
                        <?php
						}
				        ?>
				    	<tr>
				    	  <td colspan="9"><div align="center"><input type="submit" name="button" id="button" value="Simpan"></div></td>
			    	  </tr>
                      </form>
				    	<tr>
				    	  <td colspan="9">&nbsp;</td>
		    	  </tr>
				    	<tr>
				    	  <td colspan="9"><a href="pendaftaran-add.php"><img src="images/add.png" width="16" height="16">Tambah Semester Pendaftaran Baru</a></td>
		    	  </tr>
				 	</table>
			  </div>
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
