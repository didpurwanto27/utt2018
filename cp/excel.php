<?php
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel2007.php';
include "config.php";
include "open_connection.php";
include "mahasiswa_model.php";

$varjurusan = $_GET['jurusan'];
$varangkatan = $_GET['angkatan'];

$amount = 10;

if (!isset($_GET['page']))
	$page = 0;
else
	$page = $_GET['page'];

$curday = date('j');
$curmonth = date('n');
$curyear = date('Y');
$cur_month = array (
				'1' => 'JANUARI', '2' => 'FEBRUARI', '3' => 'MARET', '4' => 'APRIL', '5' => 'MEI', '6' => 'JUNI',
				'7' => 'JULI', '8' => 'AGUSTUS', '9' => 'SEPTEMBER', '10' => 'OKTOBER', '11' => 'NOVEMBER', '12' => 'DESEMBER'
			);

$curdate = "   Taiwan, ".$curday." ".$cur_month[$curmonth]." ".$curyear;

$objPHPExcel = new PHPExcel();

//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Universitas Terbuka pokjar Taiwan");
$objPHPExcel->getProperties()->setLastModifiedBy("Bapel UTT");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

$jumlah = getJmlMahasiswa($varjurusan, $varangkatan);
$mahasiswa = getDetailMahasiswa($varjurusan, $varangkatan, $page, $amount);
$count = 0;
// Style
	$styleTitle1 = array (
						'font' => array ('bold' => true),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					);

	$styleTitle23 = array (
						'font' => array ('bold' => true, 'size' => 14),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					);

	$styleTitle4 = array (
						'font' => array ('bold' => true, 'size' => 12),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'borders' => array (
							'bottom' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
						),
					);
	$styleAllborder = array (
						'borders' => array (
										'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
									),
					); 

	$stylefont10 = array (
						'font' => array ('size' => 10),
					);

	$stylefont10bold = array (
						'font' => array ('bold' => true, 'size' => 10),
					);

	$stylefont85 = array (
						'font' => array ('bold' => true, 'size' => 8.5),
					);
					
	$stylefont85bold = array (
						'font' => array ('bold' => true, 'size' => 8.5),
					);

	$stylefont85center = array (
						'font' => array ('bold' => true, 'size' => 8.5),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'borders' => array (
									'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
								),
					);

	$stylecenter = array (
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					);
					
					
	// list box
	$boxname = array (
					0 => 'D9', 1 => 'E9', 2 => 'F9', 3 => 'G9', 4 => 'H9', 5 => 'I9', 6 => 'J9', 7 => 'K9', 8 => 'L9', 9 => 'M9', 
					10 => 'N9', 11 => 'O9', 12 => 'P9', 13 => 'Q9', 14 => 'R9', 15 => 'S9', 16 => 'T9', 17 => 'U9', 18 => 'V9', 19 => 'W9',
					20 => 'X9', 21 => 'Y9', 22 => 'Z9', 23 => 'AA9', 24 => 'AB9', 25 => 'D10', 26 => 'E10', 27 => 'F10', 28 => 'G10', 29 => 'H10', 
					30 => 'I10', 31 => 'J10', 32 => 'K10', 33 => 'L10', 34 => 'M10', 35 => 'N10', 35 => 'O10', 36 => 'P10', 37 => 'Q10',
					38 => 'R10', 39 => 'S10', 40 => 'T10', 41 => 'U10', 42 => 'V10', 43 => 'W10', 44 => 'X10', 45 => 'Y10', 46 => 'Z10', 
					47 => 'AA10', 48 => 'AB10'
				);

	$boxalamat = array (
					0 => 'D12', 1 => 'E12', 2 => 'F12', 3 => 'G12', 4 => 'H12', 5 => 'I12', 6 => 'J12', 7 => 'K12', 8 => 'L12', 9 => 'M12', 
					10 => 'N12', 11 => 'O12', 12 => 'P12', 13 => 'Q12', 14 => 'R12', 15 => 'S12', 16 => 'T12', 17 => 'U12', 18 => 'V12', 19 => 'W12',
					20 => 'X12', 21 => 'Y12', 22 => 'Z12', 23 => 'AA12', 24 => 'AB12', 25 => 'D13', 26 => 'E13', 27 => 'F13', 28 => 'G13', 29 => 'H13', 
					30 => 'I13', 31 => 'J13', 32 => 'K13', 33 => 'L13', 34 => 'M13', 35 => 'N13', 36 => 'O13', 37 => 'P13', 38 => 'Q13',
					39 => 'R13', 40 => 'S13', 41 => 'T13', 42 => 'U13', 43 => 'V13', 44 => 'W13', 45 => 'X13', 46 => 'Y13', 47 => 'Z13', 
					48 => 'AA13', 49 => 'AB13', 50 => 'D14', 51 => 'E14', 52 => 'F14', 53 => 'G14', 54 => 'H14', 55 => 'I14', 56 => 'J14', 
					57 => 'K14', 58 => 'L14', 59 => 'M14', 60 => 'N14', 61 => 'O14', 62 => 'P14', 63 => 'Q14', 64 => 'R14', 65 => 'S14', 
					66 => 'T14', 67 => 'U14', 68 => 'V14', 69 => 'W14', 70 => 'X14', 71 => 'Y14', 72 => 'Z14', 73 => 'AA14', 74 => 'AB14'
				);

	$boxkabkot = array (0 => 'D16', 1 => 'E16', 2 => 'F16', 3 => 'G16', 4 => 'H16');
	$boxkodepos = array (0 => 'D18', 1 => 'E18', 2 => 'F18', 3 => 'G18', 4 => 'H18');
	$boxtelp = array (0 => 'H20', 1 => 'I20', 2 => 'J20', 3 => 'K20', 4 => 'L20', 5 => 'M20', 6 => 'N20', 7 => 'O20', 8 => 'P20');
	$boxhp = array (0 => 'H22', 1 => 'I22', 2 => 'J22', 3 => 'K22', 4 => 'L22', 5 => 'M22', 6 => 'N22', 7 => 'O22', 8 => 'P22');
	$boxemail = array (
					0 => 'D24', 1 => 'E24', 2 => 'F24', 3 => 'G24', 4 => 'H24', 5 => 'I24', 6 => 'J24', 7 => 'K24', 8 => 'L24', 9 => 'M24', 
					10 => 'N24', 11 => 'O24', 12 => 'P24', 13 => 'Q24', 14 => 'R24', 15 => 'S24', 16 => 'T24', 17 => 'U24', 18 => 'V24', 19 => 'W24',
					20 => 'X24', 21 => 'Y24', 22 => 'Z24', 23 => 'AA24', 24 => 'AB24', 25 => 'D25', 26 => 'E25', 27 => 'F25', 28 => 'G25', 29 => 'H25', 
					30 => 'I25', 31 => 'J25', 32 => 'K25', 33 => 'L25', 34 => 'M25', 35 => 'N25', 35 => 'O25', 36 => 'P25', 37 => 'Q25',
					38 => 'R25', 39 => 'S25', 40 => 'T25', 41 => 'U25', 42 => 'V25', 43 => 'W25', 44 => 'X25', 45 => 'Y25', 46 => 'Z25', 
					47 => 'AA25', 48 => 'AB25'
				);
	$boxfb = array (
					0 => 'D27', 1 => 'E27', 2 => 'F27', 3 => 'G27', 4 => 'H27', 5 => 'I27', 6 => 'J27', 7 => 'K27', 8 => 'L27', 9 => 'M27', 
					10 => 'N27', 11 => 'O27', 12 => 'P27', 13 => 'Q27', 14 => 'R27', 15 => 'S27', 16 => 'T27', 17 => 'U27', 18 => 'V27', 19 => 'W27',
					20 => 'X27', 21 => 'Y27', 22 => 'Z27', 23 => 'AA27', 24 => 'AB27', 25 => 'D28', 26 => 'E28', 27 => 'F28', 28 => 'G28', 29 => 'H28', 
					30 => 'I28', 31 => 'J28', 32 => 'K28', 33 => 'L28', 34 => 'M28', 35 => 'N28', 35 => 'O28', 36 => 'P28', 37 => 'Q28',
					38 => 'R28', 39 => 'S28', 40 => 'T28', 41 => 'U28', 42 => 'V28', 43 => 'W28', 44 => 'X28', 45 => 'Y28', 46 => 'Z28', 
					47 => 'AA28', 48 => 'AB28'
				);
	$boxtgl = array (0 => 'D31', 1 => 'E31');
	$boxbln = array (0 => 'F31', 1 => 'G31');
	$boxthn = array (0 => 'H31', 1 => 'I31', 2 => 'J31', 3 => 'K31');
	$boxtmplhr = array (
					0 => 'D33', 1 => 'E33', 2 => 'F33', 3 => 'G33', 4 => 'H33', 5 => 'I33', 6 => 'J33', 7 => 'K33', 8 => 'L33', 9 => 'M33', 
					10 => 'N33', 11 => 'O33', 12 => 'P33', 13 => 'Q33', 14 => 'R33', 15 => 'S33', 16 => 'T33', 17 => 'U33', 18 => 'V33', 19 => 'W33',
					20 => 'X33', 21 => 'Y33', 22 => 'Z33', 23 => 'AA33', 24 => 'AB33'
				);
	$boxprodi = array (0 => 'D37', 1 => 'E37');
	$boxupbjj = array (0 => 'D39', 1 => 'E39');
	$boxjurusan = array (0 => 'D52', 1 => 'E52', 2 => 'F52');
	$boxijazah = array (0 => 'D54', 1 => 'E54', 2 => 'F54', 3 => 'G54');
	$boxmengajar = array (
					0 => 'D60', 1 => 'E60', 2 => 'F60', 3 => 'G60', 4 => 'H60', 5 => 'I60', 6 => 'J60', 7 => 'K60', 8 => 'L60', 9 => 'M60', 
					10 => 'N60', 11 => 'O60', 12 => 'P60', 13 => 'Q60', 14 => 'R60', 15 => 'S60', 16 => 'T60', 17 => 'U60', 18 => 'V60', 19 => 'W60',
					20 => 'X60', 21 => 'Y60', 22 => 'Z60', 23 => 'AA60', 24 => 'AB60'
				);
	$boxibu = array (
					0 => 'D62', 1 => 'E62', 2 => 'F62', 3 => 'G62', 4 => 'H62', 5 => 'I62', 6 => 'J62', 7 => 'K62', 8 => 'L62', 9 => 'M62', 
					10 => 'N62', 11 => 'O62', 12 => 'P62', 13 => 'Q62', 14 => 'R62', 15 => 'S62', 16 => 'T62', 17 => 'U62', 18 => 'V62', 19 => 'W62',
					20 => 'X62', 21 => 'Y62', 22 => 'Z62', 23 => 'AA62', 24 => 'AB62'
				);
	$boxbank = array (
					0 => 'D64', 1 => 'E64', 2 => 'F64', 3 => 'G64', 4 => 'H64', 5 => 'I64', 6 => 'J64', 7 => 'K64', 8 => 'L64', 9 => 'M64', 
					10 => 'N64', 11 => 'O64', 12 => 'P64', 13 => 'Q64', 14 => 'R64'
				);


while($rowmahasiswa = mysql_fetch_array($mahasiswa, MYSQL_ASSOC))
{
	$objPHPExcel->setActiveSheetIndex($count);
	$title = substr($rowmahasiswa['NAMA_MHS'], 0, 20);
	$title .= '-' . $rowmahasiswa['ID_MHS'];
	$objPHPExcel->getActiveSheet()->setTitle($title);
	
	//Set Page Margin
	$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.551181102362205);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setHeader(0.31496062992126);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.31496062992126);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.511811023622047);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.451181102362205);
	$objPHPExcel->getActiveSheet()->getPageMargins()->setFooter(0.31496062992126);

	//echo date('H:i:s') . " Add some data\n";
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2.13);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(4.99);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20.56);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(2.7);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(2.7);

	$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(10);

	$objPHPExcel->getActiveSheet()->mergeCells('B2:AB2');
	$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'KEMENTRIAN PENDIDIKAN NASIONAL');
	$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleTitle1);

	$objPHPExcel->getActiveSheet()->mergeCells('B3:AB3');
	$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'UNIVERSITAS TERBUKA');
	$objPHPExcel->getActiveSheet()->mergeCells('B4:AB4');
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'FORMULIR DATA PRIBADI MAHASISWA');
	$objPHPExcel->getActiveSheet()->getStyle('B3:B4')->applyFromArray($styleTitle23);

	$objPHPExcel->getActiveSheet()->mergeCells('B5:AB5');
	$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'PROGRAM REGULER');
	$objPHPExcel->getActiveSheet()->getStyle('B5:AB5')->applyFromArray($styleTitle4);

	$objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B7', 'K-01');
	$objPHPExcel->getActiveSheet()->SetCellValue('C7', 'N I M');
	$objPHPExcel->getActiveSheet()->getStyle('D7:L7')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B9', 'K-02');
	$objPHPExcel->getActiveSheet()->SetCellValue('C9', 'NAMA');
	$objPHPExcel->getActiveSheet()->getStyle('D9:AB10')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('11')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B12', 'K-03');
	$objPHPExcel->getActiveSheet()->SetCellValue('C12', 'ALAMAT PENGIRIMAN');
	$objPHPExcel->getActiveSheet()->getStyle('D12:AB14')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('15')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B16', 'K-04');
	$objPHPExcel->getActiveSheet()->SetCellValue('C16', 'KODE KABUPATEN/KOTA');
	$objPHPExcel->getActiveSheet()->SetCellValue('K16', 'Lihat Lampiran Katalog UT');
	$objPHPExcel->getActiveSheet()->getStyle('C16')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('K16')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D16:H16')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('17')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B18', 'K-05');
	$objPHPExcel->getActiveSheet()->SetCellValue('C18', 'KODE POS');
	$objPHPExcel->getActiveSheet()->getStyle('D18:H18')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('19')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B20', 'K-06');
	$objPHPExcel->getActiveSheet()->SetCellValue('C20', 'NOMOR TELEPON');
	$objPHPExcel->getActiveSheet()->getStyle('D20:G20')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->getStyle('H20:P20')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('21')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B22', 'K-07');
	$objPHPExcel->getActiveSheet()->SetCellValue('C22', 'NOMOR HP');
	$objPHPExcel->getActiveSheet()->getStyle('D22:P22')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('23')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B24', 'K-08');
	$objPHPExcel->getActiveSheet()->SetCellValue('C24', 'ALAMAT E-MAIL');
	$objPHPExcel->getActiveSheet()->getStyle('D24:AB25')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('26')->setRowHeight(7);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('B27', ' ');
	$objPHPExcel->getActiveSheet()->SetCellValue('C27', 'NAMA FACEBOOK');
	$objPHPExcel->getActiveSheet()->getStyle('D27:AB28')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('29')->setRowHeight(7);
	
	$objPHPExcel->getActiveSheet()->mergeCells('D30:E30');
	$objPHPExcel->getActiveSheet()->SetCellValue('D30', 'TGL');
	$objPHPExcel->getActiveSheet()->getStyle('D30:E30')->applyFromArray($stylefont85center);
	$objPHPExcel->getActiveSheet()->getRowDimension('30')->setRowHeight(12);

	$objPHPExcel->getActiveSheet()->mergeCells('F30:G30');
	$objPHPExcel->getActiveSheet()->SetCellValue('F30', 'BLN');
	$objPHPExcel->getActiveSheet()->getStyle('F30:G30')->applyFromArray($stylefont85center);

	$objPHPExcel->getActiveSheet()->mergeCells('H30:K30');
	$objPHPExcel->getActiveSheet()->SetCellValue('H30', 'TAHUN');
	$objPHPExcel->getActiveSheet()->getStyle('H30:K30')->applyFromArray($stylefont85center);

	$objPHPExcel->getActiveSheet()->SetCellValue('B31', 'K-09');
	$objPHPExcel->getActiveSheet()->SetCellValue('C31', 'TANGGAL LAHIR');
	$objPHPExcel->getActiveSheet()->getStyle('D31:K31')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('32')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B33', 'K-10');
	$objPHPExcel->getActiveSheet()->SetCellValue('C33', 'TEMPAT LAHIR');
	$objPHPExcel->getActiveSheet()->getStyle('D33:AB33')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('34')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B35', 'K-11');
	$objPHPExcel->getActiveSheet()->SetCellValue('C35', 'AGAMA');
	$objPHPExcel->getActiveSheet()->SetCellValue('E35', 'Islam');
	$objPHPExcel->getActiveSheet()->getStyle('E35')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D35')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('J35', 'Katholik');
	$objPHPExcel->getActiveSheet()->getStyle('J35')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('I35')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('P35', 'Protestan');
	$objPHPExcel->getActiveSheet()->getStyle('P35')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('O35')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('U35', 'Hindu');
	$objPHPExcel->getActiveSheet()->getStyle('U35')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('T35')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('Y35', 'Budha');
	$objPHPExcel->getActiveSheet()->getStyle('Y35')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('X35')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('36')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B37', 'K-12');
	$objPHPExcel->getActiveSheet()->SetCellValue('C37', 'KODE PROGRAM STUDI');
	$objPHPExcel->getActiveSheet()->getStyle('C37')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->SetCellValue('G37', 'Yang Dipilih, Lihat Katalog UT');
	$objPHPExcel->getActiveSheet()->getStyle('G37')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D37:E37')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('38')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B39', 'K-13');
	$objPHPExcel->getActiveSheet()->SetCellValue('C39', 'KODE UPBJJ');
	$objPHPExcel->getActiveSheet()->SetCellValue('G39', 'Lihat Lampiran Katalog UT');
	$objPHPExcel->getActiveSheet()->getStyle('G39')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D39:E39')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('40')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B41', 'K-14');
	$objPHPExcel->getActiveSheet()->SetCellValue('C41', 'JENIS KELAMIN');
	$objPHPExcel->getActiveSheet()->SetCellValue('E41', 'Laki-laki');
	$objPHPExcel->getActiveSheet()->getStyle('E41')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D41')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('J41', 'Perempuan');
	$objPHPExcel->getActiveSheet()->getStyle('J41')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('I41')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('42')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B43', 'K-15');
	$objPHPExcel->getActiveSheet()->SetCellValue('C43', 'KEWARGANEGARAAN');
	$objPHPExcel->getActiveSheet()->SetCellValue('E43', 'Indonesia');
	$objPHPExcel->getActiveSheet()->getStyle('D43')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('J43', 'Asing');
	$objPHPExcel->getActiveSheet()->getStyle('I43')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('44')->setRowHeight(6);

	$objPHPExcel->getActiveSheet()->SetCellValue('B45', 'K-16');
	$objPHPExcel->getActiveSheet()->SetCellValue('C45', 'STATUS PEKERJAAN');
	$objPHPExcel->getActiveSheet()->SetCellValue('E45', 'TNI/POLRI');
	$objPHPExcel->getActiveSheet()->getStyle('E45')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D45')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('J45', 'PNS');
	$objPHPExcel->getActiveSheet()->getStyle('J45')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('I45')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('N45', 'Swasta');
	$objPHPExcel->getActiveSheet()->getStyle('N45')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('M45')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('R45', 'Wiraswasta');
	$objPHPExcel->getActiveSheet()->getStyle('R45')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('Q45')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('X45', 'Tidak Bekerja');
	$objPHPExcel->getActiveSheet()->getStyle('X45')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('W45')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('46')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B47', 'K-17');
	$objPHPExcel->getActiveSheet()->SetCellValue('C47', 'STATUS PERKAWINAN');
	$objPHPExcel->getActiveSheet()->SetCellValue('E47', 'Menikah');
	$objPHPExcel->getActiveSheet()->getStyle('E47')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D47')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('J47', 'Tidak Menikah');
	$objPHPExcel->getActiveSheet()->getStyle('J47')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('I47')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('48')->setRowHeight(6);

	$objPHPExcel->getActiveSheet()->SetCellValue('C49', 'PENDIDIKAN TERAKHIR');
	$objPHPExcel->getActiveSheet()->getRowDimension('49')->setRowHeight(12);
	$objPHPExcel->getActiveSheet()->getStyle('C49')->applyFromArray($stylefont10bold);

	$objPHPExcel->getActiveSheet()->SetCellValue('B50', 'K-18');
	$objPHPExcel->getActiveSheet()->SetCellValue('C50', 'JENJANG');
	$objPHPExcel->getActiveSheet()->SetCellValue('E50', 'SLTA');
	$objPHPExcel->getActiveSheet()->getStyle('E50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('I50', 'D-I');
	$objPHPExcel->getActiveSheet()->getStyle('I50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('H50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('L50', 'D-II');
	$objPHPExcel->getActiveSheet()->getStyle('L50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('K50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('O50', 'D-III');
	$objPHPExcel->getActiveSheet()->getStyle('O50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('N50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('R50', 'S-1');
	$objPHPExcel->getActiveSheet()->getStyle('R50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('Q50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('U50', 'S-2');
	$objPHPExcel->getActiveSheet()->getStyle('U50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('T50')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('X50', 'S-3');
	$objPHPExcel->getActiveSheet()->getStyle('X50')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('W50')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('51')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B52', 'K-19');
	$objPHPExcel->getActiveSheet()->SetCellValue('C52', 'KODE JURUSAN');
	$objPHPExcel->getActiveSheet()->getStyle('D52:F52')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('53')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B54', 'K-20');
	$objPHPExcel->getActiveSheet()->SetCellValue('C54', 'TAHUN IJAZAH');
	$objPHPExcel->getActiveSheet()->getStyle('D54:G54')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('55')->setRowHeight(6.75);

	$objPHPExcel->getActiveSheet()->SetCellValue('B56', 'K-21');
	$objPHPExcel->getActiveSheet()->SetCellValue('C56', 'SEBAGAI TENAGA PENGAJAR');
	$objPHPExcel->getActiveSheet()->getStyle('C56')->applyFromArray($stylefont85);
	$objPHPExcel->getActiveSheet()->SetCellValue('E56', 'Ya');
	$objPHPExcel->getActiveSheet()->getStyle('E56')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('D56')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('I56', 'Tidak');
	$objPHPExcel->getActiveSheet()->getStyle('I56')->applyFromArray($stylefont10);
	$objPHPExcel->getActiveSheet()->getStyle('G56')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->SetCellValue('C57', 'JIKA YA');
	$objPHPExcel->getActiveSheet()->getRowDimension('57')->setRowHeight(10.5);

	$objPHPExcel->getActiveSheet()->SetCellValue('B58', 'K-22');
	$objPHPExcel->getActiveSheet()->SetCellValue('C58', 'MENGAJAR PADA');
	$objPHPExcel->getActiveSheet()->SetCellValue('E58', 'TK');
	$objPHPExcel->getActiveSheet()->getStyle('D58')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('H58', 'SD');
	$objPHPExcel->getActiveSheet()->getStyle('G58')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('K58', 'SLTP');
	$objPHPExcel->getActiveSheet()->getStyle('J58')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('N58', 'SLTA');
	$objPHPExcel->getActiveSheet()->getStyle('M58')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('Q58', 'PT');
	$objPHPExcel->getActiveSheet()->getStyle('P58')->applyFromArray($styleAllborder);
	$objPHPExcel->getActiveSheet()->SetCellValue('T58', 'Non Formal');
	$objPHPExcel->getActiveSheet()->getStyle('S58')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('59')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B60', 'K-23');
	$objPHPExcel->getActiveSheet()->SetCellValue('C60', 'MENGAJAR BIDANG STUDI');
	$objPHPExcel->getActiveSheet()->getStyle('C60')->applyFromArray($stylefont85);
	$objPHPExcel->getActiveSheet()->getStyle('D60:AB60')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('61')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B62', 'K-24');
	$objPHPExcel->getActiveSheet()->SetCellValue('C62', 'NAMA IBU KANDUNG');
	$objPHPExcel->getActiveSheet()->getStyle('D62:AB62')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->getRowDimension('63')->setRowHeight(7);

	$objPHPExcel->getActiveSheet()->SetCellValue('B64', 'K-25');
	$objPHPExcel->getActiveSheet()->SetCellValue('C64', 'NO. REKENING BRI');
	$objPHPExcel->getActiveSheet()->getStyle('D64:R64')->applyFromArray($styleAllborder);

	$objPHPExcel->getActiveSheet()->SetCellValue('U65', $curdate);

	$objPHPExcel->getActiveSheet()->getRowDimension('66')->setRowHeight(10);
	//$objPHPExcel->getActiveSheet()->getRowDimension('63')->setRowHeight(10);

	$objPHPExcel->getActiveSheet()->SetCellValue('B67', 'Keterangan: - Isilah dengan huruf Kapital pada kotak yang disediakan');
	$objPHPExcel->getActiveSheet()->getStyle('B67')->applyFromArray($stylefont85);

	$objPHPExcel->getActiveSheet()->SetCellValue('T68', '(                                                  )');

	$objPHPExcel->getActiveSheet()->SetCellValue('B68', '                              - K-11, K-14, K-15, K-16, K-17, K-18, K-21, dan K-22 diisi dengan X pada satu kotak yang sesuai   ');
	$objPHPExcel->getActiveSheet()->getStyle('B68')->applyFromArray($stylefont85);
	$objPHPExcel->getActiveSheet()->SetCellValue('B69', '                              - Data diisi sesuai dengan formulir online   ');
	$objPHPExcel->getActiveSheet()->getStyle('B69')->applyFromArray($stylefont85);
	$objPHPExcel->getActiveSheet()->SetCellValue('V69', 'Tanda Tangan');

	$lenname = strlen($rowmahasiswa['NAMA_MHS']);
	
	for($i=0; $i<$lenname; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxname[$i], strtoupper($rowmahasiswa['NAMA_MHS'][$i]));
		$objPHPExcel->getActiveSheet()->getStyle($boxname[$i])->applyFromArray($stylecenter);
	}


	$rowmahasiswa['ALAMAT_MHS'] = str_ireplace('taiwan', '', $rowmahasiswa['ALAMAT_MHS']);
	$rowmahasiswa['ALAMAT_MHS'] = trim($rowmahasiswa['ALAMAT_MHS']);
	$rowmahasiswa['ALAMAT_MHS'] = preg_replace("/[[:space:]]+/"," ",$rowmahasiswa['ALAMAT_MHS']);
	

	if($rowmahasiswa['ALAMAT_MHS'][$lenalamat-1] == ",") 
		$rowmahasiswa['ALAMAT_MHS'] = substr($rowmahasiswa['ALAMAT_MHS'], 0, -1);
	
	$allalamat = $rowmahasiswa['ALAMAT_MHS']." ".$rowmahasiswa['DISTRICT_MHS']." ".$rowmahasiswa['KABKOT_MHS']." ".$rowmahasiswa['KODEPOS_MHS'];
	
	$lenalamat = strlen($allalamat);
	
	
	
	if($lenalamat > 75)
		$lenalamat = 75;
		
	for($i=0; $i<$lenalamat; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxalamat[$i], $allalamat[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxalamat[$i])->applyFromArray($stylecenter);
	}
	$kabkot = "99009";
	$lenkabkot = strlen($kabkot);
	if($lenkabkot > 5)
		$lenkabkot = 5;
	for($i=0; $i<$lenkabkot; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxkabkot[$i], $kabkot[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxkabkot[$i])->applyFromArray($stylecenter);
	}

	$lenkodepos = strlen($rowmahasiswa['KODEPOS_MHS']);
	if($lenkodepos > 5)
		$lenkodepos = 5;
	for($i=0; $i<$lenkodepos; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxkodepos[$i], $rowmahasiswa['KODEPOS_MHS'][$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxkodepos[$i])->applyFromArray($stylecenter);
	}
	
	$len = strlen($rowmahasiswa['TELP_MHS']);
	if($len >= 9)
	{
		$telpmhs = substr($rowmahasiswa['TELP_MHS'], -9);
	}
	else
	{
		$telpmhs = $rowmahasiswa['TELP_MHS'];
	}
	
	$lentelp = strlen($telpmhs);
	$objPHPExcel->getActiveSheet()->SetCellValue('D20', '+');
	$objPHPExcel->getActiveSheet()->SetCellValue('E20', '8');
	$objPHPExcel->getActiveSheet()->SetCellValue('F20', '8');
	$objPHPExcel->getActiveSheet()->SetCellValue('G20', '6');
	$objPHPExcel->getActiveSheet()->getStyle('D20:G20')->applyFromArray($stylecenter);
	for($i=0; $i<9; $i++) {
		if($i < $lentelp)
		{
			$objPHPExcel->getActiveSheet()->SetCellValue($boxtelp[$i], $telpmhs[$i]);
		}
		else
		{
			$objPHPExcel->getActiveSheet()->SetCellValue($boxtelp[$i], ' ');
		}
		$objPHPExcel->getActiveSheet()->getStyle($boxtelp[$i])->applyFromArray($stylecenter);
	}

	$len = strlen($rowmahasiswa['CELLPHONE_MHS']);
	if($len >= 9)
	{
		$hpmhs = substr($rowmahasiswa['CELLPHONE_MHS'], -9);
	}
	else
	{
		$hpmhs = $rowmahasiswa['CELLPHONE_MHS'];
	}
	$lenhp = strlen($hpmhs);
	$objPHPExcel->getActiveSheet()->SetCellValue('D22', '+');
	$objPHPExcel->getActiveSheet()->SetCellValue('E22', '8');
	$objPHPExcel->getActiveSheet()->SetCellValue('F22', '8');
	$objPHPExcel->getActiveSheet()->SetCellValue('G22', '6');
	$objPHPExcel->getActiveSheet()->getStyle('D22:G22')->applyFromArray($stylecenter);
	for($i=0; $i<9; $i++) {
		if($i < $lenhp)
		{
			$objPHPExcel->getActiveSheet()->SetCellValue($boxhp[$i], $hpmhs[$i]);
		}
		else
		{
			$objPHPExcel->getActiveSheet()->SetCellValue($boxhp[$i], ' ');
		}
		$objPHPExcel->getActiveSheet()->getStyle($boxhp[$i])->applyFromArray($stylecenter);
	}

	$lenemail = strlen($rowmahasiswa['EMAIL_MHS']);
	for($i=0; $i<$lenemail; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxemail[$i], $rowmahasiswa['EMAIL_MHS'][$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxemail[$i])->applyFromArray($stylecenter);
	}
	
	$lenfb = strlen($rowmahasiswa['NAMAFB_MHS']);
	for($i=0; $i<$len; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxfb[$i], $rowmahasiswa['NAMAFB_MHS'][$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxfb[$i])->applyFromArray($stylecenter);
	}

	list($thn, $bln, $tgl) = explode("-", $rowmahasiswa['TGL_LHR_MHS']);
	for($i=0; $i<2; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxtgl[$i], $tgl[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxtgl[$i])->applyFromArray($stylecenter);
	}

	for($i=0; $i<2; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxbln[$i], $bln[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxbln[$i])->applyFromArray($stylecenter);
	}

	for($i=0; $i<4; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxthn[$i], $thn[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxthn[$i])->applyFromArray($stylecenter);
	}

	$lentmplhr = strlen($rowmahasiswa['TMP_LHR_MHS']);
	if($lentmplhr > 24)
		$lentmplhr = 24;
	for($i=0; $i<$lentmplhr; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxtmplhr[$i], $rowmahasiswa['TMP_LHR_MHS'][$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxtmplhr[$i])->applyFromArray($stylecenter);
	}

	if(strtolower($rowmahasiswa['AGAMA_MHS']) == "islam") {	
		$objPHPExcel->getActiveSheet()->SetCellValue('D35', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D35')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['AGAMA_MHS']) == "katholik") {	
		$objPHPExcel->getActiveSheet()->SetCellValue('I35', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('L35')->applyFromArray($stylecenter);
	}else if (strtolower($rowmahasiswa['AGAMA_MHS']) == "protestan") {	
		$objPHPExcel->getActiveSheet()->SetCellValue('P35', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('P35')->applyFromArray($stylecenter);
	}else if (strtolower($rowmahasiswa['AGAMA_MHS']) == "hindu") {	
		$objPHPExcel->getActiveSheet()->SetCellValue('U35', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('U35')->applyFromArray($stylecenter);
	}else if (strtolower($rowmahasiswa['AGAMA_MHS']) == "budha") {	
		$objPHPExcel->getActiveSheet()->SetCellValue('Y35', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('Y35')->applyFromArray($stylecenter);
	}

	$objPHPExcel->getActiveSheet()->SetCellValue('D37', $rowmahasiswa['PROGSTUDI_ID'][0]);
	$objPHPExcel->getActiveSheet()->SetCellValue('E37', $rowmahasiswa['PROGSTUDI_ID'][1]);
	$objPHPExcel->getActiveSheet()->getStyle('D37:E37')->applyFromArray($stylecenter);

	$objPHPExcel->getActiveSheet()->SetCellValue('D39', '1');
	$objPHPExcel->getActiveSheet()->SetCellValue('E39', '0');
	$objPHPExcel->getActiveSheet()->getStyle('D39:E39')->applyFromArray($stylecenter);

	if(strtolower($rowmahasiswa['JK_MHS']) == "pria") {
		$objPHPExcel->getActiveSheet()->SetCellValue('D41', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D41')->applyFromArray($stylecenter);
	} else if(strtolower($rowmahasiswa['JK_MHS']) == "wanita") {
		$objPHPExcel->getActiveSheet()->SetCellValue('I41', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('I41')->applyFromArray($stylecenter);
	}

	if(strtolower($rowmahasiswa['WN_MHS']) == "wni") {
		$objPHPExcel->getActiveSheet()->SetCellValue('D43', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D43')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['WN_MHS']) == "asing") {
		$objPHPExcel->getActiveSheet()->SetCellValue('I43', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('I43')->applyFromArray($stylecenter);
	}

	if(strtolower($rowmahasiswa['PEKERJAAN_MHS']) == "tni/polri") {
		$objPHPExcel->getActiveSheet()->SetCellValue('D45', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D45')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['PEKERJAAN_MHS']) == "pns") {
		$objPHPExcel->getActiveSheet()->SetCellValue('I45', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('I45')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['PEKERJAAN_MHS']) == "swasta / tki") {
		$objPHPExcel->getActiveSheet()->SetCellValue('M45', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('M45')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['PEKERJAAN_MHS']) == "wiraswasta") {
		$objPHPExcel->getActiveSheet()->SetCellValue('Q45', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('Q45')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['PEKERJAAN_MHS']) == "tidak bekerja") {
		$objPHPExcel->getActiveSheet()->SetCellValue('W45', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('W45')->applyFromArray($stylecenter);
	}

	if(strtolower($rowmahasiswa['STKWN_MHS']) == "menikah") {
		$objPHPExcel->getActiveSheet()->SetCellValue('D47', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D47')->applyFromArray($stylecenter);
	} else {
		$objPHPExcel->getActiveSheet()->SetCellValue('I47', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('I47')->applyFromArray($stylecenter);
	}

	if(strtolower($rowmahasiswa['JENJANGPDK_MHS']) == "slta") {
		$objPHPExcel->getActiveSheet()->SetCellValue('D50', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('D50')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['JENJANGPDK_MHS']) == "d-i") {
		$objPHPExcel->getActiveSheet()->SetCellValue('H50', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('H50')->applyFromArray($stylecenter);
	} else if (strtolower($rowmahasiswa['JENJANGPDK_MHS']) == "d-ii") {
		$objPHPExcel->getActiveSheet()->SetCellValue('K50', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('K50')->applyFromArray($stylecenter);
	} else {
		$objPHPExcel->getActiveSheet()->SetCellValue('N50', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('N50')->applyFromArray($stylecenter);
	}

	$kdjur = array (
					'SMTA Umum IPA / IPS' => '101',
					'STM/SMTA' => '102',
					'SMK/SMKA' => '107',
					'SMEA' => '104',
					'SPMA' => '103',
					'SPG/SPGLB/SGO/SMOA' => '108'
				);

	$newjurusan = $kdjur[$rowmahasiswa['JURUSAN_MHS']];
	for($i=0; $i<3; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxjurusan[$i], $newjurusan[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxjurusan[$i])->applyFromArray($stylecenter);
	}

	$lenthnijazah = strlen($rowmahasiswa['THNIJAZAH_MHS']);
	for($i=0; $i<$lenthnijazah; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxijazah[$i], $rowmahasiswa['THNIJAZAH_MHS'][$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxijazah[$i])->applyFromArray($stylecenter);
	}


	$objPHPExcel->getActiveSheet()->SetCellValue('G56', 'X');
		$objPHPExcel->getActiveSheet()->getStyle('G56')->applyFromArray($stylecenter);

	$lenibu = strlen($rowmahasiswa['NAMAIBU_MHS']);
	if($lenibu > 25)
		$lenibu = 25;
	for($i=0; $i<$lenibu; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxibu[$i], strtoupper($rowmahasiswa['NAMAIBU_MHS'][$i]));
		$objPHPExcel->getActiveSheet()->getStyle($boxibu[$i])->applyFromArray($stylecenter);
	}


	/*$lenbank = strlen($bank);
	for($i=0; $i<$lenibu; $i++) {
		$objPHPExcel->getActiveSheet()->SetCellValue($boxbank[$i], $bank[$i]);
		$objPHPExcel->getActiveSheet()->getStyle($boxbank[$i])->applyFromArray($stylecenter);
	}*/



	// Rename sheet
	//echo date('H:i:s') . " Rename sheet\n";
	if($count < $jumlah)
	{
		$objPHPExcel->CreateSheet();
		$count++;
	}
}
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

ob_end_clean();
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$varjurusan.'-'.$varangkatan.'-'.$page.'.xlsx"');

$objWriter->save('php://output');
exit();
//$objPHPExcel->disconnectWorksheets();
//unset($objPHPExcel);


?> 