<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mahasiswa</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li class="current"><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">PENCARIAN MAHASISWA</div>
			<div>
				<form action="mahasiswa-search.php" method="get">
					<fieldset>
						<legend>Dasar Pencarian</legend>
						<div>
							<label>ID</label>
							<input type="text" name="id" />
						</div>
						<div>
							<label>Nama</label>
							<input type="text" name="nama" />
						</div>
						<div>
							<label>Email</label>
							<input type="text" name="email" />
						</div>
                        <div>
							<label>Facebook</label>
							<input type="text" name="fb" />
						</div>
					</fieldset>
					<button type="submit" class="cari">Cari Data</button>
				</form>
			</div>
			
			<?php
			if($error == "nokeyword")
				print "<p>Masukkan keyword pada pencarian</p>";
			if($_GET && $error != "nokeyword") {
			?>
			<div class="search">
				<table  id="background-image" width="100%">
					<thead>
						<tr>
				        	<th scope="col">ID</th>
				            <th scope="col">NIM</th>
				            <th scope="col">NAMA</th>
				            <th scope="col">Semester</th>
				            <th scope="col">Operation</th>
				        </tr>
					</thead>
					<tbody>
					<?php while($row = mysql_fetch_array($result, MYSQL_ASSOC)) { ?>
						<tr>
							<td class="operation"><a href="mahasiswa-detail.php?id=<?php echo $row['id_mhs'];?>"><?php echo $row['id_mhs'];?></a></td>
							<td class="operation"><?php echo $row['nim_mhs'];?></td>
							<td><?php echo $row['nama_mhs'];?></td>
							<td class="operation"><?php echo $row['semester_aktif'];?></td>
							<td class="operation">
								<a href="mahasiswa-edit.php?id=<?php echo $row['id_mhs'] ?>"><img src="img/edit.png" alt="Edit Data" /></a>
				            	<a href="del.php?id=<?php echo $row['id_mhs']?>" onclick="return confirm('Apakah ingin menghapus data ini?')"><img src="img/cross.png" alt="Hapus Data" /></a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<?php
			}
			?>
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
