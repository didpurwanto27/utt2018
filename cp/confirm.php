<?php
include "config.php";
include "open_connection.php";
include "mahasiswa_model.php";
include "pembayaran_model.php";

$id_pembayaran = $_GET['id'];
$id_mhs = $_GET['id_mhs'];
$page = $_GET['page'];

$statusSmester = checkStatusSemester($id_mhs);

if($statusSmester){
	$confirm = confirmPembayaran($id_pembayaran, FALSE);
} else {
	$confirm = confirmPembayaran($id_pembayaran, TRUE);
}

header("location: pembayaran_pending.php?page=$page");

?>