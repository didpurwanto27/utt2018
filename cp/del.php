<?php
include "mahasiswa_model.php";

$id = $_GET['id'];
$jurusan = $_GET['jurusan'];
$angkatan = $_GET['angkatan'];

delMahasiswa($id);

if(!empty($jurusan) && !empty($angkatan))
	header('Location: mahasiswa.php?jurusan='.$jurusan.'&angkatan='.$angkatan);
else
	header('Location: mahasiswa.php');

?>