<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Control Panel</title>
	<meta name="generator" content="Studio 3 http://aptana.com/">
	<meta name="author" content="nedi">
	<link rel="stylesheet" href="css/style1.css" type="text/css" />
	<!-- Date: 2012-04-07 -->
</head>
<body>	
	<div id="wrapper">
		<div id="header">
			<div id="line-top"></div>
			<img src="img/logo.gif" alt="logo" />
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="link"><a href="mahasiswa.php" class="studentlink"></a></div>
			<div id="link"><a href="pendaftaran.php" class="daftarlink"></a></div>
			<div id="link"><a href="pembayaran.php" class="bayarlink"></a></div>
			<div id="link"><a href="links.php" class="linklink"></a></div>
			<div id="content-bottom"></div>
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
