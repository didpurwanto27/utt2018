<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mahasiswa</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
	<link type="text/css" href="css/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script> 
	<script>
		$(function(){
			// Datepicker
			$('#datepicker').datepicker();	
			$("#datepicker").datepicker(
				"option", {
					dateFormat: "yy-mm-dd"					
				}		
			);
			$("#datepicker").val("<?php echo $row['tgl_lhr_mhs']?>");
		});
	</script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li class="current"><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_add.php">Tambah Pembayaran</a></li>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">PEMBAYARAN</div>
			<div>
				<form action="mahasiswa-search.php" method="get">
					<fieldset>
						<legend>Dasar Pencarian</legend>
						<div>
							<label>ID</label>
							<input type="text" name="id" />
						</div>
						<div>
							<label>Nama</label>
							<input type="text" name="nama" />
						</div>
						<div>
							<label>Tanggal Pembayaran</label>
							<input type="text" name="tgl" id="datepicker" class="input100 inputdate" />
						</div>
					</fieldset>
					<button type="submit" name="submit" class="cari">Cari Data</button>
				</form>
			</div>
			
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
