<?php
include "session_function.php";
include "mahasiswa_model.php";
include "pendaftaran_model.php";

check_session();

$varjurusan = $_GET['jurusan'];
$varangkatan = $_GET['angkatan'];
$amount = 10;

if (!isset($_GET['page']))
	$page = 0;
else
	$page = $_GET['page'];

$jur = getJurusan();
$arrjurusan = array();
while($rowjur = mysql_fetch_array($jur, MYSQL_ASSOC)){
	$arrjurusan[$rowjur['id_progstudi']] = $rowjur['nama_progstudi'];
}

$angkatan = getAngkatan();
$aktif_semester = getAktifSemester();
	
if (isset($_GET['jurusan']) && isset($_GET['angkatan']))
{		
	$mahasiswa = getMahasiswa($varjurusan, $varangkatan, $page, $amount);
	$jumlah = getJmlMahasiswa($varjurusan, $varangkatan);
	$numpage = ceil($jumlah[0]/$amount);
}

include "mahasiswa_view.php";

?>