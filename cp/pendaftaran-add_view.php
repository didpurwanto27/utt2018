<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>UPDATE SEMESTER PENDAFTARAN</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link type="text/css" href="css/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li class="current"><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a></li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div id="content">
			<div id="header-content">TAMBAH SEMESTER PENDAFTARAN</div>
			<div>
				<form action="" method="post">
					<fieldset class="registrasi">
						<legend>Informasi Semester</legend>
						<div>
							<label>ID Semester</label>
							<input type="text" name="idsemester" />
						</div>
						<div>
							<label>Reg. Baru Mulai</label>
							<input type="text" name="regbarumulai" id="datepicker1" class="input100 inputdate" />
						</div>
						<div>
							<label>Reg. Baru Selesai</label>
							<input type="text" name="regbaruselesai" id="datepicker2" class="input100 inputdate" />
						</div>
						<div>
							<label>Reg. Lama Mulai</label>
							<input type="text" name="reglamamulai" id="datepicker3" class="input100 inputdate" />
						</div>
						<div>
							<label>Reg. Lama Selesai</label>
							<input type="text" name="reglamaselesai" id="datepicker4" class="input100 inputdate" />
						</div>
						<div>
							<label>Ujian Ulang Mulai</label>
							<input type="text" name="ujulangmulai" id="datepicker5" class="input100 inputdate" />
						</div>
						<div>
							<label>Ujian Ulang Selesai</label>
							<input type="text" name="ujulangselesai" id="datepicker6" class="input100 inputdate" />
						</div>
						
					</fieldset>
					<fieldset class="UkUoMaba">
						<div>
							<label>UK MaBa</label>
							<input type="text" name="nominal_uk_maba" id="nominal_uk_maba"/>
						</div>
						<div>
							<label>UO MaBa</label>
							<input type="text" name="nominal_uo_maba" id="nominal_uo_maba"/>
						</div>
						</div>
					</fieldset>
					<fieldset class="UkUoMaLa">
						<div>
							<label>UK MaLa</label>
							<input type="text" name="nominal_uk_mala" id="nominal_uk_mala"/>
						</div>
						<div>
							<label>UO MaLa</label>
							<input type="text" name="nominal_uo_mala" id="nominal_uo_mala"/>
						</div>
					</fieldset>
					<div>
					   <button type="submit" name="submit" class="edit">Update Semester</button>
					</div>
				</form>
			</div>			
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
