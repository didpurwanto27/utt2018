<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>VIEW INFORMASI MAHASISWA</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link type="text/css" href="css/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<script>
		$(function(){
			$('input').attr('disabled', 'disabled');
			$('button').click(function(){
				window.location = 'mahasiswa-edit.php?id=<?php echo $id ?>&jurusan=<?php echo $jurusan ?>&angkatan=<?php echo $angkatan ?>&page=<?php echo $page?>';
			});
		});
	</script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li class="current"><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">INFORMASI MAHASISWA</div>
			<div>
				<form action="" method="post">
					<fieldset class="registrasi">
						<legend>Informasi Registrasi</legend>
						<div>
							<label>NIM</label>
							<input type="text" name="nim" value="<?php echo $row['nim_mhs']?>" />
						</div>
						<div>
							<label>Nama Lengkap</label>
							<input type="text" name="nama" value="<?php echo $row['nama_mhs']?>" />
						</div>
						<div>
							<label>Alamat</label>
							<input type="text" name="alamat" value="<?php echo $row['alamat_mhs']?>" />
						</div>
						<div>
							<label>District</label>
							<input type="text" name="district" value="<?php echo $row['district_mhs']?>" />
						</div>
						<div>
							<label>Kota</label>
							<input type="text" name="kabkot" value="<?php echo $row['kabkot_mhs']?>" />
						</div>
						<div>
							<label>Kodepos</label>
							<input type="text" name="kodepos" value="<?php echo $row['kodepos_mhs']?>" />
						</div>
						<div>
							<label>No Telepon</label>
							<input type="text" name="telp" value="<?php echo $row['telp_mhs']?>" />
						</div>
						<div>
							<label>Handphone</label>
							<input type="text" name="hp" value="<?php echo $row['cellphone_mhs']?>" />
						</div>
						<div>
							<label>Email</label>
							<input type="text" name="email" value="<?php echo $row['email_mhs']?>" />
						</div>
						<div>
							<label>Tanggal Lahir</label>
							<input type="text" name="tgllahir" value="<?php echo $row['tgl_lhr_mhs']?>" readonly="readonly" />
						</div>
						<div>
							<label>Tempat Lahir</label>
							<input type="text" name="tmplahir" value="<?php echo $row['tmp_lhr_mhs']?>" />
						</div>
						<div>
							<label>Agama</label>
							<input type="text" name="agama" value="<?php echo $row['agama_mhs']?>"/>						
						</div>
						<div>
							<label>Program Studi</label>
							<input type="text" name="prodi" value="<?php echo $row['nama_progstudi']?>"/>
						</div>
						<div>
							<label>Kode UPBJJ</label>
							<input type="text" name="upbjj" value="<?php echo $row['kdupbjj_mhs']?> Jakarta" disabled="disable" />
						</div>						
						<div>
							<label>Jenis Kelamin</label>
							<input type="text" name="jk" value="<?php echo $row['jk_mhs']?>"/>
						</div>
						<div>
							<label>Kewarganegaraan</label>
							<input type="text" name="warganegara" value="<?php echo $row['wn_mhs']?>"/>
						</div>
						<div>
							<label>Pekerjaan</label>
							<input type="text" name="pekerjaan" value="<?php echo $row['pekerjaan_mhs']?>"/>
						</div>
						<div>
							<label>Status Pernikahan</label>
							<input type="text" name="kawin" value="<?php echo $row['stkwn_mhs']?>"/>
						</div>
					</fieldset>
					<fieldset class="pendidikan">
						<legend>Pendidikan Terakhir</legend>
						<div>
							<label>Jenjang</label>
							<input type="text" name="jenjang" value="<?php echo $row['jenjangpdk_mhs']?>"/>
						</div>
						<div>
							<label>Jurusan</label>
							<input type="text" name="jurusan" value="<?php echo $row['jurusan_mhs']?>"/>
						</div>
						<div>
							<label>Tahun Ijazah</label>
							<input type="text" name="thnijazah" value="<?php echo $row['thnijazah_mhs']?>" />
						</div>
						<div>
							<label>Nama Ibu</label>
							<input type="text" name="ibu" value="<?php echo $row['namaibu_mhs']?>" />
						</div>
						<div>
							<label>No. Rekening</label>
							<input type="text" name="rekening" value="<?php echo $row['norekening_mhs']?>" />
						</div>
						<div>
							<label>Nama Bank</label>
							<input type="text" name="bank" value="<?php echo $row['bank_mhs']?>" />
						</div>
					</fieldset>
					<fieldset>
						<legend>Informasi Lainnya</legend>
						<div>
							<label>Nama Facebook</label>
							<input type="text" name="fb" value="<?php echo $row['namafb_mhs']?>" />
						</div>
						<div>
							<label>Angkatan</label>
							<input type="text" name="angkatan" value="<?php echo $row['angkatan_mhs']?>" />
						</div>
						<div>
							<label>Semester Terakhir</label>
							<input type="text" name="semester" value="<?php echo $row['semester_aktif']?>" />
						</div>
						<div>
							<label><b>Tempat Ujian</b></label>
							<input type="text" name="tempat_ujian" value="<?php print $row['tempatujian']?>" />
						</div>
						<div>
							<label><b>Status Mahasiswa</b></label>
							<input type="text" name="status" value="<?php print $status[$row['aktif']]?>" />
						</div>
					</fieldset>
          		<div>
               		<button type="button" class="edit">Update Informasi</button>
          		</div>
				</form>
			</div>			
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
