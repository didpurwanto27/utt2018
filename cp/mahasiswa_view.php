<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mahasiswa</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li class="current"><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">MAHASISWA</div>
			<div>
				<form action="mahasiswa.php" method="get">
					Jurusan : 
					<select id="jurusan" name="jurusan">
					  <option value="54" <?php if (isset($_GET['jurusan']) && $_GET['jurusan'] == '54') echo 'selected' ?>>Manajemen</option>
					  <option value="72" <?php if (isset($_GET['jurusan']) && $_GET['jurusan'] == '72') echo 'selected' ?>>Ilmu Komunikasi</option>
					  <option value="87" <?php if (isset($_GET['jurusan']) && $_GET['jurusan'] == '87') echo 'selected' ?>>Bahasa Inggris (Penerjemah)</option>
					</select>
					Angkatan : 
					<select id="angkatan" name="angkatan">
						<?php
						while ($row = mysql_fetch_array($angkatan, MYSQL_ASSOC)) {
						?>
						<option value="<?php echo $row['angkatan_mhs']?>" <?php if (isset($_GET['angkatan']) && $_GET['angkatan'] == $row['angkatan_mhs']) echo 'selected' ?>>
							<?php 
								$str = $row['angkatan_mhs'];
								$len = strlen($str);
								
								if($len>4){
									$yearangkatan = substr($str, 0, 4);
									$semangkatan  = substr($str, -1);
									
									$str = $yearangkatan.".".$semangkatan;
									print $str;
								}else {
									print $str;
								}
							?>
						</option>
						<?php
						}
						?>
					</select>
					<button type="submit">Lihat Data</button>
				</form>
			</div>
			<div>
				<?php
				if(isset($_GET['jurusan'])) {
					if($jumlah[0] == 0) {
						$str_angkatan = $varangkatan;
						$len_angkatan = strlen($str_angkatan);
							
						$year = substr($str_angkatan, 0, 4);
						$sem  = substr($str_angkatan, -1);
						$str_angkatan = $year.".".$sem;
						print "<p>Data jurusan <strong>".$arrjurusan[$varjurusan]."</strong> angkatan <strong>".$str_angkatan."</strong> Kosong</p>";
					}else {						
				?>
				<table id="background-image" width="100%">
				    <thead>
				    	<tr>
				        	<th scope="col">ID</th>
				            <th scope="col">NIM</th>
				            <th scope="col">NAMA</th>
							<th scope="col">Email</th>
							<th scope="col">HP</th>
							<th scope="col">FB</th>
				            <th scope="col">Registrasi?</th>
							<th scope="col">Agama</th>
							<th scope="col">Ujian</th>
				            <th scope="col">Operation</th>
				        </tr>
				    </thead>
				    <tfoot>
				    	<tr>
				        	<td colspan="6">
				        		|
				        		<?php
				        			for($i=0; $i<$numpage; $i++) {
				        				if($page == ($i*$amount)) 
											$style = "cur";
										else
											$style = "ncur";
				        		?>
				        		 <a href="mahasiswa.php?jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan ?>&page=<?php echo $i*$amount ?>">
				        		 	<span class="<?php echo $style ?>">
				        		 	<?php echo $i+1; ?>
				        		 	</span>
				        		 </a> |
								 
				        		<?php
									}
				        		?>
								
				        	</td>
				        </tr>
						<tr>
							<td colspan="7">
								<a href="excel.php?jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan ?>&page=<?php echo $page ?>">
									<strong>Download Fomulir halaman ini</strong>
								</a>
								<?php if ($page == 0) { ?> | <a href="excel2.php?jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan ?>&page=<?php echo $page ?>"><strong>Download Alamat</strong></a> | <a href="excel5.php?jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan ?>&page=<?php echo $page ?>"><strong>Download List Pemohon Ujian Ulang</strong></a> <?php } ?>
							</td>							
						</tr>
				    </tfoot>
				    <tbody>
				    	<?php				    	
				    	while ($rowmahasiswa = mysql_fetch_array($mahasiswa, MYSQL_ASSOC)) {
				    	?>
				    	<tr>
				        	<td class="operation"><a href="mahasiswa-detail.php?id=<?php echo $rowmahasiswa['id_mhs']?>&jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan?>&page=<?php echo $page?>"><?php print $rowmahasiswa['id_mhs'] ?></a></td>
				            <td class="operation"><?php print ucwords(strtolower($rowmahasiswa['nim_mhs'])) ?></td>
				            <td><?php print $rowmahasiswa['nama_mhs'] ?></td>
							<td><?php print $rowmahasiswa['email_mhs'] ?></td>
							<td><?php print $rowmahasiswa['cellphone_mhs'] ?></td>
							<td><?php print $rowmahasiswa['namafb_mhs'] ?></td>
				            <td class="operation"><?php print $rowmahasiswa['semester_aktif'] ?></td>
							<td class="operation"><?php print $rowmahasiswa['agama_mhs'] ?></td>
							<td class="operation"><?php print $rowmahasiswa['tempatujian'] ?></td>
				            <td class="operation">
				            	<a href="mahasiswa-edit.php?id=<?php echo $rowmahasiswa['id_mhs'] ?>&jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan ?>&page=<?php echo $page?>"><img src="img/edit.png" alt="Edit Data" /></a>
				            	<a href="del.php?id=<?php echo $rowmahasiswa['id_mhs']?>&jurusan=<?php echo $varjurusan ?>&angkatan=<?php echo $varangkatan?>" onclick="return confirm('Apakah ingin menghapus data ini?')"><img src="img/cross.png" alt="Hapus Data" /></a>
				            </td>
				        </tr>
				        <?php
						}
				        ?>
				    </tbody>
				</table>
				<?php
					}
				}
				?>
			</div>
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>