<?php
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel2007.php';
include "config.php";
include "open_connection.php";
include "mahasiswa_model.php";

$varjurusan = $_GET['jurusan'];
$varangkatan = $_GET['angkatan'];

$curday = date('j');
$curmonth = date('n');
$curyear = date('Y');
$cur_month = array (
				'1' => 'JANUARI', '2' => 'FEBRUARI', '3' => 'MARET', '4' => 'APRIL', '5' => 'MEI', '6' => 'JUNI',
				'7' => 'JULI', '8' => 'AGUSTUS', '9' => 'SEPTEMBER', '10' => 'OKTOBER', '11' => 'NOVEMBER', '12' => 'DESEMBER'
			);

$curdate = "   ".$curday." ".$cur_month[$curmonth]." ".$curyear;

$objPHPExcel = new PHPExcel();

//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
$objPHPExcel->getProperties()->setLastModifiedBy("Junaidillah Fadlil");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

$listAlamat = getListAlamat($varjurusan, $varangkatan);

$objPHPExcel->getActiveSheet()->setTitle("Alamat");

//Set Page Size
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

//Set Page Margin
$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.63);
$objPHPExcel->getActiveSheet()->getPageMargins()->setHeader(0.31496062992126);
$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.18);
$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.24);
$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.46);
$objPHPExcel->getActiveSheet()->getPageMargins()->setFooter(0.31496062992126);

// Style
$styleTitle1 = array (
					'font' => array ('bold' => true),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$styleTitle23 = array (
					'font' => array ('bold' => true, 'size' => 14),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$styleTitle4 = array (
					'font' => array ('bold' => true, 'size' => 12),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
						'bottom' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
					),
				);
$styleAllborder = array (
					'borders' => array (
									'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
								),
				); 

$stylefont10 = array (
					'font' => array ('size' => 10),
				);

$stylefont10bold = array (
					'font' => array ('bold' => true, 'size' => 10),
				);

$stylefont85 = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);
				
$stylefont85bold = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);

$stylefont85center = array (
					'font' => array ('bold' => true, 'size' => 8.5),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
								'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
							),
				);

$stylecenter = array (
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$styletopborder = array (
					'font' => array ('bold' => true, 'size' => 10),
					'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'wrap' => true
					),
					'borders' => array (
						'top' => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
						'left' => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
						'right' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
					)
				);
				
$stylemidborder = array (
					'font' => array ('bold' => true, 'size' => 10),
					'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'wrap' => true
					),
					'borders' => array (
						'left' => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
						'right' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
					)
				);
				
$stylebtmborder = array (
					'font' => array ('bold' => true, 'size' => 10),
					'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'wrap' => true
					),
					'borders' => array (
						'left' => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
						'right' => array ('style' => PHPExcel_Style_Border::BORDER_THIN),
						'bottom' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
					)
				);

//echo date('H:i:s') . " Add some data\n";

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(43);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(43);

$count = 0;

while($rowList = mysql_fetch_array($listAlamat, MYSQL_ASSOC))
{	
	$pengirim = "From: Badan Pelaksana UT Taiwan\nPrimasari Cahya Wardhani (0938672639)\nDorm 3, Room 608, NTUST, No. 43 Sec. 4 Keelung Rd, Da'an District, Taipei 106, Taiwan, ROC";
	$penerima = "To: " . $rowList['NAMA_MHS']. ' ('. $rowList['CELLPHONE_MHS'].')';
	$alamat = $rowList['ALAMAT_MHS'] . ', ' . $rowList['DISTRICT_MHS'] . ', ';
	$alamat .= $rowList['KABKOT_MHS'] . ' ' . $rowList['KODEPOS_MHS'];
	
	if ($count % 2 == 0) {
		$rowPengirim = $count * 2 + 1;
		$boxPengirim = 'B' . $rowPengirim;
		
		$rowNama = $count * 2 + 2;
		$boxNama = 'B' . $rowNama;
		
		$rowAlamat = $count * 2 + 3;
		$boxAlamat = 'B' . $rowAlamat;
	}
	else {
		$rowPengirim = ($count - 1) * 2 + 1;
		$boxPengirim = 'D' . $rowPengirim;
		
		$rowNama = ($count - 1) * 2 + 2;
		$boxNama = 'D' . $rowNama;
		
		$rowAlamat = ($count - 1) * 2 + 3;
		$boxAlamat = 'D' . $rowAlamat;
	}
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxPengirim, $pengirim);
	$objPHPExcel->getActiveSheet()->getStyle($boxPengirim)->applyFromArray($styletopborder);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxNama, $penerima);
	$objPHPExcel->getActiveSheet()->getStyle($boxNama)->applyFromArray($styletopborder);
	
	$objPHPExcel->getActiveSheet()->getRowDimension($rowAlamat)->setRowHeight(36);
	$objPHPExcel->getActiveSheet()->SetCellValue($boxAlamat, $alamat);
	$objPHPExcel->getActiveSheet()->getStyle($boxAlamat)->applyFromArray($stylebtmborder);

	$count++;
}
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

ob_end_clean();
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Alamat-'.$varjurusan.'-'.$varangkatan.'.xlsx"');

$objWriter->save('php://output');
exit();
//$objPHPExcel->disconnectWorksheets();
//unset($objPHPExcel);


?> 