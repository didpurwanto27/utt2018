<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>TAMBAH MAHASISWA</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link type="text/css" href="css/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<script>
		$(function(){
			// Datepicker
			$('#datepicker').datepicker();	
			$("#datepicker").datepicker(
				"option", {
					dateFormat: "yy-mm-dd"					
				}		
			);
			$("#datepicker").val("1991-01-01");
		});
	</script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li class="current"><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li><a href="pembayaran.php">Pembayaran</a>						
							<ul>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
			<div id="header-content">TAMBAH MAHASISWA</div>
			<div>
				<form action="" method="post">
					<fieldset class="registrasi">
						<legend>Informasi Registrasi</legend>
						<div>
							<label>ID</label>
							<input type="text" name="id" value="<?php echo $id?>" readonly="readonly" />
						</div>
						<div>
							<label>NIM</label>
							<input type="text" name="nim" value="<?php echo $row['nim_mhs']?>" />
						</div>
						<div>
							<label>Nama Lengkap</label>
							<input type="text" name="nama" value="<?php echo $row['nama_mhs']?>" />
						</div>
						<div>
							<label>Alamat</label>
							<input type="text" name="alamat" value="<?php echo $row['alamat_mhs']?>" />
						</div>
						<div>
							<label>District</label>
							<input type="text" name="district" value="<?php echo $row['district_mhs']?>" />
						</div>
						<div>
							<label>Kota</label>
							<input type="text" name="kabkot" value="<?php echo $row['kabkot_mhs']?>" />
						</div>
						<div>
							<label>Kodepos</label>
							<input type="text" name="kodepos" value="<?php echo $row['kodepos_mhs']?>" />
						</div>
						<div>
							<label>No Telepon</label>
							<input type="text" name="telp" value="<?php echo $row['telp_mhs']?>" />
						</div>
						<div>
							<label>Handphone</label>
							<input type="text" name="hp" value="<?php echo $row['cellphone_mhs']?>" />
						</div>
						<div>
							<label>Email</label>
							<input type="text" name="email" value="<?php echo $row['email_mhs']?>" />
						</div>
						<div>
							<label>Tanggal Lahir</label>
							<select name="tgllhr">
								<?php for($i = 1; $i <= 31; $i++) {
								if($i < 10)	{$i = "0".$i;}?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php }?>						
							</select>&nbsp;
                            <select name="blnlhr">
								<option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">Nopember</option>
                                <option value="12">Desember</option>			
							</select>&nbsp;
                            <select name="thnlhr">
								<?php for($i = 1960; $i <= date("Y"); $i++) {?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php }?>						
							</select>
						</div>
						<div>
							<label>Tempat Lahir</label>
							<input type="text" name="tmplahir" value="<?php echo $row['tmp_lhr_mhs']?>" />
						</div>
						<div>
							<label>Agama</label>
							<select name="agama">
								<option value="islam" <?php if($row['agama_mhs'] == "islam") echo "selected"; ?>>Islam</option>
								<option value="katolik" <?php if($row['agama_mhs'] == "katolik") echo "selected"; ?>>Katolik</option>
								<option value="protestan" <?php if($row['agama_mhs'] == "protestan") echo "selected"; ?>>Protestan</option>
								<option value="hindu" <?php if($row['agama_mhs'] == "hindu") echo "selected"; ?>>Hindu</option>
								<option value="budha" <?php if($row['agama_mhs'] == "budha") echo "selected"; ?>>Budha</option>								
							</select>							
						</div>
						<div>
							<label>Program Studi</label>
							<select name="prodi">
								<option value="54" <?php if($row['progstudi_id'] == "54") echo "selected"; ?>>Manajemen</option>
								<option value="72" <?php if($row['progstudi_id'] == "72") echo "selected"; ?>>Ilmu Komunikasi</option>
								<option value="87" <?php if($row['progstudi_id'] == "87") echo "selected"; ?>>Bahasa Inggris (Penerjemah)</option>								
							</select>
						</div>
						<div>
							<label>Kode UPBJJ</label>
							<input type="text" name="upbjj" value="21" readonly="readonly" />
						</div>						
						<div>
							<label>Jenis Kelamin</label>
							<select name="jk">
								<option value="Pria" <?php if($row['jk_mhs'] == "Pria") echo "selected"; ?>>Pria</option>
								<option value="Wanita" <?php if($row['jk_mhs'] == "Wanita") echo "selected"; ?>>Wanita</option>						
							</select>
						</div>
						<div>
							<label>Kewarganegaraan</label>
							<select name="warganegara">
								<option value="WNI" <?php if($row['wn_mhs'] == "WNI") echo "selected"; ?>>WNI</option>
								<option value="Asing" <?php if($row['wn_mhs'] == "Asing") echo "selected"; ?>>Asing</option>						
							</select>
						</div>
						<div>
							<label>Pekerjaan</label>
							<select name="pekerjaan">
								<option value="Swasta / TKI" <?php if($row['pekerjaan_mhs'] == "Swasta / TKI") echo "selected"; ?>>Swasta / TKI</option>
								<option value="TNI/Polri" <?php if($row['pekerjaan_mhs'] == "TNI/Polri") echo "selected"; ?>>TNI/Polri</option>
								<option value="Wiraswasta" <?php if($row['pekerjaan_mhs'] == "Wiraswasta") echo "selected"; ?>>Wiraswasta</option>
								<option value="Tidak Bekerja" <?php if($row['pekerjaan_mhs'] == "Tidak Bekerja") echo "selected"; ?>>Tidak Bekerja</option>
								<option value="PNS" <?php if($row['pekerjaan_mhs'] == "PNS") echo "selected"; ?>>PNS</option>						
							</select>
						</div>
						<div>
							<label>Status Pernikahan</label>
							<select name="kawin">
								<option value="Menikah" <?php if($row['stkwn_mhs'] == "Menikah") echo "selected"; ?>>Menikah</option>
								<option value="Tidak Menikah" <?php if($row['stkwn_mhs'] == "Tidak Menikah") echo "selected"; ?>>Tidak Menikah</option>						
							</select>
						</div>
					</fieldset>
					<fieldset class="pendidikan">
						<legend>Pendidikan Terakhir</legend>
						<div>
							<label>Jenjang</label>
							<select name="jenjang">
								<option value="SLTA" <?php if($row['jenjangpdk_mhs'] == "SLTA") echo "selected"; ?>>SLTA</option>
								<option value="D-I" <?php if($row['jenjangpdk_mhs'] == "D-I") echo "selected"; ?>>D-I</option>	
								<option value="D-II" <?php if($row['jenjangpdk_mhs'] == "D-II") echo "selected"; ?>>D-II</option>
								<option value="D-III" <?php if($row['jenjangpdk_mhs'] == "D-III") echo "selected"; ?>>D-III</option>		
							</select>
						</div>
						<div>
							<label>Jurusan</label>
							<select name="jurusan">
								<option value="SMTA Umum IPA / IPS" <?php if($row['jurusan_mhs'] == "SMTA Umum IPA / IPS") echo "selected"; ?>>SMTA Umum IPA / IPS</option>
								<option value="STM/SMTA" <?php if($row['jurusan_mhs'] == "STM/SMTA") echo "selected"; ?>>STM/SMTA</option>	
								<option value="SMK/SMKA" <?php if($row['jurusan_mhs'] == "SMK/SMKA") echo "selected"; ?>>SMK/SMKA</option>
								<option value="SMEA" <?php if($row['jurusan_mhs'] == "SMEA") echo "selected"; ?>>SMEA</option>		
								<option value="SPMA" <?php if($row['jurusan_mhs'] == "SPMA") echo "selected"; ?>>SPMA</option>		
								<option value="SPG/SPGLB/SGO/SMOA" <?php if($row['jurusan_mhs'] == "SPG/SPGLB/SGO/SMOA") echo "selected"; ?>>SPG/SPGLB/SGO/SMOA</option>	
							</select>
						</div>
						<div>
							<label>Tahun Ijazah</label>
							<input type="text" name="thnijazah" value="<?php echo $row['thnijazah_mhs']?>" />
						</div>
						<div>
							<label>Nama Ibu</label>
							<input type="text" name="ibu" value="<?php echo $row['namaibu_mhs']?>" />
						</div>
						<div>
							<label>No. Rekening</label>
							<input type="text" name="rekening" value="<?php echo $row['norekening_mhs']?>" />
						</div>
						<div>
							<label>Nama Bank</label>
							<input type="text" name="bank" value="<?php echo $row['bank_mhs']?>" />
						</div>
					</fieldset>
					<fieldset>
						<legend>Informasi Lainnya</legend>
						<div>
							<label>Nama Facebook</label>
							<input type="text" name="fb" value="<?php echo $row['namafb_mhs']?>" />
						</div>
						<div>
							<label>Angkatan</label>
							<input type="text" name="angkatan" value="<?php echo $row['angkatan_mhs']?>" />
						</div>
						<div>
							<label>Semester Terakhir</label>
							<input type="text" name="semester" value="<?php echo $row['semester_aktif']?>" />
						</div>
						<div>
							<label>Tempat Ujian</label>
							<select name="tempatujian">
								<option value="1" <?php if($row['id_tempatujian'] == "1") echo "selected"; ?>>Taipei</option>
								<option value="2" <?php if($row['id_tempatujian'] == "2") echo "selected"; ?>>Tainan</option>
								<option value="3" <?php if($row['id_tempatujian'] == "3") echo "selected"; ?>>Indonesia</option>								
							</select>
						</div>
					</fieldset>
          		<div>
               		<button type="submit" class="add">Tambah Mahasiswa</button>
          		</div>
				</form>
			</div>			
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>
	</div>
</body>
</html>
