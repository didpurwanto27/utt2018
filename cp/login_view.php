<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>CP Login Page</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/login.css" />
	<link type="text/css" href="css/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function()) {
			$('#user').focus();
		}
	</script>
</head>
<body>
	<div class="header">
		<div class="logobound">
			<div class="container">
				<img src="img/loginpagelogo.jpg" alt="logo"/>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<form action="" method="POST">
				<label>Username</label>
				<input type="text" name="username" id="user" />
				<label>Password</label>
				<input type="password" name="password" />
				<input type="submit" class="mybutton" value="Sign In" />
			</form>
		</div>
		<?php
			if($error) {				
		?>
			<div class="myerror">Username & Password Salah !</div>
		<?php
			}
		?>
	</div>
</body>
</html>