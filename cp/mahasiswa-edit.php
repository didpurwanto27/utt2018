<?php
include "mahasiswa_model.php";

$id = $_GET['id'];
$jurusan = $_GET['jurusan'];
$angkatan = $_GET['angkatan'];
$page = $_GET['page'];

if($_POST['nama'] && $_POST['agama']) {
	
	$update = updateMahasiswa($id, $_POST);
	
	if($update){
		if(!empty($jurusan) && !empty($angkatan))
			header("location: mahasiswa.php?jurusan=".$jurusan."&angkatan=".$angkatan."&page=".$page);
		else
			header("location: mahasiswa.php");
	}	
}

$status = array(
				"0" => 'Belum Update',
				"1" => 'Aktif',
				"2" => 'Cuti',
				"3" => 'Berhenti/ke Indonesia',
				"NULL" => 'Belum Update'
			);

$mahasiswa = infoMahasiswa($id);
$row = mysql_fetch_assoc($mahasiswa);

include "mahasiswa-edit_view.php";

?>