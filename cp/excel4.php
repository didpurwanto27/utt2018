<?php
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel2007.php';
include "config.php";
include "open_connection.php";
include "mahasiswa_model.php";

$varjurusan = $_GET['jurusan'];
$varangkatan = $_GET['angkatan'];

$curday = date('j');
$curmonth = date('n');
$curyear = date('Y');
$cur_month = array (
				'1' => 'JANUARI', '2' => 'FEBRUARI', '3' => 'MARET', '4' => 'APRIL', '5' => 'MEI', '6' => 'JUNI',
				'7' => 'JULI', '8' => 'AGUSTUS', '9' => 'SEPTEMBER', '10' => 'OKTOBER', '11' => 'NOVEMBER', '12' => 'DESEMBER'
			);

$curdate = "   ".$curday." ".$cur_month[$curmonth]." ".$curyear;

$objPHPExcel = new PHPExcel();

//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
$objPHPExcel->getProperties()->setLastModifiedBy("Junaidillah Fadlil");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

$objPHPExcel->getActiveSheet()->setTitle("List Mahasiswa");

//Set Page Size
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

//Set Page Margin
$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.63);
$objPHPExcel->getActiveSheet()->getPageMargins()->setHeader(0.31496062992126);
$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.18);
$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.24);
$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.46);
$objPHPExcel->getActiveSheet()->getPageMargins()->setFooter(0.31496062992126);

// Style
$styleTitle1 = array (
					'font' => array ('bold' => true),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$styleTitle23 = array (
					'font' => array ('bold' => true, 'size' => 14),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$styleTitle4 = array (
					'font' => array ('bold' => true, 'size' => 12),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
						'bottom' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
					),
				);
$styleAllborder = array (
					'borders' => array (
									'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
								),
				); 

$stylefont10 = array (
					'font' => array ('size' => 10),
				);

$stylefont10bold = array (
					'font' => array ('bold' => true, 'size' => 10),
				);

$stylefont85 = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);
				
$stylefont85bold = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);

$stylefont85center = array (
					'font' => array ('bold' => true, 'size' => 8.5),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
								'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
							),
				);

$stylecenter = array (
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

$stylebox = array (
					'font' => array ('size' => 10),
					'alignment' => array (
						//'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'wrap' => true
					),
					'borders' => array (
									'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
					)
				);
				

//echo date('H:i:s') . " Add some data\n";

$listAbsensi = getListAbsensi($varjurusan, $varangkatan);
$count = 1;

$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->SetCellValue("B1", "No");
$objPHPExcel->getActiveSheet()->getStyle("B1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
$objPHPExcel->getActiveSheet()->SetCellValue("C1", "Nama Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("C1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
$objPHPExcel->getActiveSheet()->SetCellValue("D1", "Alamat Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("D1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->SetCellValue("E1", "Telp Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("E1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->SetCellValue("F1", "No HP Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("F1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->SetCellValue("G1", "Email Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("G1")->applyFromArray($stylebox);

$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->SetCellValue("H1", "FB Mahasiswa");
$objPHPExcel->getActiveSheet()->getStyle("H1")->applyFromArray($stylebox);

while($rowList = mysql_fetch_array($listAbsensi, MYSQL_ASSOC))
{	
	$alamat = $rowList['ALAMAT_MHS'] . ', ' . $rowList["DISTRICT_MHS"] . ', ';
	$alamat .= $rowList["KABKOT_MHS"] . ' ' . $rowList["KODEPOS_MHS"];
	
	$rowNo = $count + 1;
	$boxNo = 'B' . $rowNo;
	$boxNama = 'C' . $rowNo;
	$boxAlamat = 'D' . $rowNo;
	$boxTelp = 'E' . $rowNo;
	$boxHape = 'F' . $rowNo;
	$boxEmail = 'G' . $rowNo;
	$boxFB = 'H' . $rowNo;

	$objPHPExcel->getActiveSheet()->SetCellValue($boxNo, $count);
	$objPHPExcel->getActiveSheet()->getStyle($boxNo)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxNama, $rowList['NAMA_MHS']);
	$objPHPExcel->getActiveSheet()->getStyle($boxNama)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxAlamat, $alamat);
	$objPHPExcel->getActiveSheet()->getStyle($boxAlamat)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxTelp, $rowList['TELP_MHS']);
	$objPHPExcel->getActiveSheet()->getStyle($boxTelp)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxHape, $rowList['CELLPHONE_MHS']);
	$objPHPExcel->getActiveSheet()->getStyle($boxHape)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxEmail, $rowList['EMAIL_MHS']);
	$objPHPExcel->getActiveSheet()->getStyle($boxEmail)->applyFromArray($stylebox);
	
	$objPHPExcel->getActiveSheet()->SetCellValue($boxFB, $rowList['NAMAFB_MHS']);
	$objPHPExcel->getActiveSheet()->getStyle($boxFB)->applyFromArray($stylebox);

	$count++;
}
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

ob_end_clean();
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="ListMahasiswa-'.$varjurusan.'-'.$varangkatan.'.xlsx"');

$objWriter->save('php://output');
exit();
//$objPHPExcel->disconnectWorksheets();
//unset($objPHPExcel);


?> 