<?php
include "config.php";
include "open_connection.php";

function getJurusan()
{
	$sql = "SELECT id_progstudi, nama_progstudi ";
	$sql .= "FROM Program_Studi ";
	
	$result = mysql_query($sql);
	
	return $result;
}

function getMahasiswa($jurusan, $angkatan, $page, $amount)
{
	$sql = "SELECT m.id_mhs, m.nim_mhs, m.nama_mhs, m.cellphone_mhs, m.email_mhs, m.namafb_mhs, m.agama_mhs, t.tempatujian, m.semester_aktif ";
	$sql .= "FROM Mahasiswa m, Tempat_Ujian t ";
	$sql .= "WHERE m.progstudi_id = $jurusan ";
	$sql .= "     and m.angkatan_mhs = $angkatan ";
	$sql .= "     and m.id_tempatujian = t.id_tempatujian ORDER BY m.nama_mhs ASC ";
	$sql .= "LIMIT $page, $amount";
	$result = mysql_query($sql);
		
	if(!$result){
		print $sql."\n";
		die('Invalid query '.mysql_error());
	
	}
	
	return $result;
}

function getDetailMahasiswa($jurusan, $angkatan, $page, $amount)
{
	$sql = "SELECT * FROM Mahasiswa ";
	$sql .= "WHERE PROGSTUDI_ID = $jurusan";
	$sql .= "     AND ANGKATAN_MHS = $angkatan ORDER BY nama_mhs ASC ";
	$sql .= " LIMIT $page, $amount";	
	$result = mysql_query($sql);
		
	if(!$result){
		print $sql."\n";
		die('Invalid query '.mysql_error());
	
	}
	
	return $result;
}

function getDetailMahasiswaAll($jurusan, $angkatan)
{
	$sql = "SELECT * FROM Mahasiswa ";
	$sql .= "WHERE PROGSTUDI_ID = $jurusan";
	$sql .= "     AND ANGKATAN_MHS = $angkatan";
	$result = mysql_query($sql);

	if(!$result){
		print $sql."\n";
		die('Invalid query '.mysql_error());
	}
	
	return $result;
}

function getJmlMahasiswa($jurusan, $angkatan) {
	$sql = "SELECT COUNT(*) FROM Mahasiswa ";
	$sql .= "WHERE PROGSTUDI_ID = $jurusan";
	$sql .= "     AND ANGKATAN_MHS = $angkatan";
	
	$result = mysql_query($sql);
	
	$result = mysql_fetch_row($result);
	
	return $result;
}

function infoMahasiswa($id) {
  $sql = "SELECT DISTINCT m.nim_mhs, m.nama_mhs, m.alamat_mhs, m.district_mhs, m.kabkot_mhs, m.progstudi_id, ";
  $sql .= "m.kodepos_mhs, m.telp_mhs, m.cellphone_mhs, m.email_mhs, m.tgl_lhr_mhs, m.tmp_lhr_mhs, ";
  $sql .= "m.agama_mhs, p.nama_progstudi, m.kdupbjj_mhs, m.jk_mhs, m.wn_mhs, ";
  $sql .= "m.pekerjaan_mhs, m.stkwn_mhs, m.jenjangpdk_mhs, m.jurusan_mhs, ";
  $sql .= "m.thnijazah_mhs, m.namaibu_mhs, m.norekening_mhs, m.bank_mhs, m.angkatan_mhs, ";
  $sql .= "m.namafb_mhs, m.semester_aktif, m.aktif, t.tempatujian, m.id_tempatujian ";
  $sql .= "FROM Mahasiswa m, Program_Studi p, Tempat_Ujian t ";
  $sql .= "WHERE id_mhs = '$id' AND m.progstudi_id = p.id_progstudi AND m.id_tempatujian = t.id_tempatujian";
  $result = mysql_query($sql);
  
    
  return $result;
}

function getListAbsensi($jurusan, $angkatan) {
	$sql = "SELECT NIM_MHS, NAMA_MHS, ALAMAT_MHS, DISTRICT_MHS, KABKOT_MHS, KODEPOS_MHS, TELP_MHS, CELLPHONE_MHS, EMAIL_MHS, NAMAFB_MHS FROM Mahasiswa, Semester ";
	$sql .= "WHERE PROGSTUDI_ID = $jurusan AND ANGKATAN_MHS = $angkatan ";
	$sql .= "     AND SEMESTER_AKTIF = ID_SEMESTER ";
	$sql .= "     AND ACTIVE_SEMESTER = 1 ";
	$result = mysql_query($sql);
	
	return $result;
}
 
function getListAlamat ($jurusan, $angkatan) {
	$sql = "SELECT NAMA_MHS, CELLPHONE_MHS, ALAMAT_MHS, DISTRICT_MHS, KABKOT_MHS, KODEPOS_MHS FROM Mahasiswa, Semester ";
	$sql .= "WHERE PROGSTUDI_ID = $jurusan and ANGKATAN_MHS = $angkatan and Mahasiswa.semester_aktif = Semester.id_semester and Semester.active_semester = 1 ";
	//$sql .= "     AND SEMESTER_AKTIF = ID_SEMESTER ";
	//$sql .= "     AND ACTIVE_SEMESTER = 1 ";
	
	$result = mysql_query($sql);
	
	return $result;
}

function getListSusulan ($jurusan, $angkatan) {
	$sql = "SELECT Mahasiswa.NIM_MHS as Nim_Mhs, Mahasiswa.NAMA_MHS as Nama_Mhs, Program_Studi.Nama_ProgStudi as Jurusan, Mahasiswa.Angkatan_Mhs as Angkatan, Mata_Kuliah.Kode_MK as Kode_MK, Mata_Kuliah.Nama_Mk as Nama_Mk, Mata_Kuliah.Sks as Sks, Mata_Kuliah.Semester as SemesterAjar, Mata_Kuliah.Jadwal_Ujian as Jadwal_Ujian,
	        Mata_Kuliah.TTM as TTM FROM Mata_Kuliah, Mahasiswa, MK_Susulan, Semester, Program_Studi ";
	$sql .= "WHERE Mahasiswa.Progstudi_id = Program_Studi.ID_ProgStudi AND Mata_Kuliah.ID_Mk = MK_Susulan.ID_Mk and Mahasiswa.ID_Mhs = MK_Susulan.ID_Mhs and Mahasiswa.PROGSTUDI_ID = $jurusan and Mahasiswa.ANGKATAN_MHS = $angkatan";
	$sql .= "  AND MK_Susulan.SEMESTER_AKTIF = Semester.ID_SEMESTER AND Semester.Active_Semester = 1 Order By Nama_Mhs";
	
	$result = mysql_query($sql);
	
	return $result;
}

function updateMahasiswa($id, $arrayInformasi) {
	$sql = "UPDATE Mahasiswa ";
	$sql .= "SET nim_mhs = '".$arrayInformasi['nim']."', nama_mhs = \"".$arrayInformasi['nama']."\", ";
	$sql .= "alamat_mhs = \"".$arrayInformasi['alamat']."\", "; 
	$sql .="district_mhs = \"".$arrayInformasi['district']."\", "; 
	$sql .= "kabkot_mhs = \"".$arrayInformasi['kabkot']."\", "; 
	$sql .="kodepos_mhs = '".$arrayInformasi['kodepos']."' ,";
	$sql .= "telp_mhs = '".$arrayInformasi['telp']."', cellphone_mhs = '".$arrayInformasi['hp']."', ";
	$sql .= "email_mhs = '".$arrayInformasi['email']."', tgl_lhr_mhs = '".$arrayInformasi['tgllahir']."', ";
	$sql .= "tmp_lhr_mhs = \"".$arrayInformasi['tmplahir']."\", "; $sql .="agama_mhs = \"".$arrayInformasi['agama']."\", ";
	$sql .= "progstudi_id = '".$arrayInformasi['prodi']."', jk_mhs = '".$arrayInformasi['jk']."', ";
	$sql .= "wn_mhs = '".$arrayInformasi['warganegara']."', pekerjaan_mhs = '".$arrayInformasi['pekerjaan']."', ";
	$sql .= "stkwn_mhs = '".$arrayInformasi['kawin']."', jenjangpdk_mhs = '".$arrayInformasi['jenjang']."', ";
	$sql .= "jurusan_mhs = '".$arrayInformasi['jurusan']."', thnijazah_mhs = '".$arrayInformasi['thnijazah']."', ";
	$sql .= "namaibu_mhs = \"".$arrayInformasi['ibu']."\", ";
	$sql .= "norekening_mhs = '".$arrayInformasi['rekeneing']."', ";
	$sql .= "bank_mhs = '".$arrayInformasi['bank']."', namafb_mhs = \"".$arrayInformasi['fb']."\", ";
	$sql .= "angkatan_mhs = '".$arrayInformasi['angkatan']."', semester_aktif = '".$arrayInformasi['semester']."', ";
	$sql .= "id_tempatujian = '".$arrayInformasi['tempatujian']."', aktif = '".$arrayInformasi['status']."' ";
	$sql .= "WHERE id_mhs = '".$id."'";
		
	//print $sql;	
		
	$result = mysql_query($sql);
	
	if(!result)
		die(mysql_error());
	else
		return true;
}

function getAngkatan()
{
	$sql = "SELECT DISTINCT(angkatan_mhs)";
	$sql .= "FROM Mahasiswa ";
	//$sql .= "ORDER BY angkatan_mhs DESC ";
	
	$result = mysql_query($sql);
	
	return $result;
}

function delMahasiswa($id)
{
	$sql = "DELETE FROM Mahasiswa ";
	$sql .= "WHERE id_mhs = '$id'";
	
	mysql_query($sql);
	
	return true;
}

function searchMahasiswa($id, $nama, $email, $fb){
	$sql = "SELECT id_mhs, nim_mhs, nama_mhs, semester_aktif ";
	$sql .= "FROM Mahasiswa ";
	$sql .= "WHERE id_mhs LIKE \"%".$id."%\" ";
	$sql .= "AND nama_mhs LIKE \"%".$nama."%\" ";
	$sql .= "AND email_mhs LIKE \"%".$email."%\" ";
	$sql .= "AND namafb_mhs LIKE \"%".$fb."%\" ";
	
	$result = mysql_query($sql);
	if(!$result){
		print $sql.'\n';
		die(mysql_error());
	}
		
	
	return $result;
}

function checkMahasiswa($id) {
	$sql = "SELECT * FROM Mahasiswa WHERE id_mhs = '".$id."'";
	
	$result = mysql_query($sql);
	
	$jumlah = mysql_num_rows($result);
	
	if($jumlah > 0)
		return FALSE;
	else
		return TRUE;
}

function addMahasiswa($arrayInfo) {
	$tgllahir = $arrayInfo['thnlhr'] . "-" . $arrayInfo['blnlhr'] . "-" . $arrayInfo['tgllhr'] ;
	$sql = "INSERT INTO Mahasiswa (`ID_MHS`,`NIM_MHS`,`NAMA_MHS`,`ALAMAT_MHS`,
	`DISTRICT_MHS`,`KABKOT_MHS`,`KODEPOS_MHS`,`TELP_MHS`,`CELLPHONE_MHS`,
	`EMAIL_MHS`,`TGL_LHR_MHS`,`TMP_LHR_MHS`,`AGAMA_MHS`,`PROGSTUDI_ID`,
	`KDUPBJJ_MHS`,`JK_MHS`,`WN_MHS`,`PEKERJAAN_MHS`,`STKWN_MHS`,`JENJANGPDK_MHS`,
	`JURUSAN_MHS`,`THNIJAZAH_MHS`,`NAMAIBU_MHS`,`NOREKENING_MHS`,`BANK_MHS`,
	`ANGKATAN_MHS`,`NAMAFB_MHS`,`TIME`,`SEMESTER_AKTIF`,`ID_TEMPATUJIAN`) ";
	$sql .= "VALUES('".$arrayInfo['id']."','".$arrayInfo['nim']."','".$arrayInfo['nama']."','".$arrayInfo['alamat']."',
	'".$arrayInfo['district']."','".$arrayInfo['kabkot']."','".$arrayInfo['kodepos']."','".$arrayInfo['telp']."','".$arrayInfo['hp']."',
	'".$arrayInfo['email']."','".$tgllahir."','".$arrayInfo['tmplahir']."','".$arrayInfo['agama']."','".$arrayInfo['prodi']."',
	'".$arrayInfo['upbjj']."','".$arrayInfo['jk']."','".$arrayInfo['warganegara']."','".$arrayInfo['pekerjaan']."','".$arrayInfo['kawin']."','".$arrayInfo['jenjang']."',
	'".$arrayInfo['jurusan']."','".$arrayInfo['thnijazah']."','".$arrayInfo['ibu']."','".$arrayInfo['rekening']."','".$arrayInfo['bank']."',
	'".$arrayInfo['angkatan']."','".$arrayInfo['fb']."',NOW(),'".$arrayInfo['semester']."','".$arrayInfo['tempatujian']."') ";
	
	$result = mysql_query($sql);
	
	if(!$result){
		print $sql."\n";
		die(mysql_error());
	}		
	else 
		return TRUE;
	
}

?>