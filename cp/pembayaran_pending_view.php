<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mahasiswa</title>
	<meta name="generator" content="tsWebEditor (tswebeditor.net.tc - www.tswebeditor.tk)">
	<meta name="author" content="nedi">
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<link rel="stylesheet" type="text/css" href="css/style3.css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<script src="js/jquery-1.7.2.js" type="text/javascript"></script> 
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="line-top">
				<div id="logo" class="grid_1">
					<img src="img/logo.gif" alt="logo" />
				</div>
				<div class="grid_2">
					<ul id="nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="mahasiswa.php">Mahasiswa</a>
							<ul>
								<li><a href="mahasiswa-search.php">Pencarian Mahasiswa</a></li>
								<li><a href="mahasiswa-add.php">Tambah Mahasiswa</a></li>
							</ul>
						</li>
						<li><a href="pendaftaran.php">Pendaftaran</a></li>
						<li class="current"><a href="pembayaran.php">Pembayaran</a>
							<ul>
								<li><a href="pembayaran_add.php">Tambah Pembayaran</a></li>
								<li><a href="pembayaran_pending.php">Pembayaran Pending</a></li>
							</ul>
						</li>
						<li><a href="links.php">Links</a></li>
					</ul>
				</div>
				<div id="clear"></div>
			</div>
		</div>
		<div class="logout"><a href="signout.php" class="alogout">Sign Out</a></div>
		<div id="content">
		<div id="header-content">PEMBAYARAN PENDING</div>
			<?php
			if($jumlah == 0)
				print "<div>Tidak Ada Data Pending</div>";
			else {
			?>
			<div>
				<table id="money-image" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nama Mahasiswa</th>
							<th>Semester</th>
							<th>Tanggal</th>
							<th>Jumlah</th>
							<th>Keperluan</th>
							<th>Corfirm?</th>
						</tr>
					</thead>
					<tfoot>
				    	<tr>
				        	<td>
				        		|
				        		<?php
				        			for($i=0; $i<$numpage; $i++) {
				        				if($page == ($i*$amount)) 
											$style = "cur";
										else
											$style = "ncur";
				        		?>
				        		 <a href="pembayaran_pending.php?&page=<?php echo $i*$amount ?>">
				        		 	<span class="<?php echo $style ?>">
				        		 	<?php echo $i+1; ?>
				        		 	</span>
				        		 </a> |
				        		<?php
									}
				        		?>
				        	</td>
				        </tr>
				    </tfoot>
					<tbody>
				    	<?php				    	
				    	while ($rowmahasiswa = mysql_fetch_array($list_pending, MYSQL_ASSOC)) {
				    	?>
				    	<tr>
				        	<td><?php print $rowmahasiswa['MAHASISWA_ID'] ?></td>
				            <td><?php print ucwords(strtolower($rowmahasiswa['NAMA_MHS'])) ?></td>
				            <td><?php print $rowmahasiswa['SEMESTER_ID'] ?></td>
				            <td><?php print $rowmahasiswa['TGL_PEMBAYARAN'] ?></td>
							<td><?php print $rowmahasiswa['JMLH_PEMBAYARAN'] ?></td>
							<td><?php print $rowmahasiswa['DETAIL_PEMBAYARAN'] ?></td>
				            <td class="operation">
				            	<a href="confirm.php?id=<?php echo $rowmahasiswa['ID_PEMBAYARAN']?>&id_mhs=<?php echo $rowmahasiswa['MAHASISWA_ID'] ?>&page=<?php echo $page ?>"><img src="img/check.png" /></a>
				            </td>
				        </tr>
				        <?php
						}
				        ?>
				    </tbody>
				</table>
			</div>
			<?php
			}
			?>
		</div>
		<div id="footer">UT Taiwan 2012</div>
		<div id="bottom"></div>	
</body>
</html>
