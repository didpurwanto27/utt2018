<?php
include "config.php";
include "open_connection.php";
include "function.php";
require("dompdf/dompdf_config.inc.php");

$id = $_GET['id'];
$html = file_get_contents("http://ut-taiwan.org/registrasi/templatereport.php?id=".$id);
$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->set_paper('A4', 'portrait');
if ( isset($pdf) ) { 
    $pdf->page_script('
        if ($PAGE_COUNT > 1) {
            $font = Font_Metrics::get_font("helvetica", "bold");
            $size = 9;
            $pageText = $PAGE_NUM . "/" . $PAGE_COUNT;
            $y = $pdf->get_height() - 24;
            $x = $pdf->get_width() - 15 - Font_Metrics::get_text_width($pageText, $font, $size);
            $pdf->text($x, $y, $pageText, $font, $size);
        }        
    ');
}
$dompdf->render();
$file = "Form Pendaftaran UT Taiwan - " . $id.".pdf";
$dompdf->stream($file);
?>