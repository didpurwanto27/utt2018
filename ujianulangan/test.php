<?php
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel2007.php';
include "config.php";
include "open_connection.php";

$id = $_GET['id'];

$curday = date('j');
$curmonth = date('n');
$curyear = date('Y');
$cur_month = array (
				'1' => 'JANUARI', '2' => 'FEBRUARI', '3' => 'MARET', '4' => 'APRIL', '5' => 'MEI', '6' => 'JUNI',
				'7' => 'JULI', '8' => 'AGUSTUS', '9' => 'SEPTEMBER', '10' => 'OKTOBER', '11' => 'NOVEMBER', '12' => 'DESEMBER'
			);

$curdate = "   ".$curday." ".$cur_month[$curmonth]." ".$curyear;

$objPHPExcel = new PHPExcel();

//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
$objPHPExcel->getProperties()->setLastModifiedBy("Junaidillah Fadlil");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

//Set Page Margin
$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.551181102362205);
$objPHPExcel->getActiveSheet()->getPageMargins()->setHeader(0.31496062992126);
$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.31496062992126);
$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.511811023622047);
$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.451181102362205);
$objPHPExcel->getActiveSheet()->getPageMargins()->setFooter(0.31496062992126);

// Style
$styleTitle1 = array (
					'font' => array ('bold' => true),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					);

$styleTitle23 = array (
					'font' => array ('bold' => true, 'size' => 14),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					);

$styleTitle4 = array (
					'font' => array ('bold' => true, 'size' => 12),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
								'bottom' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
							),
					);
$styleAllborder = array (
					'borders' => array (
									'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN)
								),
					); 

$stylefont10 = array (
					'font' => array ('size' => 10),
				);

$stylefont10bold = array (
					'font' => array ('bold' => true, 'size' => 10),
				);

$stylefont85 = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);
				
$stylefont85bold = array (
					'font' => array ('bold' => true, 'size' => 8.5),
				);

$stylefont85center = array (
					'font' => array ('bold' => true, 'size' => 8.5),
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'borders' => array (
								'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
							),
				);

$stylecenter = array (
					'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
				);

//echo date('H:i:s') . " Add some data\n";
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2.13);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(4.99);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20.56);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(2.7);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(2.7);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(10);

$objPHPExcel->getActiveSheet()->mergeCells('B2:AB2');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'KEMENTRIAN PENDIDIKAN NASIONAL');
$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($styleTitle1);

$objPHPExcel->getActiveSheet()->mergeCells('B3:AB3');
$objPHPExcel->getActiveSheet()->SetCellValue('B3', 'UNIVERSITAS TERBUKA');
$objPHPExcel->getActiveSheet()->mergeCells('B4:AB4');
$objPHPExcel->getActiveSheet()->SetCellValue('B4', 'FORMULIR DATA PRIBADI MAHASISWA');
$objPHPExcel->getActiveSheet()->getStyle('B3:B4')->applyFromArray($styleTitle23);

$objPHPExcel->getActiveSheet()->mergeCells('B5:AB5');
$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'PROGRAM REGULER');
$objPHPExcel->getActiveSheet()->getStyle('B5:AB5')->applyFromArray($styleTitle4);

$objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B7', 'K-01');
$objPHPExcel->getActiveSheet()->SetCellValue('C7', 'N I M');
$objPHPExcel->getActiveSheet()->getStyle('D7:L7')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B9', 'K-02');
$objPHPExcel->getActiveSheet()->SetCellValue('C9', 'NAMA');
$objPHPExcel->getActiveSheet()->getStyle('D9:AB10')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('11')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B12', 'K-03');
$objPHPExcel->getActiveSheet()->SetCellValue('C12', 'ALAMAT PENGIRIMAN');
$objPHPExcel->getActiveSheet()->getStyle('D12:AB13')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('14')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B15', 'K-04');
$objPHPExcel->getActiveSheet()->SetCellValue('C15', 'KODE KABUPATEN/KOTA');
$objPHPExcel->getActiveSheet()->SetCellValue('K15', 'Lihat Lampiran Katalog UT');
$objPHPExcel->getActiveSheet()->getStyle('C15')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('K15')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D15:H15')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('16')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B17', 'K-05');
$objPHPExcel->getActiveSheet()->SetCellValue('C17', 'KODE POS');
$objPHPExcel->getActiveSheet()->getStyle('D17:H17')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('18')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B19', 'K-06');
$objPHPExcel->getActiveSheet()->SetCellValue('C19', 'NOMOR TELEPON');
$objPHPExcel->getActiveSheet()->getStyle('D19:G19')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->getStyle('H19:P19')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('20')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B21', 'K-07');
$objPHPExcel->getActiveSheet()->SetCellValue('C21', 'NOMOR HP');
$objPHPExcel->getActiveSheet()->getStyle('D21:P21')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('22')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B23', 'K-08');
$objPHPExcel->getActiveSheet()->SetCellValue('C23', 'ALAMAT E-MAIL');
$objPHPExcel->getActiveSheet()->getStyle('D23:AB24')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('25')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->mergeCells('D26:E26');
$objPHPExcel->getActiveSheet()->SetCellValue('D26', 'TGL');
$objPHPExcel->getActiveSheet()->getStyle('D26:E26')->applyFromArray($stylefont85center);
$objPHPExcel->getActiveSheet()->getRowDimension('26')->setRowHeight(12);

$objPHPExcel->getActiveSheet()->mergeCells('F26:G26');
$objPHPExcel->getActiveSheet()->SetCellValue('F26', 'BLN');
$objPHPExcel->getActiveSheet()->getStyle('F26:G26')->applyFromArray($stylefont85center);

$objPHPExcel->getActiveSheet()->mergeCells('H26:K26');
$objPHPExcel->getActiveSheet()->SetCellValue('H26', 'TAHUN');
$objPHPExcel->getActiveSheet()->getStyle('H26:K26')->applyFromArray($stylefont85center);

$objPHPExcel->getActiveSheet()->SetCellValue('B27', 'K-09');
$objPHPExcel->getActiveSheet()->SetCellValue('C27', 'TANGGAL LAHIR');
$objPHPExcel->getActiveSheet()->getStyle('D27:K27')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('28')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B29', 'K-10');
$objPHPExcel->getActiveSheet()->SetCellValue('C29', 'TEMPAT LAHIR');
$objPHPExcel->getActiveSheet()->getStyle('D29:AB29')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('30')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B31', 'K-11');
$objPHPExcel->getActiveSheet()->SetCellValue('C31', 'AGAMA');
$objPHPExcel->getActiveSheet()->SetCellValue('E31', 'Islam');
$objPHPExcel->getActiveSheet()->getStyle('E31')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D31')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('J31', 'Katholik');
$objPHPExcel->getActiveSheet()->getStyle('J31')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('I31')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('P31', 'Protestan');
$objPHPExcel->getActiveSheet()->getStyle('P31')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('O31')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('U31', 'Hindu');
$objPHPExcel->getActiveSheet()->getStyle('U31')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('T31')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('Y31', 'Budha');
$objPHPExcel->getActiveSheet()->getStyle('Y31')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('X31')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('32')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B33', 'K-12');
$objPHPExcel->getActiveSheet()->SetCellValue('C33', 'KODE PROGRAM STUDI');
$objPHPExcel->getActiveSheet()->getStyle('C33')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->SetCellValue('G33', 'Yang Dipilih, Lihat Katalog UT');
$objPHPExcel->getActiveSheet()->getStyle('G33')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D33:E33')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('34')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B35', 'K-13');
$objPHPExcel->getActiveSheet()->SetCellValue('C35', 'KODE UPBJJ');
$objPHPExcel->getActiveSheet()->SetCellValue('G35', 'Lihat Lampiran Katalog UT');
$objPHPExcel->getActiveSheet()->getStyle('G35')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D35:E35')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('36')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B37', 'K-14');
$objPHPExcel->getActiveSheet()->SetCellValue('C37', 'JENIS KELAMIN');
$objPHPExcel->getActiveSheet()->SetCellValue('E37', 'Laki-laki');
$objPHPExcel->getActiveSheet()->getStyle('E37')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D37')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('J37', 'Perempuan');
$objPHPExcel->getActiveSheet()->getStyle('J37')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('I37')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('38')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B39', 'K-15');
$objPHPExcel->getActiveSheet()->SetCellValue('C39', 'KEWARGANEGARAAN');
$objPHPExcel->getActiveSheet()->SetCellValue('E39', 'Indonesia');
$objPHPExcel->getActiveSheet()->getStyle('D39')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('J39', 'Asing');
$objPHPExcel->getActiveSheet()->getStyle('I39')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('40')->setRowHeight(6);

$objPHPExcel->getActiveSheet()->SetCellValue('B41', 'K-16');
$objPHPExcel->getActiveSheet()->SetCellValue('C41', 'STATUS PEKERJAAN');
$objPHPExcel->getActiveSheet()->SetCellValue('E41', 'TNI/POLRI');
$objPHPExcel->getActiveSheet()->getStyle('E41')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D41')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('J41', 'PNS');
$objPHPExcel->getActiveSheet()->getStyle('J41')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('I41')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('N41', 'Swasta');
$objPHPExcel->getActiveSheet()->getStyle('N41')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('M41')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('R41', 'Wiraswasta');
$objPHPExcel->getActiveSheet()->getStyle('R41')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('Q41')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('X41', 'Tidak Bekerja');
$objPHPExcel->getActiveSheet()->getStyle('X41')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('W41')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('42')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B43', 'K-17');
$objPHPExcel->getActiveSheet()->SetCellValue('C43', 'STATUS PERKAWINAN');
$objPHPExcel->getActiveSheet()->SetCellValue('E43', 'Menikah');
$objPHPExcel->getActiveSheet()->getStyle('E43')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D43')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('J43', 'Tidak Menikah');
$objPHPExcel->getActiveSheet()->getStyle('J43')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('I43')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('44')->setRowHeight(6);

$objPHPExcel->getActiveSheet()->SetCellValue('C45', 'PENDIDIKAN TERAKHIR');
$objPHPExcel->getActiveSheet()->getRowDimension('45')->setRowHeight(12);
$objPHPExcel->getActiveSheet()->getStyle('C45')->applyFromArray($stylefont10bold);

$objPHPExcel->getActiveSheet()->SetCellValue('B46', 'K-18');
$objPHPExcel->getActiveSheet()->SetCellValue('C46', 'JENJANG');
$objPHPExcel->getActiveSheet()->SetCellValue('E46', 'SLTA');
$objPHPExcel->getActiveSheet()->getStyle('E46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('I46', 'D-I');
$objPHPExcel->getActiveSheet()->getStyle('I46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('H46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('L46', 'D-II');
$objPHPExcel->getActiveSheet()->getStyle('L46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('K46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('O46', 'D-III');
$objPHPExcel->getActiveSheet()->getStyle('O46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('N46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('R46', 'S-1');
$objPHPExcel->getActiveSheet()->getStyle('R46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('Q46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('U46', 'S-2');
$objPHPExcel->getActiveSheet()->getStyle('U46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('T46')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('X46', 'S-3');
$objPHPExcel->getActiveSheet()->getStyle('X46')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('W46')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('47')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B48', 'K-19');
$objPHPExcel->getActiveSheet()->SetCellValue('C48', 'KODE JURUSAN');
$objPHPExcel->getActiveSheet()->getStyle('D48:F48')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('49')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B50', 'K-20');
$objPHPExcel->getActiveSheet()->SetCellValue('C50', 'TAHUN IJAZAH');
$objPHPExcel->getActiveSheet()->getStyle('D50:G50')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('51')->setRowHeight(6.75);

$objPHPExcel->getActiveSheet()->SetCellValue('B52', 'K-21');
$objPHPExcel->getActiveSheet()->SetCellValue('C52', 'SEBAGAI TENAGA PENGAJAR');
$objPHPExcel->getActiveSheet()->getStyle('C52')->applyFromArray($stylefont85);
$objPHPExcel->getActiveSheet()->SetCellValue('E52', 'Ya');
$objPHPExcel->getActiveSheet()->getStyle('E52')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('D52')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('I52', 'Tidak');
$objPHPExcel->getActiveSheet()->getStyle('I52')->applyFromArray($stylefont10);
$objPHPExcel->getActiveSheet()->getStyle('G52')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->SetCellValue('C53', 'JIKA YA');
$objPHPExcel->getActiveSheet()->getRowDimension('53')->setRowHeight(10.5);

$objPHPExcel->getActiveSheet()->SetCellValue('B54', 'K-22');
$objPHPExcel->getActiveSheet()->SetCellValue('C54', 'MENGAJAR PADA');
$objPHPExcel->getActiveSheet()->SetCellValue('E54', 'TK');
$objPHPExcel->getActiveSheet()->getStyle('D54')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('H54', 'SD');
$objPHPExcel->getActiveSheet()->getStyle('G54')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('K54', 'SLTP');
$objPHPExcel->getActiveSheet()->getStyle('J54')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('N54', 'SLTA');
$objPHPExcel->getActiveSheet()->getStyle('M54')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('Q54', 'PT');
$objPHPExcel->getActiveSheet()->getStyle('P54')->applyFromArray($styleAllborder);
$objPHPExcel->getActiveSheet()->SetCellValue('T54', 'Non Formal');
$objPHPExcel->getActiveSheet()->getStyle('S54')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('55')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B56', 'K-23');
$objPHPExcel->getActiveSheet()->SetCellValue('C56', 'MENGAJAR BIDANG STUDI');
$objPHPExcel->getActiveSheet()->getStyle('C56')->applyFromArray($stylefont85);
$objPHPExcel->getActiveSheet()->getStyle('D56:AB56')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('57')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B58', 'K-24');
$objPHPExcel->getActiveSheet()->SetCellValue('C58', 'NAMA IBU KANDUNG');
$objPHPExcel->getActiveSheet()->getStyle('D58:AB58')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->getRowDimension('59')->setRowHeight(7);

$objPHPExcel->getActiveSheet()->SetCellValue('B60', 'K-25');
$objPHPExcel->getActiveSheet()->SetCellValue('C60', 'NO. REKENING BRI');
$objPHPExcel->getActiveSheet()->getStyle('D60:R60')->applyFromArray($styleAllborder);

$objPHPExcel->getActiveSheet()->SetCellValue('U61', $curdate);

$objPHPExcel->getActiveSheet()->getRowDimension('62')->setRowHeight(10);
$objPHPExcel->getActiveSheet()->getRowDimension('63')->setRowHeight(10);

$objPHPExcel->getActiveSheet()->SetCellValue('B64', 'Keterangan: - Isilah dengan huruf Kapital pada kotak yang disediakan');
$objPHPExcel->getActiveSheet()->getStyle('B64')->applyFromArray($stylefont85);

$objPHPExcel->getActiveSheet()->SetCellValue('T64', '(                                                  )');

$objPHPExcel->getActiveSheet()->SetCellValue('B65', '                              - K-11, K-14, K-15, K-16, K-17, K-18, K-21, dan K-22 diisi dengan X pada satu kotak yang sesuai   ');
$objPHPExcel->getActiveSheet()->getStyle('B65')->applyFromArray($stylefont85);

$objPHPExcel->getActiveSheet()->SetCellValue('V65', 'Tanda Tangan');

//======================================================//
// Data //
//=====================================================//
$sql = "SELECT id, nama, alamat, kabkot, kodepos, notelp, nohp, email, tgllahir, tmplahir, agama, ";
$sql .= "kdprogramstudi, kdupbjj, jk, warganegara, pekerjaan, kawin, jenjangpendidikan, jurusan, thnijazah, ";
$sql .= "namaibu, bank ";
$sql .= "FROM pendaftaran ";
$sql .= "WHERE id = '".$id."'";

$result = mysql_query($sql) or die ('Can not connect');
while ($row = mysql_fetch_array($result)) {
	$id 		= $row['id'];
	$nama 		= $row['nama'];
	$alamat 	= $row['alamat'];
	$kabkot	 	= $row['kabkot'];
	$kodepos 	= $row['kodepos'];
	$notelp 	= $row['notelp'];
	$nohp		= $row['nohp'];
	$email		= $row['email'];
	$tgllahir	= $row['tgllahir'];
	$tmplahir	= $row['tmplahir'];
	$agama		= $row['agama'];
	$kdprodi	= $row['kdprogramstudi'];
	$kdupbjj	= $row['kdupbjj'];
	$jk			= $row['jk'];
	$warga		= $row['warganegara'];
	$pekerjaan	= $row['pekerjaan'];
	$kawin		= $row['kawin'];
	$jenjang	= $row['jenjangpendidikan'];
	$jurusan	= $row['jurusan'];
	$thnijazah	= $row['thnijazah'];
	$namaibu	= $row['namaibu'];
	$bank		= $row['bank'];
}

// list box
$boxname = array (
				0 => 'D9', 1 => 'E9', 2 => 'F9', 3 => 'G9', 4 => 'H9', 5 => 'I9', 6 => 'J9', 7 => 'K9', 8 => 'L9', 9 => 'M9', 
				10 => 'N9', 11 => 'O9', 12 => 'P9', 13 => 'Q9', 14 => 'R9', 15 => 'S9', 16 => 'T9', 17 => 'U9', 18 => 'V9', 19 => 'W9',
				20 => 'X9', 21 => 'Y9', 22 => 'Z9', 23 => 'AA9', 24 => 'AB9', 25 => 'D10', 26 => 'E10', 27 => 'F10', 28 => 'G10', 29 => 'H10', 
				30 => 'I10', 31 => 'J10', 32 => 'K10', 33 => 'L10', 34 => 'M10', 35 => 'N10', 35 => 'O10', 36 => 'P10', 37 => 'Q10',
				38 => 'R10', 39 => 'S10', 40 => 'T10', 41 => 'U10', 42 => 'V10', 43 => 'W10', 44 => 'X10', 45 => 'Y10', 46 => 'Z10', 
				47 => 'AA10', 48 => 'AB10'
			);

$boxalamat = array (
				0 => 'D12', 1 => 'E12', 2 => 'F12', 3 => 'G12', 4 => 'H12', 5 => 'I12', 6 => 'J12', 7 => 'K12', 8 => 'L12', 9 => 'M12', 
				10 => 'N12', 11 => 'O12', 12 => 'P12', 13 => 'Q12', 14 => 'R12', 15 => 'S12', 16 => 'T12', 17 => 'U12', 18 => 'V12', 19 => 'W12',
				20 => 'X12', 21 => 'Y12', 22 => 'Z12', 23 => 'AA12', 24 => 'AB12', 25 => 'D13', 26 => 'E13', 27 => 'F13', 28 => 'G13', 29 => 'H13', 
				30 => 'I13', 31 => 'J13', 32 => 'K13', 33 => 'L13', 34 => 'M13', 35 => 'N13', 36 => 'O13', 37 => 'P13', 38 => 'Q13',
				39 => 'R13', 40 => 'S13', 41 => 'T13', 42 => 'U13', 43 => 'V13', 44 => 'W13', 45 => 'X13', 46 => 'Y13', 47 => 'Z13', 
				48 => 'AA13', 49 => 'AB13'
			);

$boxkabkot = array (0 => 'D15', 1 => 'E15', 2 => 'F15', 3 => 'G15', 4 => 'H15');
$boxkodepos = array (0 => 'D17', 1 => 'E17', 2 => 'F17', 3 => 'G17', 4 => 'H17');
$boxtelp = array (0 => 'H19', 1 => 'I19', 2 => 'J19', 3 => 'K19', 4 => 'L19', 5 => 'M19', 6 => 'N19', 7 => 'O19', 8 => 'P19');
$boxhp = array (0 => 'H21', 1 => 'I21', 2 => 'J21', 3 => 'K21', 4 => 'L21', 5 => 'M21', 6 => 'N21', 7 => 'O21', 8 => 'P21');
$boxemail = array (
				0 => 'D23', 1 => 'E23', 2 => 'F23', 3 => 'G23', 4 => 'H23', 5 => 'I23', 6 => 'J23', 7 => 'K23', 8 => 'L23', 9 => 'M23', 
				10 => 'N23', 11 => 'O23', 12 => 'P23', 13 => 'Q23', 14 => 'R23', 15 => 'S23', 16 => 'T23', 17 => 'U23', 18 => 'V23', 19 => 'W23',
				20 => 'X23', 21 => 'Y23', 22 => 'Z23', 23 => 'AA23', 24 => 'AB23', 25 => 'D24', 26 => 'E24', 27 => 'F24', 28 => 'G24', 29 => 'H24', 
				30 => 'I24', 31 => 'J24', 32 => 'K24', 33 => 'L24', 34 => 'M24', 35 => 'N24', 35 => 'O24', 36 => 'P24', 37 => 'Q24',
				38 => 'R24', 39 => 'S24', 40 => 'T24', 41 => 'U24', 42 => 'V24', 43 => 'W24', 44 => 'X24', 45 => 'Y24', 46 => 'Z24', 
				47 => 'AA24', 48 => 'AB24'
			);
$boxtgl = array (0 => 'D27', 1 => 'E27');
$boxbln = array (0 => 'F27', 1 => 'G27');
$boxthn = array (0 => 'H27', 1 => 'I27', 2 => 'J27', 3 => 'K27');
$boxtmplhr = array (
				0 => 'D29', 1 => 'E29', 2 => 'F29', 3 => 'G29', 4 => 'H29', 5 => 'I29', 6 => 'J29', 7 => 'K29', 8 => 'L29', 9 => 'M29', 
				10 => 'N29', 11 => 'O29', 12 => 'P29', 13 => 'Q29', 14 => 'R29', 15 => 'S29', 16 => 'T29', 17 => 'U29', 18 => 'V29', 19 => 'W29',
				20 => 'X29', 21 => 'Y29', 22 => 'Z29', 23 => 'AA29', 24 => 'AB29'
			);
$boxprodi = array (0 => 'D33', 1 => 'E33');
$boxupbjj = array (0 => 'D35', 1 => 'E35');
$boxjurusan = array (0 => 'D48', 1 => 'E48', 2 => 'F48');
$boxijazah = array (0 => 'D50', 1 => 'E50', 2 => 'F50', 3 => 'G50');
$boxmengajar = array (
				0 => 'D56', 1 => 'E56', 2 => 'F56', 3 => 'G56', 4 => 'H56', 5 => 'I56', 6 => 'J56', 7 => 'K56', 8 => 'L56', 9 => 'M56', 
				10 => 'N56', 11 => 'O56', 12 => 'P56', 13 => 'Q56', 14 => 'R56', 15 => 'S56', 16 => 'T56', 17 => 'U56', 18 => 'V56', 19 => 'W56',
				20 => 'X56', 21 => 'Y56', 22 => 'Z56', 23 => 'AA56', 24 => 'AB56'
			);
$boxibu = array (
				0 => 'D58', 1 => 'E58', 2 => 'F58', 3 => 'G58', 4 => 'H58', 5 => 'I58', 6 => 'J58', 7 => 'K58', 8 => 'L58', 9 => 'M58', 
				10 => 'N58', 11 => 'O58', 12 => 'P58', 13 => 'Q58', 14 => 'R58', 15 => 'S58', 16 => 'T58', 17 => 'U58', 18 => 'V58', 19 => 'W58',
				20 => 'X58', 21 => 'Y58', 22 => 'Z58', 23 => 'AA55', 24 => 'AB58'
			);
$boxbank = array (
				0 => 'D60', 1 => 'E60', 2 => 'F60', 3 => 'G60', 4 => 'H60', 5 => 'I60', 6 => 'J60', 7 => 'K60', 8 => 'L60', 9 => 'M60', 
				10 => 'N60', 11 => 'O60', 12 => 'P60', 13 => 'Q60', 14 => 'R60'
			);



$lenname = strlen($nama);
for($i=0; $i<$lenname; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxname[$i], strtoupper($nama[$i]));
	$objPHPExcel->getActiveSheet()->getStyle($boxname[$i])->applyFromArray($stylecenter);
}

$alamat = str_ireplace('taiwan', '', $alamat);
$alamat = trim($alamat);
$alamat = preg_replace("/[[:space:]]+/"," ",$alamat);
$lenalamat = strlen($alamat);

if($alamat[$lenalamat-1] == ",") 
	$alamat = substr($alamat, 0, -1);

$lenalamat = $lenalamat-1;

if($lenalamat > 50)
	$lenalamat = 50;

for($i=0; $i<$lenalamat; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxalamat[$i], $alamat[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxalamat[$i])->applyFromArray($stylecenter);
}

$lenkabkot = strlen($kabkot);
if($lenkabkot > 5)
	$lenkabkot = 5;
for($i=0; $i<$lenkabkot; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxkabkot[$i], $kabkot[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxkabkot[$i])->applyFromArray($stylecenter);
}

$lenkodepos = strlen($kodepos);
for($i=0; $i<$lenkodepos; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxkodepos[$i], $kodepos[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxkodepos[$i])->applyFromArray($stylecenter);
}

$lentelp = strlen($notelp);
$objPHPExcel->getActiveSheet()->SetCellValue('D19', '+');
$objPHPExcel->getActiveSheet()->SetCellValue('E19', '8');
$objPHPExcel->getActiveSheet()->SetCellValue('F19', '8');
$objPHPExcel->getActiveSheet()->SetCellValue('G19', '6');
$objPHPExcel->getActiveSheet()->getStyle('D19:G19')->applyFromArray($stylecenter);
for($i=0; $i<$lentelp; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxtelp[$i], $notelp[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxtelp[$i])->applyFromArray($stylecenter);
}

$lenhp = strlen($nohp);
$objPHPExcel->getActiveSheet()->SetCellValue('D21', '+');
$objPHPExcel->getActiveSheet()->SetCellValue('E21', '8');
$objPHPExcel->getActiveSheet()->SetCellValue('F21', '8');
$objPHPExcel->getActiveSheet()->SetCellValue('G21', '6');
$objPHPExcel->getActiveSheet()->getStyle('D21:G21')->applyFromArray($stylecenter);
for($i=0; $i<$lenhp; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxhp[$i], $nohp[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxhp[$i])->applyFromArray($stylecenter);
}

$lenemail = strlen($email);
for($i=0; $i<$lenemail; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxemail[$i], $email[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxemail[$i])->applyFromArray($stylecenter);
}

list($thn, $bln, $tgl) = explode("-", $tgllahir);
for($i=0; $i<2; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxtgl[$i], $tgl[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxtgl[$i])->applyFromArray($stylecenter);
}

for($i=0; $i<2; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxbln[$i], $bln[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxbln[$i])->applyFromArray($stylecenter);
}

for($i=0; $i<4; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxthn[$i], $thn[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxthn[$i])->applyFromArray($stylecenter);
}

$lentmplhr = strlen($tmplahir);
if($lentmplhr > 24)
	$lentmplhr = 24;
for($i=0; $i<$lentmplhr; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxtmplhr[$i], $tmplahir[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxtmplhr[$i])->applyFromArray($stylecenter);
}

if(strtolower($agama) == "islam") {	
	$objPHPExcel->getActiveSheet()->SetCellValue('D31', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D31')->applyFromArray($stylecenter);
} else if (strtolower($agama) == "katholik") {	
	$objPHPExcel->getActiveSheet()->SetCellValue('I31', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('L31')->applyFromArray($stylecenter);
}else if (strtolower($agama) == "protestan") {	
	$objPHPExcel->getActiveSheet()->SetCellValue('P31', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('P31')->applyFromArray($stylecenter);
}else if (strtolower($agama) == "hindu") {	
	$objPHPExcel->getActiveSheet()->SetCellValue('U31', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('U31')->applyFromArray($stylecenter);
}else if (strtolower($agama) == "budha") {	
	$objPHPExcel->getActiveSheet()->SetCellValue('Y31', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('Y31')->applyFromArray($stylecenter);
}

$objPHPExcel->getActiveSheet()->SetCellValue('D33', $kdprodi[0]);
$objPHPExcel->getActiveSheet()->SetCellValue('E33', $kdprodi[1]);
$objPHPExcel->getActiveSheet()->getStyle('D33:E33')->applyFromArray($stylecenter);

$objPHPExcel->getActiveSheet()->SetCellValue('D35', '2');
$objPHPExcel->getActiveSheet()->SetCellValue('E35', '1');
$objPHPExcel->getActiveSheet()->getStyle('D35:E35')->applyFromArray($stylecenter);

if(strtolower($jk) == "pria") {
	$objPHPExcel->getActiveSheet()->SetCellValue('D37', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D37')->applyFromArray($stylecenter);
} else if(strtolower($jk) == "wanita") {
	$objPHPExcel->getActiveSheet()->SetCellValue('I37', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('I37')->applyFromArray($stylecenter);
}

if(strtolower($warga) == "wni") {
	$objPHPExcel->getActiveSheet()->SetCellValue('D39', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D39')->applyFromArray($stylecenter);
} else if (strtolower($warganegara) == "asing") {
	$objPHPExcel->getActiveSheet()->SetCellValue('I39', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('I39')->applyFromArray($stylecenter);
}

if(strtolower($pekerjaan) == "tni/polri") {
	$objPHPExcel->getActiveSheet()->SetCellValue('D41', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D41')->applyFromArray($stylecenter);
} else if (strtolower($pekerjaan) == "pns") {
	$objPHPExcel->getActiveSheet()->SetCellValue('I41', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('I41')->applyFromArray($stylecenter);
} else if (strtolower($pekerjaan) == "swasta / tki") {
	$objPHPExcel->getActiveSheet()->SetCellValue('M41', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('M41')->applyFromArray($stylecenter);
} else if (strtolower($pekerjaan) == "wiraswasta") {
	$objPHPExcel->getActiveSheet()->SetCellValue('Q41', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('Q41')->applyFromArray($stylecenter);
} else if (strtolower($pekerjaan) == "tidak bekerja") {
	$objPHPExcel->getActiveSheet()->SetCellValue('W41', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('W41')->applyFromArray($stylecenter);
}

if(strtolower($kawin) == "menikah") {
	$objPHPExcel->getActiveSheet()->SetCellValue('D43', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D43')->applyFromArray($stylecenter);
} else {
	$objPHPExcel->getActiveSheet()->SetCellValue('I43', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('I43')->applyFromArray($stylecenter);
}

if(strtolower($jenjang) == "slta") {
	$objPHPExcel->getActiveSheet()->SetCellValue('D46', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('D46')->applyFromArray($stylecenter);
} else if (strtolower($jenjang) == "d-i") {
	$objPHPExcel->getActiveSheet()->SetCellValue('H46', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('H46')->applyFromArray($stylecenter);
} else if (strtolower($jenjang) == "d-ii") {
	$objPHPExcel->getActiveSheet()->SetCellValue('K46', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('K46')->applyFromArray($stylecenter);
} else {
	$objPHPExcel->getActiveSheet()->SetCellValue('N46', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('N46')->applyFromArray($stylecenter);
}

$kdjur = array (
				'SMTA Umum IPA / IPS' => '101',
				'STM/SMTA' => '102',
				'SMK/SMKA' => '107',
				'SMEA' => '104',
				'SPMA' => '103',
				'SPG/SPGLB/SGO/SMOA' => '108'
			);

$newjurusan = $kdjur[$jurusan];
for($i=0; $i<3; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxjurusan[$i], $newjurusan[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxjurusan[$i])->applyFromArray($stylecenter);
}

$lenthnijazah = strlen($thnijazah);
for($i=0; $i<$lenthnijazah; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxijazah[$i], $thnijazah[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxijazah[$i])->applyFromArray($stylecenter);
}


$objPHPExcel->getActiveSheet()->SetCellValue('G52', 'X');
	$objPHPExcel->getActiveSheet()->getStyle('G52')->applyFromArray($stylecenter);

$lenibu = strlen($namaibu);
for($i=0; $i<$lenibu; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxibu[$i], strtoupper($namaibu[$i]));
	$objPHPExcel->getActiveSheet()->getStyle($boxibu[$i])->applyFromArray($stylecenter);
}



/*$lenbank = strlen($bank);
for($i=0; $i<$lenibu; $i++) {
	$objPHPExcel->getActiveSheet()->SetCellValue($boxbank[$i], $bank[$i]);
	$objPHPExcel->getActiveSheet()->getStyle($boxbank[$i])->applyFromArray($stylecenter);
}*/



// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('Simple');

// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

ob_end_clean();
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$id.'.xlsx"');

$objWriter->save('php://output');
exit();
//$objPHPExcel->disconnectWorksheets();
//unset($objPHPExcel);


?> 