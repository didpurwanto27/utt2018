<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	newUlanganDateChecker();
	
	$id = $_GET['id'];
	$sql = "select * from Mahasiswa where id_mhs like '" . $id ."'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style2.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Ujian Ulangan Mahasiswa Baru UT-Taiwan Online</title>
  </head>

<body>
<div id="stylized" class="myform">
<h1>Pilih Ujian Pengulangan Untuk Semester Depan</h1>
<p>Silahkan memilih ujian pengulangan di semester depan. Syarat yang harus diperhatikan :<br />
  1. Sudah pernah mengambil mata kuliah tersebut sebelumnya dan mendapat nilai yang kurang memuaskan<br />
  2. Jadwal ujian pengulangan <strong>tidak boleh bersamaan dengan</strong> jadwal ujian mata kuliah yang diambil semester depan.<br />
  3. Biaya untuk ujian pengulangan adalah 300 NTD / sks
</p>
<p>Semester Depan (<?php echo cetakSemesterAktif(); ?>) anda akan mengambil paket mata kuliah sbb :<br />
<?php $jadwalarr = array();
      $semesterke = getSemesterKeByID($id);
	  $jurusan = getJurusanMhsByID($id);
	  $sql2 = "SELECT KODE_MK, NAMA_MK, SKS, JADWAL_UJIAN
	           FROM Mata_Kuliah Where SEMESTER = " . $semesterke . " AND PROGSTUDI_ID = " . $jurusan;
	  $r2 = mysql_query($sql2);
	  $nomor = 1;
	  while($row2 = mysql_fetch_array($r2))
	  {
         echo $nomor . ". " . $row2['KODE_MK'] ."/". $row2['NAMA_MK']."/". $row2['SKS']."/JADWAL:". $row2['JADWAL_UJIAN'] . "<br />";
	     $nomor++;
		 array_push($jadwalarr, $row2['JADWAL_UJIAN']); 
	  } ?>
</p>
<?php
$jam[1] = "I.1"; $jam[2] = "I.2"; $jam[3] = "I.3"; $jam[4] = "I.4"; $jam[5] = "I.5";
$jam[6] = "II.1"; $jam[7] = "II.2"; $jam[8] = "II.3"; $jam[9] = "II.4"; $jam[10] = "II.5"; $jam[11] = "III.1";
 ?>
<p>Pilih mata kuliah yang HANYA ingin anda ikuti ujian ulangannya di semester depan : <br />
<form name="2" action="submit2.php" method="post">
<table>
<? 
$semaktif = getSemesterAktif();
for($i = 1; $i <= 11; $i++) {?>

<? $waktu = explode(".", $jam[$i]);?>
<tr><td><strong> HARI <?=$waktu[0]?> JAM KE - <?=$waktu[1]?></strong><br /></td></tr>
<? if(in_array($jam[$i], $jadwalarr)){ echo "<tr><td>". printNamaMKOnJadwalSemester($jam[$i], $semesterke, $jurusan) . "</td></tr>";}
   else { 
       $taken = 0; 
       $sql4 = "SELECT ID_MK, KODE_MK, NAMA_MK, SKS, SEMESTER
	           FROM Mata_Kuliah Where SEMESTER < " . $semesterke . " AND PROGSTUDI_ID = " . $jurusan . " AND JADWAL_UJIAN like '" . $jam[$i]. "' ORDER BY ID_MK";
	  $r4 = mysql_query($sql4);
	   while($row4 = mysql_fetch_array($r4))
	  { ?><tr><td><input name="mk[<?=$jam[$i];?>]" type="radio" <? if(isMKTaken($id,$row4['ID_MK'],$semaktif)){ $taken = 1; echo 'checked="checked"';}?> value="<?php echo $row4['ID_MK'];?>" /> <?php echo $row4['KODE_MK']."/".$row4['NAMA_MK']."/SMSTR:".$row4['SEMESTER']."/SKS:".$row4['SKS']."/BIAYA:".($row4['SKS']*300)."NT$";?></td></tr><? }?><tr><td><input name="mk[<?=$jam[$i];?>]" <? if($taken==0){ echo 'checked="checked"';}?> type="radio" value="0" /> Tidak mengambil ujian susulan di Jam Ujian ini </td></tr><?	}?><tr><td>--------------------------------------------------------------------------</td></tr>

<? } ?>
</table>
<input type="hidden" name="id" value="<?php echo $id;?>" />
<button type="submit">Ambil !</button>
</form>
</p>
<div class="spacer"></div>

</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
