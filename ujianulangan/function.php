<?php
function generate_number() 
{
	$character = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
	$number = "123456789";
	$random_string_length = 4;
	
	$random_string = "";
	
	for ($i = 0; $i < $random_string_length; $i++) {
		$random_string .= $character[mt_rand (0, strlen($character)-1)];
	}
	
	for ($i = 0; $i < $random_string_length; $i++) {
		$random_string .= $number[mt_rand (0, strlen($number)-1)];
	}

	return $random_string;
}

function isValidEmail($email) {
  if(preg_match("/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $email) > 0)
  	return true;
  else
  	return false;
}

function getTotalAngkatan()
{
	$sql = "select count(id_semester) from Semester";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function cetakSemesterAktif()
{
	$sql = "select id_semester from Semester where active_semester = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	$result = substr($result, 0, 4) . "." . $result[4];
	//$result = substr($result, 0, 4) . "/" . substr($result, 4, 4) . "." . $result[8];
	return $result;
}

function cetakSemesterAktifbyInput($input)
{
	$result = substr($input, 0, 4) . "." . $input[4];
	//$result = substr($result, 0, 4) . "/" . substr($result, 4, 4) . "." . $result[8];
	return $result;
}

function printKota($i)
{
	$sql = "select TEMPATUJIAN from Tempat_Ujian where ID_TEMPATUJIAN = " . $i;
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function isMKTaken($idmhs,$idmk,$semaktif)
{
	$sql = "select count(id_mk) from MK_Susulan where id_mhs like '".$idmhs."' and id_mk like '".$idmk."' and semester_aktif like '".$semaktif."'";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getSemesterAktif()
{
	$sql = "select id_semester from Semester where active_semester = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getNamaMK($id)
{
	$sql = "select nama_mk from Mata_Kuliah where id_mk = " . $id;
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;

}

function newRegistrationDateChecker()
{
	$sql = "select re_reg_begin, re_reg_end from Semester where Active_Semester = 1";
	$r = mysql_query($sql);
	$begin = mysql_result($r, 0, 0);
	$end = mysql_result($r, 0, 1);
	
	if((date('Y-m-d')>=$begin) && (date('Y-m-d')<=$end)){}
	else
	{
		$pass = 'Location: http://ut-taiwan.org/registrasilama/closed.php';
        header( $pass ) ;
	}
}

function newUlanganDateChecker()
{
	$sql = "select ulang_reg_begin, ulang_reg_end from Semester where Active_Semester = 1";
	$r = mysql_query($sql);
	$begin = mysql_result($r, 0, 0);
	$end = mysql_result($r, 0, 1);
	
	if((date('Y-m-d')>=$begin) && (date('Y-m-d')<=$end)){}
	else
	{
		$pass = 'Location: http://ut-taiwan.org/ujianulangan/closed.php';
        header( $pass ) ;
	}
}

function printNamaMKOnJadwalSemester($jam, $semesterke, $jurusan)
{
	$sql = "Select NAMA_MK from Mata_Kuliah Where SEMESTER LIKE '".$semesterke."' and JADWAL_UJIAN LIKE '".$jam."'" . " AND PROGSTUDI_ID = " . $jurusan;;
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getAngkatanMhsByID($id)
{
	$sql = "select ANGKATAN_MHS from Mahasiswa where ID_MHS LIKE '" . $id . "'";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getJurusanMhsByID($id)
{
	$sql = "select PROGSTUDI_ID from Mahasiswa where ID_MHS LIKE '" . $id . "'";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getSemesterKeByID($id)
{
	$angkatan = getAngkatanMhsByID($id);
	$sql = "select count(ID_SEMESTER) from Semester where ID_SEMESTER > '" . $angkatan. "'"; 
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	$semesterke = $result + 1;
	return $semesterke;
}
?>