<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	newRegistrationDateChecker();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Mahasiswa Baru Online - Universitas Terbuka Taiwan</title>
   <script language="JavaScript" type="text/javascript">
    function jcap(form){

		 if (form.nama.value == "") {
			alert( "Harap masukkan nama anda." );
			form.nama.focus();
			return false ;
		  }
		  else if (form.alamat.value == "") {
			alert( "Harap masukkan alamat anda." );
			form.alamat.focus();
			return false ;
		  }
		  else if (form.distriktaiwan.value == "") {
			alert( "Harap masukkan Distrik Kota Taiwan anda." );
			form.distriktaiwan.focus();
			return false ;
		  }
		  else if (form.kotataiwan.value == "") {
			alert( "Harap masukkan Kota Taiwan anda." );
			form.kotataiwan.focus();
			return false ;
		  }
		  else if (form.kodepostaiwan.value == "") {
			alert( "Harap masukkan kode pos Taiwan anda" );
			form.kodepostaiwan.focus();
			return false ;
		  }
		  else if (form.telepon.value == "") {
			alert( "Harap masukkan nomor telepon anda." );
			form.telepon.focus();
			return false ;
		  }
		  else if (form.handphone.value == "") {
			alert( "Harap masukkan nomor handphone anda." );
			form.handphone.focus();
			return false ;
		  }
		  else if (form.email.value == "") {
			alert( "Harap masukkan email anda." );
			form.email.focus();
			return false ;
		  }
		  else if (form.facebook.value == "") {
			alert( "Harap masukkan facebook anda." );
			form.facebook.focus();
			return false ;
		  }
		  else if (form.tempatlahir.value == "") {
			alert( "Harap masukkan tempatlahir anda." );
			form.tempatlahir.focus();
			return false ;
		  }
		  else if (form.tahunijazah.value == "") {
			alert( "Harap masukkan tahun ijazah anda." );
			form.tahunijazah.focus();
			return false ;
		  }
		  else if (form.namaibu.value == "") {
			alert( "Harap masukkan nama Ibu anda." );
			form.namaibu.focus();
			return false ;
		  }
		  /*else if (form.norekening.value == "") {
			alert( "Harap masukkan nomor rekening anda." );
			form.norekening.focus();
			return false ;
		  }
		  else if (form.namabank.value == "") {
			alert( "Harap masukkan nama bank anda." );
			form.namabank.focus();
			return false ;
		  }*/
		  return true;
  }
  </script>
</head>

<body>
<div id="stylized" class="myform">
<form name="1" action="submit.php" method="post" onsubmit="return jcap(this);">
<h1>Pendaftaran Mahasiswa Baru Universitas Terbuka Online</h1>
<h1>Ditolak !</h1>
<p>Sistem mencatat bahwa anda pernah melakukan pendaftaran untuk Angkatan <? echo getTotalAngkatan();?> (<? echo cetakSemesterAktif();?>).
Jika anda ingin melakukan perubahan terhadap data pendaftaran yang telah anda masukkan, silahkan segera hubungi kontak berikut : <br/>
<br/>
?</p>
</form>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
