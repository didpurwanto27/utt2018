<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h2>Program Studi</h2>
						</div>
					</div>
					
					<p>JENJANG S1 PROGRAM STUDI</p>
					<p>Pada tahun ajaran 2018.1, Universitas Terbuka Taiwan menyediakan 3 pilihan Program Studi, yaitu:
					</p>
					<h3>Manajemen</h3>
					<p>Tujuan</p>
					<ul>
					<li>Menyediakan akses pendidikan tinggi di bidang ilmu manajemen/bisnis yang berkualitas dunia bagi masyarakat yang seluas-luasnya melalui sistem PTTJJ.</li>
					<li>Menghasilkan lulusan yang memiliki kompetensi akademik dan atau profesional bidang manajemen yang mampu bersaing secara global.</li>
					<li>Meningkatkan partisipasi masyarakat dalam pendidikan berkelanjutan di bidang ilmu manajemen/bisnis guna mewujudkan masyarakat berbasis pengetahuan (knowledge-based society).</li>
					<li>Menghasilkan produk-produk akademik bidang ilmu manajemen/bisnis melalui sistem PTTJJ.</li>
					<li>Meningkatkan kualitas dan kuantitas penelitian dalam bidang manajemen/bisnis.</li>
					<li>Memanfaatkan dan mendiseminasikan hasil kajian keilmuan bidang manajemen/bisnis untuk menjawab tantangan kebutuhan pembangunan nasional.</li>
					<li>Memperkokoh persatuan dan kesatuan bangsa melalui pelayanan pendidikan tinggi bidang ilmu manajemen/bisnis secara luas dan merata.</li>
					<li>Meningkatkan pemahaman lintas budaya dan jaringan kerja sama melalui kemitraan pendidikan bidang manajemen/bisnis pada tingkat lokal, nasional dan global.</li>
					</ul>
					<br />
					<p><b>SKS</b> </p>
					<p>Dalam 8 semester, SKS yang harus ditempuh adalah sebanyak 144 untuk mahasiswa yang melakukan Registrasi sebelum, saat dan setelah 2016/2017.2.</p>
					<b><p>Kurikulum</p></b>


					<br>
					<br>
					<h3>Bahasa Inggris (Penerjemahan)</h3>
					<p>Tujuan</p>
					<ul>
					<li>Menghasilkan lulusan yang memiliki kompetensi dalam menerjemahkan berbagai jenis teks (discourse genre) dari Bahasa Inggris ke bahasa Indonesia, dan sebaliknya.</li>
					<li>Menghasilkan berbagai temuan penelitian di bidang kajian penerjemahan melalui pendekatan yang relevan dengan pendidikan terbuka dan jarak jauh.</li>
					<li>Menghasilkan berbagai jenis produk layanan di bidang penerjemahan berdasarkan kebutuhan masyarakat.</li>
					</ul>
					<br />
					<b><p>SKS</p></b>
					<p>Dalam 8 semester, SKS yang harus ditempuh adalah sebanyak 144.</p>
					<b><p>Kurikulum</p></b>

					<br>
					<br>
					<h3>Ilmu Komunikasi</h3>
					<p>Tujuan</p>
					Menghasilkan lulusan ilmu komunikasi yang mampu menggunakan konsep, teori dan metode ilmu komunikasi untuk menganalisis permasalahan komunikasi sesuai dengan teori dan perkembangan bidang komunikasi.
					Setelah menyelesaikan pendidikan, mahasiswa S1 Ilmu Komunikasi memiliki kemampuan:
					<ul>
						<li>Menerapkan konsep-konsep dan kaidah kehidupan bermasyarakat, berbangsa dan bernegara Indonesia dalam kehidupan sehari-hari dana dalam menunjang keilmuannya.</li>
						<li>Menjelaskan konsep-konsep dasar ilmu sosial dan ilmu politik dan mengkaitkannya dengan teori dan konsep ilmu komunikasi.</li>
						<li>Menganalisis perubahan di bidang sosial, politik dan ekonomi khususnya yang berkaitan dengan dinamika dan fenomena serta permasalahan komunikasi.</li>
						<li>Melakukan kajian di bidang komunikasi.</li>
					</ul>
					<b><p>SKS</p></b>
					<p>Dalam 8 semester, SKS yang harus ditempuh adalah sebanyak 144.</p>
					<b><p>Kurikulum</p></b>
					<p>Jurusan-jurusan selain di atas bisa saja diusulkan di tahun-tahun mendatang sepanjang jurusan tersebut dibuka oleh UT Pusat, bukan jurusan yang mengharuskan adanya praktikum di Lab, dan calon peminatnya adalah lebih dari 30 orang.</p>



					
				</article>
				
			</div>
			<!--div class="col-lg-4">

				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
					
					<!-- <div class="widget">
						<h5 class="widgetheading">Kontak</h5>
						Fatya (Taipei) 0984218655<br />
						Nisa (Chungli) 0986481240<br />
						Naufal (Tainan) 097625445<br />
					</div> -->
			<!--/div-->
		</div>
	</div>
</section>
<br>
<br>