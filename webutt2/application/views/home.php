<aside id="colorlib-hero">
	<div class="flexslider">
		<ul class="slides">
	   	<!--li style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_1.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
		   				<div class="slider-text-inner text-center">
		   					<!--h1>Best Online Learning System</h1>
		   					<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p-->
		   				<!--/div>
		   			</div>
		   		</div>
	   		</div>
	   	</li-->
	   	<li style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_2.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
		   				<div class="slider-text-inner text-center">
		   					<!--h1>Online Free Course</h1>
		   					<p><a href="#" class="btn btn-primary btn-lg btn-learn">Free Trial</a></p-->
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	   	<li style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_3.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
		   				<div class="slider-text-inner text-center">
		   					<!--h1>Education is a Key to Success</h1>
		   					<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p-->
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>
	   	<li style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_4.jpg);">
	   		<div class="overlay"></div>
	   		<div class="container-fluid">
	   			<div class="row">
		   			<div class="col-md-8 col-sm-12 col-md-offset-2 slider-text">
		   				<div class="slider-text-inner text-center">
		   					<!--h1>Best Online Learning Center</h1>
		   					<p><a href="#" class="btn btn-primary btn-lg btn-learn">Register Now</a></p-->
		   				</div>
		   			</div>
		   		</div>
	   		</div>
	   	</li>	
	  	</ul>
  	</div>
</aside>

<div id="colorlib-intro">
	<div class="container">
		<div class="row">
			<div class="col-md-4 intro-wrap">
				<div class="intro-flex">
					<div class="one-third color-1 animate-box">
						<span class="icon"><i class="flaticon-market"></i></span>
						<div class="desc">
							<h3>E-Learning UT Taiwan</h3>
							<p><a href="#" class="view-more">Klik Disini</a></p>
						</div>
					</div>
					<div class="one-third color-2 animate-box">
						<span class="icon"><i class="flaticon-open-book"></i></span>
						<div class="desc">
							<h3>Pustaka UT Taiwan</h3>
							<p><a href="http://www.pustaka.ut.ac.id/lib/" class="view-more">Klik Disini</a></p>
						</div>
					</div>
					<div class="one-third color-3 animate-box">
						<div class="desc2">
							<h3>Tanggal Penting!!!</h3>
							
							<p><a class="view-more">02/12/2018</a> 
							<font size="2px" color="white">UAS 1</font></p>

							<p><a class="view-more">09/12/2018</a> 
							<font size="2px" color="white">UAS 2</font></p>

							<p><a class="view-more">03/12/2018-01/01/2019</a> 
							<font size="2px" color="white">Pendaftaran Mahasiswa Baru dan Pesan Modul Part 1</font></p>

							<p><a class="view-more">03/12/2018-21/01/2019</a> 
							<font size="2px" color="white">Pendaftaran Mahasiswa Lama dan Pesan Modul Part 2</font></p>

							<p><a class="view-more">03/12/2018-13/02/2019</a> 
							<font size="2px" color="white">Pendaftaran UJUL</font></p>

							<p><a class="view-more">23/12/2018</a> 
							<font size="2px" color="white">Pendaftaran Mahasiswa Lama Offline</font></p>

							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="about-desc animate-box">
					<h2>Selamat Datang di Universitas Terbuka Taiwan</h2>
					<p>Universitas Terbuka Taiwan adalah Universitas yang membuka kesempatan bagi para Pekerja Migran Indonesia untuk mendapatkan layanan pendidikan strata perguruan tinggi di Taiwan. Sistem perkuliahan dilakukan secara online dan offline dengan sistem yang terpadu.</p>
					<div class="fancy-collapse-panel">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                 <div class="panel panel-default">
                     <div class="panel-heading" role="tab" id="headingOne">
                         <h4 class="panel-title">
                             <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Kapan Pendaftaran UT Taiwan dibuka?
                             </a>
                         </h4>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                         <div class="panel-body">
                             <div class="row">
							      		<div class="col-md-6">
							      			<p>Pendaftaran UT Taiwan akan dibuka pada <span style="background-color: #FFFF00">03/12/2018-01/01/2019</span>. Adapun prosedur pendaftaran akan kami infokan nanti. </p>
							      		</div>
							      		<div class="col-md-6">
							      			<p><b><font color="red">SISTEM PENDAFTARAN BELUM DIBUKA</font></b>.</p>
							      		</div>
							      	</div>
                         </div>
                     </div>
                 </div>
                 <div class="panel panel-default">
                     <div class="panel-heading" role="tab" id="headingTwo">
                         <h4 class="panel-title">
                             <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Registrasi untuk Mahasiswa Lama
                             </a>
                         </h4>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                         <div class="panel-body">
                             <div class="row">
							      		<div class="col-md-6">
							      			<p>Registrasi untuk Mahasiswa lama saat ini belum dibuka. Registrasi akan bisa dilaksanakan pada <span style="background-color: #FFFF00">03/12/2018-21/01/2019. </span></p>
							      		</div>
							      		<div class="col-md-6">
							      			<p><b><font color="red">SISTEM REGISTRASI BELUM DIBUKA</font></b>.</p>
							      		</div>
							      	</div>
                         </div>
                     </div>
                 </div>
                 <div class="panel panel-default">
                     <div class="panel-heading" role="tab" id="headingThree">
                         <h4 class="panel-title">
                             <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Pemesanan Modul
                             </a>
                         </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                         <div class="panel-body">
                             <p>Untuk pemesanan modul, bisa beli secara mandiri atau pun kolektif. Sistem pemesanan akan kami buka <span style="background-color: #FFFF00">03/12/2018-01/01/2019</span> untuk part pertama dan <span style="background-color: #FFFF00">03/12/2018-21/01/2019</span> untuk part kedua. Pemesanan akan dikoordinasi oleh divisi Administrasi.</p>	
                         </div>
                     </div>
                 </div>
              </div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="colorlib-services">
	<div class="container">
		<div class="row">
			<div class="col-md-3 text-center animate-box">
				<div class="services">
					<span class="icon">
						<i class="flaticon-books"></i>
					</span>
					<div class="desc">
						<h3>Lokasi TTM Offline:<br /> ISOHO</h3>
						<p>Lantai 6 ISOHO 地理位置：重慶南路一段10號六樓，台北捷運站前地下街 Z10 號出口。</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 text-center animate-box">
				<div class="services">
					<span class="icon">
						<i class="flaticon-books"></i>
					</span>
					<div class="desc">
						<h3>Lokasi TTM Offline:<br /> Primus Instructor</h3>
						<p>PRIMUS Gedung B 館 lantai 10 : 台北市中正區館前路71號10樓 ( 國泰世華銀行樓上)</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 text-center animate-box">
				<div class="services">
					<span class="icon">
						<i class="flaticon-books"></i>
					</span>
					<div class="desc">
						<h3>Lokasi Registrasi <br />Tainan</h3>
						<p>Untuk tempat registrasi offline di Tainan akan kami informasikan nanti.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 text-center animate-box">
				<div class="services">
					<span class="icon">
						<i class="flaticon-books"></i>
					</span>
					<div class="desc">
						<h3>Lokasi Registrasi <br />ZHongli</h3>
						<p>Untuk tempat registrasi offline di Zhongli akan kami informasikan nanti.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="colorlib-counter" class="colorlib-counters" style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-4 col-sm-6 animate-box">
						<div class="counter-entry">
							<span class="icon"><i class="flaticon-book"></i></span>
							<div class="desc">
								<span class="colorlib-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50"></span>
								<span class="colorlib-counter-label">Mata Kuliah</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 animate-box">
						<div class="counter-entry">
							<span class="icon"><i class="flaticon-student"></i></span>
							<div class="desc">
								<span class="colorlib-counter js-counter" data-from="0" data-to="207" data-speed="5000" data-refresh-interval="50"></span>
								<span class="colorlib-counter-label">Mahasiswa Aktif</span>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 animate-box">
						<div class="counter-entry">
							<span class="icon"><i class="flaticon-professor"></i></span>
							<div class="desc">
								<span class="colorlib-counter js-counter" data-from="0" data-to="35" data-speed="5000" data-refresh-interval="50"></span>
								<span class="colorlib-counter-label">Tutor Aktif</span>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<div class="colorlib-classes colorlib-light-grey">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Sistem Kuliah Online</h2>
				<p>Sistem perkuliahan online memaksimalkan keterbatasan waktu yang dimiliki oleh para Pekerja Migran Indonesia supaya tetap mendapatkan porsi mata kuliah yang sesuai. Mahasiswa bisa berkomunikasi dengan tutor dengan sistem perkuliahan online di waktu malam hari selepas jam kerja.</p>
			</div>
		</div>
		
	</div>	
</div>

<div id="colorlib-testimony" class="testimony-img" style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Testimoni</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="row animate-box">
					<div class="owl-carousel1">
						<div class="item">
							<div class="testimony-slide">
								<div class="testimony-wrap">
									<blockquote>
										<span>Nur Hidayat</span>
										<p>Untuk mencapai kebahagian dunia akhirat tentunya kita harus memiliki ilmu ,UT taiwan merupakan pilihan terbaik bagi kami para PMI untuk meraih cita cita,meningkatkan taraf pendidikan dan ekonomi, UT taiwan the best choice.</p>
									</blockquote>
									<div class="figure-img" style="background-image: url(<?php echo base_url('public/')?>/images/person1.jpg);"></div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-slide">
								<div class="testimony-wrap">
									<blockquote>
										<span>Chusnul Chotimah</span>
										<p>Kekeluargaan dan juga profesionalitas adalah dua nilai yang menjadi landasan dasar Badan Pelaksana UT Taiwan. banyak ilmu yang didapatkan dari mahasiswa, tutor, pengurus dan tentu nya UT Pusat (UT Jakarta). Semangat untuk mencapai cita-cita ! dimanapun berada jangan berhenti untuk belajar. tidak ada kata terlambat untuk belajar! Jiayou!.</p>
									</blockquote>
									<div class="figure-img" style="background-image: url(<?php echo base_url('public/')?>/images/Chusnul.jpg);"></div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="testimony-slide">
								<div class="testimony-wrap">
									<blockquote>
										<span>M. Akmalul Ulya</span>
										<p>Kerja sosial dengan kerjaan seabrek. Alhamdulillah banyak ilmu dan kepuasan yang bisa didapatkan dari interaksi dengan rekan-rekan mahasiswa, tutor, dan juga pengurus.</p>
									</blockquote>
									<div class="figure-img" style="background-image: url(<?php echo base_url('public/')?>/images/akmalul.jpg);"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="colorlib-trainers">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Badan Pelaksana</h2>
				<p>Badan Pelaksana UT Taiwan adalah organisasi yang bekerja secara suka rela untuk mengorganisir sistem perkuliahan di UT Taiwan</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-3 animate-box">
				<div class="trainers-entry">
					<div class="trainer-img" style="background-image: url(<?php echo base_url('public/')?>/images/Chusnul.jpg)"></div>
					<div class="desc">
						<h3>Chusnul Chotimah</h3>
						<span>Rektor UT Taiwan</span>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-3 animate-box">
				<div class="trainers-entry">
					<div class="trainer-img" style="background-image: url(<?php echo base_url('public/')?>/images/hana.jpg)"></div>
					<div class="desc">
						<h3>Hana Mutialif</h3>
						<span>Bendahara</span>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-3 animate-box">
				<div class="trainers-entry">
					<div class="trainer-img" style="background-image: url(<?php echo base_url('public/')?>/images/awan.jpg)"></div>
					<div class="desc">
						<h3>Awan Gurun Gunarso</h3>
						<span>Sekretaris</span>
					</div>
				</div>
			</div>

			
		</div>
	</div>
</div>

<div class="colorlib-event">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Event Terdekat</h2>
				
			</div>
		</div>
		<div class="event-flex row-pb-sm">
			<div class="half event-img animate-box" style="background-image: url(<?php echo base_url('public/')?>/images/event.jpg);">
			</div>
			<div class="half">
				<div class="row">
					<div class="col-md-12 animate-box">
						<div class="event-entry">
							<div class="desc">
								<p class="meta"><span class="day">02</span><span class="month">Des</span></p>
								<h2><a href="event.html">Pelaksanaan Ujian Akhir Semester part 1</a></h2>
							</div>
							<div class="location">
								<span class="icon"><i class="icon-map"></i></span>
								<p>Lantai 6 ISOHO 地理位置：重慶南路一段10號六樓，台北捷運站前地下街 Z10 號出口。 atau PRIMUS Gedung B 館 lantai 10 : 台北市中正區館前路71號10樓 ( 國泰世華銀行樓上)</p>
							</div>
						</div>
					</div>
					<div class="col-md-12 animate-box">
						<div class="event-entry">
							<div class="desc">
								<p class="meta"><span class="day">09</span><span class="month">Des</span></p>
								<h2><a href="event.html">Pelaksanaan Ujian Akhir Semester part 2</a></h2>
							</div>
							<div class="location">
								<span class="icon"><i class="icon-map"></i></span>
								<p>Lantai 6 ISOHO 地理位置：重慶南路一段10號六樓，台北捷運站前地下街 Z10 號出口。 atau PRIMUS Gedung B 館 lantai 10 : 台北市中正區館前路71號10樓 ( 國泰世華銀行樓上)</p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>


<!--div class="colorlib-blog colorlib-light-grey">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Berita Terbaru</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 animate-box">
				<article class="article-entry">
					<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url('public/')?>/images/blog-1.jpg);">
						<p class="meta"><span class="day">18</span><span class="month">Apr</span></p>
					</a>
					<div class="desc">
						<h2><a href="blog.html">Pelaksanaan TTM Offline 2</a></h2>
						<p class="admin"><span>Posted by:</span> <span>James Smith</span></p>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
					</div>
				</article>
			</div>
			<div class="col-md-6">
				<div class="f-blog animate-box">
					<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url('public/')?>/images/blog-1.jpg);">
					</a>
					<div class="desc">
						<h2><a href="blog.html">How to Create Website in Scratch</a></h2>
						<p class="admin"><span>04 March 2018</span></p>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
					</div>
				</div>
				<div class="f-blog animate-box">
					<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url('public/')?>/images/blog-2.jpg);">
					</a>
					<div class="desc">
						<h2><a href="blog.html">How to Convert PSD File to HTML File?</a></h2>
						<p class="admin"><span>04 March 2018</span></p>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
					</div>
				</div>
				<div class="f-blog animate-box">
					<a href="blog.html" class="blog-img" style="background-image: url(<?php echo base_url('public/')?>/images/blog-3.jpg);">
					</a>
					<div class="desc">
						<h2><a href="blog.html">How to Build Games App in Mobile</a></h2>
						<p class="admin"><span>04 March 2018</span></p>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div-->


<div id="colorlib-subscribe" class="subs-img" style="background-image: url(<?php echo base_url('public/')?>/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
				<h2>Kontak Kami</h2>
				<p>Silahkan menghubungi kami di: pengurusuttaiwan@gmail.com</p>
			</div>
		</div>
		
	</div>
</div>

</div>



