<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Cara Pembayaran</a></h3>
						</div>
					</div>
					<ol>
						<li>Transfer sejumlah uang untuk <strong >mahasiswa lama<br /></strong>Uang Kuliah <span >sebesar </span><strong><span style="background-color: #FFFF00"><font color="brown">4500 NT atau IDR 1.750.000 </font></span> <br /></strong>Uang Operasional sebesar <strong><font color="brown"><span style="background-color: #FFFF00">2000 NT atau IDR 970.000,00</span></font><br /></strong>Uang Ujian Ulang (<strong>JIKA ADA,</strong> sebesar <font color="brown"><span style="background-color: #FFFF00">300 NT atau IDR 120.000</span></font> per SKS).
						<br />
						<br />
						<font color='green'><b><u>Bank Taiwan:</u></b></font>
						  <table>
							<tr>
								<td> Bank </td>
								<td> <b>: Hua Nan Bank  </b>
							<tr>						  
							<tr>
								<td> Atas nama </td>
								<td> <b>: Ariana Tulus Purnomo  </b>
							<tr>
							
							<tr>
								<td> Bank Code </td>
								<td> <b>: 008</b>
							<tr>
							<tr>
								<td> Swift Code </td>
								<td> <b>: HNBKTWTP</b>
							<tr>
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Taita (National Taiwan University) </b>
							<tr>							
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 154-20-047668-7 </b>
							<tr>
							<tr>
								<td> No Telp. </td>
								<td> <b>:  +886-974-086-541 </b>
							<tr>							
						  </table>
						  
						  <br />
						  <font color='green'><b><u>Atau di Bank Indonesia:</u></b></font>
						  <table>
							<tr>
								<td> Bank </td>
								<td> <b>: Bank BNI  </b>
							<tr>
							<tr>
								<td> Atas Nama </td>
								<td> <b>: Hana Mutialif Maulidiah </b>
							<tr>
							
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Tanjung Perak</b>
							<tr>
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 0296233345 </b>
							<tr>
							<tr>
								<td> No. Telp </td>
								<td> <b>: +886-973-453-283 </b>
							<tr>
						  </table>
						</li>
						<br />
						
						<li>
							<span >
								Pengiriman uang kuliah &amp; uang operasional <strong>silahkan dijadikan satu.</strong> 
									<br />
								
								<strong > 
									<span style="color: green;">>> UNTUK MAHASISWA LAMA 
									</span>
									<br />
								</strong>
								
								<font color="brown"><span style="background-color: #FFFF00">UK (4500 NT atau IDR 1.750.000) + UO (2000 NT atau IDR  970.000,00)</span></font>
								<br />
							
									Total transfer : <font color="brown"><span style="background-color: #FFFF00">6500 NT atau IDR 2.720.000</span> </font>
									<br />
									<br />
							
							</strong>
						</li>
						
						<li>
								<span >
									<span >Contoh Slip Setoran yang
									</span>
									<span >
										<span>
											<strong> BENAR
											</strong>
										</span>
										<br />
										
										<p style="color: red;">PENTING : WAJIB mencantumkan NAMA PENGIRIM untuk kemudahan konfirmasi transfer.
										</p>
									
									</span>
								</span>
						</li>
						<p><img src="<?php echo base_url('public/images/contoh_rek.jpg')?>" border="0" width="690" height="326" /></p>

						<p><strong><br /></strong><strong>Contoh slip pembayaran yang <span style="color: red;">SALAH</span></strong></p>
						<p><img src="<?php echo (base_url('public/') . '/images/slip setoran salah.jpg')?>" border="0" width="690" height="326" /></p>
						
						<li>
						<p>Setelah transfer berhasil,  silahkan  <a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page" class="btn btn-success">KONFIRMASI </a><br /><span >
						
						
						</ol>
						<div class="alert alert-warning">
						Jika uang pembayaran belum diterima bendahara, Badan Pelaksana UT Taiwan </span><strong >akan menghubungi mahasiswa</strong><span > yang bersangkutan melalui telepon atau email.</span></p></li> 
						</div>
						<p><strong><span style="text-decoration: underline;"><br />CATATAN<br /></span></strong><em >* </em>Uang kuliah dan operasional <strong>SUDAH HARUS LUNAS MAKSIMAL <font color="brown"><span style="background-color: #FFFF00">TO BE DETERMINED</span></font> (untuk mahasiswa lama)</strong></strong>. Pembayaran melewati tanggal tersebut, maka mahasiswa lama dianggap <font color="red">cuti/tidak mengikuti semester selanjutnya</font>.</p>

					
					
					<br /><br />
					
				</article>
			</div>
		</div>
	</div>
</section>
<br>
<br>