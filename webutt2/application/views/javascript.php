<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url('public/')?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url('public/')?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url('public/')?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url('public/')?>/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url('public/')?>/js/jquery.stellar.min.js"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url('public/')?>/js/jquery.flexslider-min.js"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url('public/')?>/js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url('public/')?>/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url('public/')?>/js/magnific-popup-options.js"></script>
	<!-- Counters -->
	<script src="<?php echo base_url('public/')?>/js/jquery.countTo.js"></script>
	<!-- Main -->
	<script src="<?php echo base_url('public/')?>/js/main.js"></script>

	</body>
</html>


