<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Sekilas</a></h3>
						</div>
					</div>
					
					
					Universitas Terbuka (UT) merupakan Perguruan Tinggi Negeri ke-45 yang didirikan pada tanggal 4 September 1984. Berdasarkan katalog UT , tujuan didirikannya adalah:
					<ol>
					<li>Memberikan kesempatan yang luas bagi warga negara Indonesia dan warga negara asing, di mana pun tempat tinggalnya, untuk memperoleh pendidikan tinggi;</li>
					<li>Memberikan layanan pendidikan tinggi bagi mereka, yang karena bekerja atau karena alasan lain, tidak dapat melanjutkan pendidikannya di perguruan tinggi tatap muka; </li>
					<li>Mengembangkan program pendidikan akademik dan profesional sesuai dengan kebutuhan nyata pembangunan yang belum banyak dikembangkan oleh perguruan tinggi lain.</li>
					</ol>
					<p> </p>
					<p>UT mengenal sistem belajar jarak jauh dan terbuka. Jarak jauh berarti sistem pembelajaran dilakukan tidak melalui proses tatap muka melainkan melalui media cetak ataupun media lainnya. Terbuka berarti tidak membatasi usia, tahun ijasah, masa belajar, waktu registrasi dan frekwensi mengikuti ujian. Dalam hal ini hanya satu syarat untuk mengikuti program UT yaitu menamatkan jenjang Sekolah Menengah Atas (SMA) atau yang sederajat.</p>
					<p> </p>
					<p>Sistem belajar mahasiswa UT adalah belajar secara mandiri. Dalam hal ini mahasiswa diharapkan dapat mengatur waktu belajar sendiri ataupun berkelompok tanpa harus dijadwalkan oleh UT. Pihak UT sendiri akan memberikan bahan ajar yang dapat dipelajari sendiri ataupun berkelompok. Selain itu UT juga menyediakan perpustakaan online, tutorial online dan bahan ajar yang dapat dijalankan di komputer. Mahasiswa dapat meminta informasi tentang bantuan belajar kepada Unit Program Belajar Jarak Jauh Universitas Terbuka (UPBJJ-UT) yang saat ini tersedia di tiap propinsi di Indonesia. Secara umum keberhasilan seseorang mengikuti program UT bergantung pada kemauan untuk belajar mandiri dan disiplin diri.</p>
					<p> </p>
					<p>UT juga mengenal Sistem Kredit Semester (SKS), satu sks disetarakan dengan tiga modul bahan ajar cetak. Satu modul terdiri atas 40-50 halaman, sehingga bahan ajar dengan bobot 3 sks berkisar antara 360-450 halaman, tergantung pada jenis mata kuliahnya. Berdasarkan hasil penelitian, kemampuan membaca dan memahami rata-rata mahasiswa adalah 5-6 halaman per jam sehingga untuk membaca bahan ajar dengan bobot 3 sks diperlukan waktu sekitar 75 jam. Apabila satu semester mempunyai waktu 16 minggu, maka waktu yang diperlukan untuk membaca bahan ajar dengan bobot 3 sks adalah 75 jam dibagi 16 minggu, atau kurang lebih 5 jam per minggu. Misalnya, mahasiswa mengambil 15 sks/semester, maka yang bersangkutan harus mengalokasikan waktu belajar sebanyak 15 sks dibagi 3 sks kali 5 jam = 25 jam per minggu atau kira-kira 5 jam per hari (1 minggu dihitung 5 hari belajar)</p> 


					
				</article>
				
			</div>
			<!--div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
			</div-->
		</div>
	</div>
</section>
<br>
<br>