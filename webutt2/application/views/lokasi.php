<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Lokasi Pendaftaran</a></h3>
						</div>
					</div>
					<div class="alert alert-info">
						
					Pembayaran tunai dapat dilakukan pada REGISTRASI ON THE SPOT di lokasi sebagai berikut:<br /><br />
						Taipei: Kantor KDEI Taipei<br /> <font color="brown"><span style="background-color: #FFFF00">(Tanggal 23 dan 30 Desember 2018 pukul 10.00-16.00)</span></font>, 
						<br /> <a class="btn btn-warning" href="https://goo.gl/maps/fpdmpiEdVC52">PETA LOKASI</a><br /><br />
						Tainan: Toko Maju KAMTO, Tainan <br /><font color="brown"><span style="background-color: #FFFF00">(Tanggal 23 dan 30 Desember 2018 pukul 10.00-16.00)</span></font>, 
						<br /><a class="btn btn-warning" href="https://goo.gl/maps/wFeQYqY9sN52">PETA LOKASI</a><br /><br />
						Chungli: Restoran Pondok Mami* (Belakang stasiun Chungli), Chungli<br /><font color="brown"><span style="background-color: #FFFF00">(Tanggal 23 dan 30 Desember 2018 pukul 10.00-16.00)</span></font>,
						<br /> <a class="btn btn-warning"  href="https://goo.gl/maps/yvfhDwEES992">PETA LOKASI</a> <br />
					</div>	
					
				</article>
				
			</div>
			<!--div class="col-lg-4">

				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
					
					<!-- <div class="widget">
						<h5 class="widgetheading">Kontak</h5>
						Fatya (Taipei) 0984218655<br />
						Nisa (Chungli) 0986481240<br />
						Naufal (Tainan) 097625445<br />
					</div> -->
			<!--/div-->
		</div>
	</div>
</section>
<br>
<br>