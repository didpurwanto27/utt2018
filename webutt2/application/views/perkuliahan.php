<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Perkuliahan</a></h3>
						</div>
					</div>
					
					
					<p>1.                     Bagaimana cara belajar di UT?</p>
					<p> </p>
					<p>Sistem Pembelajaran</p>
					<p>UT memiliki sistem pembelajaran jarak jauh dan terbuka. Jarak jauh berarti pembelajaran tidak dilakukan secara langsung melainkan melalui media berupa cetak (buku modul) maupun non cetak (online, video dll), sedangkan terbuka berarti tidak ada pembatasan usia, tahun ijazah, masa belajar dan frekuensi mengikuti ujian. Batasan hanya harus sudah menamatkan jenjang pendidikan sekolah menengah atas (SMA).</p>
					<p>Metode Pembelajaran</p>
					<p>Mahasiswa UT diharapkan dapat belajar secara mandiri. Cara belajar mandiri menghendaki mahasiswa untuk belajar atas prakarsa atau inisiatif sendiri. Belajar mandiri dapat dilakukan secara sendiri ataupun berkelompok, baik dalam kelompok belajar maupun dalam kelompok tutorial.</p>
					<p> </p>
					<p>2.                     Bagaimana teknis pembelajarannya?</p>
					<p> </p>
					<p>Tutorial di UT diselenggarakan dalam 2 model, yaitu:</p>
					<p>A. Tutorial Online (TUTON)</p>
					<p>Tutorial Online atau tutorial elektronik yang dapat diakses melalui menu pada situs UT PUSAT. Tutorial video dengan teknologi streaming video melalui jaringan internet. Melalui layanan ini mahasiswa dapat menyaksikan hasil rekaman tutorial yang dahulu pernah disiarkan melalui TVRI/TPI.</p>
					<p>Jam belajar: Bebas</p>
					<p>B. Tutorial Tatap Muka (TTM)</p>
					<p>TTM sendiri ada 2 jenis;</p>
					<p>a)                  TTM Online (6 kali)</p>
					<p>Proses pengajaran akan diajarkan langsung oleh tutornya secara online (teknologi streaming) dengan media Widziq.  Cara untuk mengakses Widziq klik disini!</p>
					<p>Bagi mahasiswa yang berhalangan hadir pada TTM online, anda dapat mengunduh/menonton stream rekaman kuliah dan juga materi perkuliahan di halaman mata kuliah tersebut. Silahkan masuk ke halaman site jurusan anda (manajemen, inggris, atau komunikasi), kemudian klik mata kuliah tersebut, dan silahkan klik tanggal/bab/topik yangmana anda ingin me-review.</p>
					<p>Jam belajar: Mengikuti jadwal</p>
					<p>b)                  TTM Kelas (2 kali)</p>
					<p>Proses pengajaran dilakukan dikelas yang dipandu oleh tutornya secara langsung (face to face). Bagi mahasiswa yang berhalangan hadir pada TTM kelas, anda dapat mengunduh/menonton stream rekaman kuliah dan juga materi perkuliahan di halaman mata kuliah tersebut. Silahkan masuk ke halaman site jurusan anda (manajemen, inggris, atau komunikasi), kemudian klik mata kuliah tersebut, dan silahkan klik tanggal/bab/topik yangmana anda ingin me-review.</p>
					<p>Jam belajar: Mengikuti jadwal</p>
					<p> </p>
					<p>3.                  Jurusan apa saja yang tersedia?</p>
					<p> </p>
					<p>Jenjang pendidikan yang disediakan oleh UT di Taiwan adalah pendidikan S1, dimana akan mendapat ijazah dan gelar S1 apabila dinyatakan lulus pada akhir studinya.</p>
					<p>Bahasa Inggris (Penerjemahan)</p>
					<p>Manajemen</p>
					<p>Ilmu Komunikasi</p>
					<p> </p>
					<p>4.                  Mengapa tidak/belum bisa login ke TUTON?</p>
					<ul>
					<li>Pastikan anda sudah melakukan aktivasi sebelumnya sebagaimana ketentuan dan cara yang benar. Untuk ketentuan dan cara aktivasi silahkan klik disini.</li>
					</ul>
					<p> </p>
					<p>5.                  Tugas dan diskusi yang ada di TUTON apa perlu diikuti semua? Dan mempengaruhi nilai?</p>
					<ul>
					<li>Diskusi boleh diikuti boleh tidak, namun dengan anda aktif berdiskusi maka itu akan dijadikan nilai tambah yang akan dimasukkan ke nilai akhir.</li>
					</ul>
					<p> </p>
					<p>6.                  Mana yang lebih besar porsinya, antara nilai tugas dan diskusi TUTON dengan absensi dan tugas TTM?</p>
					<ul>
					<li>Bagi mata kuliah yg memiliki TTM, maka porsi nilai yang diambil adalah  50% TTM + 50% ujian. Adapun nilai tambahan masih dapat diperoleh apabila mahasiswa tersebut tetap mengikuti TUTON mata kuliah yang di TTM-kan, akan tetapi untuk mengikuti TUTON pada mata kuliah yang memiliki TTM adalah tidak wajib untuk diikuti.</li>
					</ul>
					<p><span style="white-space: pre;"> </span>Bagi matakuliah yang tidak memiliki TTM, hanya TUTON , maka porsi nilainya yang diambil adalah 30%TUTON + 70% ujian</p>
					<p> </p>
					<p>7.                  Mengapa terkadang tidak bisa men-upload tugas TUTON?</p>
					<ul>
					<li>Pastikan tugas yang anda upload sesuai dengan format yang diminta (*.doc, *.ppt, *.pdf, dll) dan pastikan anda tidak melewati jadwal deadline pengumpulan tugas. </li>
					</ul>
					<p> </p>
					<p>8.                  Mengapa saya telat atau tidak mendapat email pemberitahuan tentang adanya materi mata kuliah terbaru yang siap untuk di-download?</p>
					<ul>
					<li>Jika di INBOX email anda tidak ada, coba periksa SPAM anda barangkali email dari div.akademik (<span id="cloak46941">This email address is being protected from spambots. You need JavaScript enabled to view it.</span><script type='text/javascript'>
					 //<!--
					 document.getElementById('cloak46941').innerHTML = '';
					 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
					 var path = 'hr' + 'ef' + '=';
					 var addy46941 = '&#97;k&#97;d&#101;m&#105;k' + '&#64;';
					 addy46941 = addy46941 + '&#117;t-t&#97;&#105;w&#97;n' + '&#46;' + '&#111;rg';
					 document.getElementById('cloak46941').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy46941 + '\'>' + addy46941+'<\/a>';
					 //-->
					 </script>) masuk ke folder tersebut. Jika anda memang tidak menemukannya, maka silahkan unduh materi perkuliahan di halaman mata kuliah tersebut. Caranya, silahkan masuk ke halaman site jurusan anda (manajemen, inggris, atau komunikasi) terlebih dahulu, kemudian klik mata kuliah tersebut, dan silahkan klik tanggal/bab/topik yang tersedia. Jika anda tetap tidak menemukan maka silahkan segera menghubungi divisi akademik.</li>
					</ul>
					<p> </p>
					<p>9.                  Mengapa saya mendapat email dari Widziq yang tidak ada sangkut pautnya dengan kuliah/UT?</p>
					<ul>
					<li>Jika email dari Widziq bukan merupakan invitation/undangan untuk mengikuti kuliah maka silahkan abaikan, karena email-email tersebut bukanlah dikirim oleh UT, namun dari sistem Widziq itu sendiri.</li>
					</ul>
					<p> </p>
					<p>10.              Mengapa TTM online terkadang terputus-putus?</p>
					<ul>
					<li>Pastikan koneksi internet anda lancar, jika semua ternyata benar maka ada kemungkinan jaringan memang tidak stabil. Harap bersabar, dan silahkan beritahu tutornya jika anda ketinggalan dibeberapa bagian saat proses pembelajaran berlangsung.</li>
					</ul>
					<p> </p>
					<p>11.              Saya tidak memahami pengoperasian dasar komputer, mengetik dan internet, apa yang harus saya lakukan?</p>
					<ul>
					<li>Ada baiknya  anda belajar pengoperasian dasar komputer terlebih dahulu kemudian mendaftar di UT. Karena kemampuan untuk operasi komputer adalah kebutuhan wajib mahasiswa UT.</li>
					</ul>
					<p> </p>
					<p>12.              Dimana saya tahu daftar mata kuliah tiap semester?</p>
					<ul>
					<li>Daftar mata kuliah yang disediakan adalah sistem paket silahkan klik link berikut</li>
					</ul>
					<p> </p>
					<p>13.              Jika saya kesulitan terhadap mata kuliah tertentu, apa yang saya lakukan?</p>
					<ul>
					<li>Silahkan kontak langsung ke tutor mata pelajaran tersebut melalui email.</li>
					</ul> 

					
				</article>
				
			</div>
			<!--div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
			</div-->
		</div>
	</div>
</section>
<br>
<br>