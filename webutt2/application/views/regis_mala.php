<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Registrasi Mahasiswa Lama</a></h3>
						</div>
					</div>
					<div class="alert alert-info">
						<b> REGISTRASI MAHASISWA LAMA & REGISTRASI UJIAN ULANG SEMESTER 2019.1 </b> <br />
						- REGISTRASI ONLINE Mahasiswa Lama dapat dilakukan <font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 - 21 Januari 2019</span></font> <br />
						- REGISTRASI ONLINE pengajuan Ujian Ulang dapat dilakukan <font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 – 13 Februari 2019</span></font> <br />
						- REGISTRASI ON THE SPOT (OFFLINE): <font color="brown"><span style="background-color: #FFFF00">23 dan 30 Desember 2018 pukul 10.00-16.00 </span></font><br />
						</div>
					<p>
						
						
						<br />
						<b>Alur Daftar ulang dan pengajuan Ujian Ulang: </b><br />
						
						<ol>
							<li>Mahasiswa Lama melakukan registrasi secara online, registrasi online ini wajib dilakukan oleh seluruh mahasiswa termasuk yang akan melakukan REGISTRASI ON THE SPOT harus terlebih dahulu mengisi data registrasi secara online melalui
							<a class="btn btn-success" href="http://ut-taiwan.org/registrasilama/"> Klik Disini </a>
							
							</li>
							<br />
							<li>Jika ada matakuliah yg ingin diulang bisa melakukan pengajuan ujian ulang melalui <a class="btn btn-success" href="http://ut-taiwan.org/ujianulangan/"> Klik Disini </a>
							</li>
							<br />
							<li>Setelah proses 1 dan 2 selesai, mahasiswa diwajibkan untuk melunasi uang kuliah (UK) <font color="brown"><span style="background-color: #FFFF00">NT$ 4500 / IDR 1.750.000</span></font> dan uang operasional (UO) <font color="brown"><span style="background-color: #FFFF00"> NT$ 2000 / IDR 970.000.</span></font>
							
							<table border="1">
						<tbody>
						<tr>
						<td rowspan="2" width="119">
						<p align="center"><strong>PEMBAYARAN</strong></p>
						</td>
						<td colspan="2" width="331">
						<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">JUMLAH YANG DIBAYARKAN</span></strong></p>
						</td>
						</tr>
						<tr>
						<td width="141">
						<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">UANG KULIAH</span></strong></p>
						</td>
						<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" width="190">
						<p class="MsoNormal" style="mso-margin-top-alt: auto; margin-right: 6.0pt; mso-margin-bottom-alt: auto; margin-left: 6.0pt; text-align: center; line-height: normal;" align="center"><strong><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">UANG OPERASIONAL</span></strong></p>
						</td>
						</tr>
						<tr>
						<td style="width: 89.25pt; padding: 0in 0in 0in 0in;" valign="top" width="119">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Taiwan</span></p>
						</td>
						<td style="width: 105.75pt; padding: 0in 0in 0in 0in;" valign="top" width="141">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">4500 NT</span></p>
						</td>
						<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" valign="top" width="190">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">2000 NT</span></p>
						</td>
						</tr>
						<tr>
						<td style="width: 89.25pt; padding: 0in 0in 0in 0in;" valign="top" width="119">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Indonesia</span></p>
						</td>
						<td style="width: 105.75pt; padding: 0in 0in 0in 0in;" valign="top" width="141">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Rp 1.750.000 </span></p>
						</td>
						<td style="width: 142.5pt; padding: 0in 0in 0in 0in;" valign="top" width="190">
						<p class="MsoNormal" style="margin: 6.0pt; text-align: center; line-height: normal;" align="center"><span style="font-size: 7.5pt; font-family: Verdana, sans-serif;">Rp 970.000</span></p>
						</td>
						</tr>
						</tbody>
						</table>
						<font color = "red">**Transfer UK dan UO dapat dijadikan satu <font color="brown"><span style="background-color: #FFFF00">(NT$ 6500 atau IDR 2.720.000).</span></font></font>
							</li>
							<br />
							<li>Jika ada mata kuliah yang diulang, diwajibkan melunasi Uang Ujian Ulang, sesuai dengan jumlah mata kuliah yg diulang. <font color="brown"><span style="background-color: #FFFF00">NT$ 300 atau IDR 120.000 per SKS</span></font>
							</li>
							<br />
							<li>Uang pembayaran dapat ditransfer ke rekening berikut: <br />
							<font color='green'><b><u>Bank Taiwan:</u></b></font>
							<table>
							<tr>
								<td> Bank </td>
								<td> <b>: Hua Nan Bank  </b>
							<tr>						  
							<tr>
								<td> Atas nama </td>
								<td> <b>: Ariana Tulus Purnomo  </b>
							<tr>

							<tr>
								<td> Bank Code </td>
								<td> <b>: 008</b>
							<tr>
							<tr>
								<td> Swift Code </td>
								<td> <b>: HNBKTWTP</b>
							<tr>
							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Taita (National Taiwan University) </b>
							<tr>							
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 154-20-047668-7 </b>
							<tr>
							<tr>
								<td> No Telp. </td>
								<td> <b>:  +886-974-086-541 </b>
							<tr>							
							</table>

							<br />
							<font color='green'><b><u>Atau di Bank Indonesia:</u></b></font>
							<table>
							<tr>
								<td> Bank </td>
								<td> <b>: Bank BNI  </b>
							<tr>
							<tr>
								<td> Atas Nama </td>
								<td> <b>: Hana Mutialif Maulidiah </b>
							<tr>

							<tr>
								<td> Kantor Cabang </td>
								<td> <b>: Tanjung Perak</b>
							<tr>
							<tr>
								<td> No. Rekening </td>
								<td> <b>: 0296233345 </b>
							<tr>
							<tr>
								<td> No. Telp </td>
								<td> <b>: +886-973-453-283 </b>
							<tr>
							</table>
							</li>
							<br />

							<li>Setelah melakukan pembayaran, mahasiswa DIHARUSKAN untuk melakukan konfirmasi pembayaran melalui dengan klik <a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page" class="btn btn-success">KONFIRMASI </a> dengan mengunggah foto bukti pembayaran (bukan foto selfie atau selca).
							</li>
							<br />
							<li>Pembayaran tunai dapat dilakukan pada REGISTRASI ON THE SPOT di lokasi sebagai berikut:<br />
							<div class="alert alert-warning">
							Taipei: Kantor KDEI Taipei<br /> (Tanggal 10 dan 17 Desember 2017 pukul 10.00-16.00), <a href="https://goo.gl/maps/fpdmpiEdVC52">Here</a><br />
							</div>
							<div class="alert alert-warning">
							Tainan: Toko Maju KAMTO, Tainan <br />(Tanggal 10 dan 17 Desember 2017 pukul 10.00-16.00), <a href="https://goo.gl/maps/wFeQYqY9sN52">Here</a><br />
							</div>
							<div class="alert alert-warning">
							Chungli: Restoran Pondok Mami* (Belakang stasiun Chungli), Chungli<br />(Tanggal 10 dan 17 Desember 2017 pukul 10.00-16.00), <a href="https://goo.gl/maps/yvfhDwEES992">Here</a> <br />
							</div>
							</li>
							<br />
							<li>Mahasiswa lama yang membayar biaya registrasi ulang dan UJUL diluar batas tanggal yang ditentukan maka mahasiswa ybs tidak akan teregistrasi semester ini (2019.1) dan dianggap <font color="red"><b>CUTI</b></font>.
							</li>
							<br />
							<li>Apabila ada pertanyaan terkait rincian biaya, cara pembayaran, dan masalah keuangan lainya bisa add line bendahara: <span style="background-color: #ffffee">@tvt3764t </span>.
							</li>
						</ol>
						
					</p>
				</article>
				
			</div>
			<div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 - 1 Januari 2019</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 - 21 Januari 2019</span></font></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>