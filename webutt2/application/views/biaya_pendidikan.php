<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Biaya Pendidikan</a></h3>
						</div>
					</div>
					<div class="alert alert-info">
					<p><strong>Mahasiswa lama</strong> (Angkatan I 2011.2 hingga Angkatan XII 2018.1) diwajibkan membayar biaya kuliah sebesar <strong>
						<font color="brown"><span style="background-color: #FFFF00">6.500 NT</span></font> </strong><strong>(Uang Kuliah sebesar <font color="brown"><span style="background-color: #FFFF00">4500 NT</span></font>, dan Uang Operasional sebesar <font color="brown"><span style="background-color: #FFFF00">2000 NT</span></font>)</strong>.</p>
					</div>
					
					
					<p>Penjelasan adalah sebagai berikut:</p>
					<ol>
						<b><li>Nominal:
						</li></b>
							<ul>
							<li>Sesuai  ketentuan  dari  UT  Pusat,  uang  kuliah  per  semester  adalah     <font color="brown"> <span style="background-color: #FFFF00">NTD 4.500 atau IDR  1.750.000 </span> </font> untuk semua mahasiswa 
							</li>
							<li>Uang operasional yang telah disepakati sampai saat ini adalah <font color="brown">  <span style="background-color: #FFFF00">NTD 2.000 atau Rp 970.000,00 </span></font> untuk semua mahasiswa
							</li>
							</li>
							<li>Uang pendaftaran mahasiswa baru  adalah <font color="brown"> <span style="background-color: #FFFF00">NTD 1.500 atau Rp 727.500,00 </span></font> khusus untuk mahasiswa baru
							</li>
							<div class="alert alert-warning">
							Catatan: Besaran tersebut adalah untuk jumlah mahasiswa lebih dari atau sama dengan 90  orang; Bila  kurang  dari  itu,  uang  operasional  akan  naik sesuai jumlah mahasiswa yang mendaftar.
							</div>
							</ul>
						<b><li>Perincian biaya
						</li></b>
						<p>- Uang Kulian (UK): ketentuan dari UT Pusat</p>
						<p>- Uang Operasional (UO): </p>
							<ul>
								<li>Konsumsi UAS I dan UAS II mahasiswa </li>
								<li>Biaya Transportasi Bapel</li>
								<li>Akomodasi pengawas ujian dari UT Jakarta</li>
								<li>Biaya lain-lain (ATK, pengepakan, logistik lokal, sosialisasi, dana cadangan)</li>
								<li>Biaya Tutor Konsultasi dan Karil</li>
								<li>Biaya sewa gedung perkuliahan</li>
							</ul>
							<br />
						<p>- Uang Pendaftaran Mahasiswa Baru: </p>
							<ul>
								<li>Registrasi</li>
								<li>Jas almamater</li>
								<li>OSMB (Orientasi Studi Mahasiswa Baru)</li>
							</ul>
					</ol>
					
					<br />
					<div class="alert alert-warning">
					<p>Note: Biaya kuliah dapat berubah sewaktu-waktu, tergantung pada ketentuan dari UT Pusat.</p>
					</div>

				</article>
				
			</div>
			
		</div>
	</div>
</section>

<br>
<br>
