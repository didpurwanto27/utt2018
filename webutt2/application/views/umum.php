<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Umum</a></h3>
						</div>
					</div>
					
					
					<p>1. Apa saja persyaratan calon mahasiswa?</p>
					<ul>
					<li>Satu lembar fotokopi ijazah SLTA/paket C yang telah dilegalisasi oleh pejabat yang berwenang.</li>
					<li>Pasfoto ukuran 2x3 sebanyak 2 lembar</li>
					</ul>
					<p> </p>
					<p>2. Apakah ada pembatasan Umur saat pendaftaran?</p>
					<ul>
					<li>Umur tidak dibatasi saat pendaftaran</li>
					</ul>
					<p> </p>
					<p>3. Bagaimana bila masa kontrak habis sebelum masa studi selesai?</p>
					<ul>
					<li>Studi bisa dilanjutkan di Indonesia dan secara administrasi mengikuti Unit Program Belajar Jarak Jauh (UPBJJ) terdekat dari kota tempat anda tinggal. Data lengkap UPBJJ di setiap kota bisa dilihat di <a href="http://www.ut.ac.id/upbjj-ut/alamat-upbjj-ut.html">http://www.ut.ac.id/upbjj-ut/alamat-upbjj-ut.html</a></li>
					</ul>
					<p> </p>
					<p> </p>
					<p>4. Kapan pembelajaran dimulai?</p>
					<ul>
					<li>Perkuliahan dilakukan sebanyak dua semester dalam satu tahun. Semester pertama dimulai bulan Maret sampai Mei dan semester berikutinya mulai September sampai November, 8x pertemuan Tutorial Tatap Muka (TTM), dan 2x Ujian Akhir</li>
					</ul>
					<p> </p>
					<p> </p>
					<p>5. Bagaimana dengan Ujian Akhir Semester (UAS)? Waktu dan Tempat?</p>
					<ul>
					<li>UAS dilakukan pada akhir semester selama 2 hari, atau 2 hari minggu, tidak melalui internet karena harus dilakukan secara langsung di ruangan/gedung yang disediakan dan akan diawasi oleh perwakilan UT Jakarta. Jadi untuk dua hari itu, harus bisa libur. Apabila ada masalah dengan 2 hari tersebut mungkin pihak KDEI bisa memberikan surat kepada majikan untuk bisa meliburkan anda pada dua hari tersebut.</li>
					</ul>
					<p> </p>
					<p>6. Bagaimana dengan Biaya?</p>
					<ul>
					<li>Uang Kuliah (UK) per-semester adalah Rp. 1.750.000,-, belum termasuk Uang Operasional (UO) untuk pengiriman modul, operasional kegiatan UT (UAS,dll).</li>
					</ul>
					<p> </p>
					<p>7. Apakah ada keringanan dengan biaya?</p>
					<ul>
					<li>Belum ada konfirmasi dari UT Pusat/Jakarta tentang keringanan biaya atau kemungkinan biaya bisa dicicil atau tidak.</li>
					</ul>
					<p> </p>
					<p> </p>
					<p>8. Bagaimana dengan rincian Uang Kuliah (UK) per-semester?</p>
					<ul>
					<li>Registrasi – Rp. 60.000,-</li>
					<li>SPP- Rp. 360.000,- </li>
					<li>Bahan Ajar – Rp. 450.000,- </li>
					<li>Tutorial – Rp. 700.000,- </li>
					<li>Ujian Akhir Semester – 180.000,- </li>
					<li>Jumlah Uang Studi per-semester  berbeda-beda menyesuaikan kurs.<br />2013.1 UK 6000 NTD<br />2013.2 UK 6000 NTD<br />2014.1 UK 5000 NTD<br />2014.2 Uk 5000 NTD<br />dan mulai 2015.1 UK stabil sebesar 4500 NTD</li>
					</ul>
					<p> </p>
					<p>9. Apa saja yang termasuk dalam Uang Operasional (UO)?</p>
					<ul>
					<li>Biaya logistik modul dan formulir Indonesia-Taiwan</li>
					<li>Reward kepada pengurus UT Taiwan</li>
					<li>Akomodasi pengawas ujian dari UT Jakarta</li>
					<li>Sewa gedung pelaksanaan ujian (UAS)</li>
					<li>Biaya lain-lain (ATK, pengepakan, logistik lokal, sosialisasi, dana cadangan)</li>
					<li>Besaran uang dana operasional bergantung pada jumlah mahasiswa yang mendaftar.</li>
					</ul>
					<p> </p>
					<p> </p>
					<p>10. Bagaimana kalau tanggal lahir di ijazah berbeda dengan yang tertera di passport?</p>
					<ul>
					<li>Tanggal lahir yang dipakai adalah yang tertera di ijazah, karena passport tidak diperlukan pada saat pendaftaran.</li>
					</ul>
					<p> </p>
					<p> </p>
					<p>11. Apakah Univeristas Terbuka (UT) termasuk Univeristas Negeri?</p>
					<ul>
					<li>Iya, UT adalalah Universitas Negeri ke-45 yang diresmikan pada tanggal 4 September 1984, berdasarkan Kepres RI No. 41/1984, bisa dilihat di www.ut.ac.id</li>
					</ul>
					<p> </p>
					<p>12. Kenapa yang dbuka hanya 3 jurusan saja untuk di Taiwan?</p>
					<ul>
					<li>Untuk jurusan yang mengharuskan praktium (jurusan terkait IPA, pendidikan/kegurusan (Pendas), jurusan terkait IPS dengan dengan kewajiban praktium) tidak dibuka di Luar Negeri karena tidak adanya fasilitas praktikum.</li>
					<li>Mengingat SDM untuk tutor di Taiwan kebanyakan di 3 jurusan tersebut</li>
					<li>Untuk Pendidikan Dasar (Pendas)/Keguruan pada saat pendaftaran harus membuktikan bahwa dia sedang bekerja sebagai staf pengajar di sekolah tertentu dengan mennujukkan surat keterangan pada saat pendaftaran.</li>
					</ul>
					<p> </p>
					<p> </p>
					<p>13. Bagaimana cara mendaftar?</p>
					<ul>
					<li>Pendaftaran dilakukan secara online, untuk yang di Taipei tempat pendaftaran dilakukan di kantor KDEI, untuk kota lain (Chung-Li, Taichung, dan Tainan) sesuai dengan pengumuman tiap semester. Untuk yang berada di kota selain 4 kota tersebut, silahkan untuk datang ke kota tempat pendaftaran terdekat. Untuk detailnya akan diumumkan kemudian.</li>
					</ul>
					<p> </p>
					<p>14. Bagaimana dengan ijazah yang belum dilegalisir?</p>
					<ul>
					<li>Pada saat pendaftaran, ijazah yang belum dilegalisir bisa digunakan akan tetapi fotokopi ijazah yang sudah dilegalisir harus dikirimkan ke UT jakarta langsung</li>
					</ul>


					
				</article>
				
			</div>
			<!--div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
			</div-->
		</div>
	</div>
</section>
<br>
<br>