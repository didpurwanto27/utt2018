<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Badan Pelaksana</a></h3>
						</div>
					</div>
					
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2018/2019</span></p>
					<table border="0" cellpadding="0">
						<tbody>
							<tr>
							<td>
							<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
								<tbody>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Bpk. Didi Sumedi (Kepala KDEI di Taipei)</span></td>
								</tr>
								<tr>
								<td><span style="font-size: small;">Penanggung Jawab</span></td>
								<td><span style="font-size: small;">:</span></td>
								<td><span style="font-size: small;">Surtasis (Ketua PPI)</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Chusnul Khotimah<br /></span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Awan Gurun Gunarso</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Bendahara</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Koordinator: Hana Mutialif</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;">Anggota: </span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 1. Ariana Tulus P</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Koordinator: Thalita Maysha</span> </td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 1. Junius H</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 2. Siti Sulikhah</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 3. Dyantika PM</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 4. Trizaurah</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 5. Mulyanto</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 6. Muh. Rakhmat Setiawan</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 7. Luthviyah Choirul Muhimmah</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Koordinator: Brigita Chika</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 1. Gayuh Minang Lati</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 2. Bagus Aris Saputra</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 3. Januar Widakdo </span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 4. Arif Cahyo Imawan </span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Koordinator: Raden Hadapiningsyah Kusumoseniarto<br /></span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 1. Zamroji Hariyanto</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 2. Nanda P. Romadhona</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"> </td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Zhongli</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">Alfian<br /></span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;">Tainan :</span></td>
								<td valign="top" width="16"><span style="font-size: small;">:</span></td>
								<td valign="top" width="332"><span style="font-size: small;">1. Muhammad Naufal</span></td>
								</tr>
								<tr>
								<td valign="top" width="251"><span style="font-size: small;"> </span></td>
								<td valign="top" width="16"> </td>
								<td valign="top" width="332"><span style="font-size: small;"> 2. Nugroho</span></td>
								</tr>
								</tbody>
							</table>
							</td>
							</tr>
						</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2018</span></p>
					<table border="0" cellpadding="0">
						<tbody>
						<tr>
						<td>
						<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
						<tbody>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Bpk. Robert James (Kepala KDEI di Taipei)</span></td>
						</tr>
						<tr>
						<td><span style="font-size: small;">Penanggung Jawab</span></td>
						<td><span style="font-size: small;">:</span></td>
						<td><span style="font-size: small;">L Tri Wijaya N Kusuma (Ketua PPI)</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Chusnul Khotimah<br /></span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Madiana Laela</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Koordinator: Ariana Tulus Purnomo</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;">Anggota: </span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 1. Ervin Kusuma Dewi</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Koordinator: Devina Rayzy Perwitasari S.P.</span> </td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 1. Yagus Cahyadi,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 2. Ilma Mufidah,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 3. Muhammad Iqbal,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 4. Yudhistira Candra Bayu,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 5. Thalita Maysha,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 6. Dianthika P. Andini,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 7. Anggara Aji Saputra </span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Koordinator: Asif Ali Zamzami</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 1. Primasari Cahya Wardhani,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 2. Muniroh,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 3. Yulianita Rahayu,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 4. Raden Hadipiningsyah Kusumoseniarto.</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Koordinator: Didik Purwanto<br /></span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;">Anggota:</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 1. Kokoy Siti Komariah,</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;"> </span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"><span style="font-size: small;"> 2. Fatya Alty Amalia</span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
						<td valign="top" width="16"> </td>
						<td valign="top" width="332"> </td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Chungli</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Dianisa Khoirum S<br /></span></td>
						</tr>
						<tr>
						<td valign="top" width="251"><span style="font-size: small;">Tainan</span></td>
						<td valign="top" width="16"><span style="font-size: small;">:</span></td>
						<td valign="top" width="332"><span style="font-size: small;">Muhammad Naufal</span></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2017</span></p>
					<table border="0" cellpadding="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Robert James (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">L. Tri (Wijaya Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">M. Setya Widyawan<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Inda Karsunawati, Laela</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Nurvidia Laksmi dan Ervin</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Ilma, Iqbal, Siti, Yagus Cahyadi, Yudhis, Chusnul, Devina<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Retno Widyaningrum, Syahril M., Andromeda, Taufiqotul B., Primasari, Muniroh, Ali<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Didik P., Ayu, Farid Wajdi<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Chungli</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Rifqa<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;"> </span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Tainan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Wahid Dian B., Naufal</span></td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2016</span></p>
					<table border="0" cellpadding="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Arief Fadillah (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Pitut Pramuji (Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">M. Akamalul 'Ulya<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Inda Karsunawati</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Chandrawati, Nur Vidia Laksmi</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Intan Dzikria, Ilma, Iqbal, Siti <br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Retno Widyaningrum, Syahril M., Andromeda, Taufiqotul B., Eka<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Mario Leonardus Silalahi, Didik P., Ayu, Haris Chandra<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Chungli</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Yauwseph Tandiono<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;"> </span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Tainan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Wahid Dian B.</span></td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;"><br /><span style="font-size: 12.16px;"> </span>Susunan Badan Pelaksana UT Taiwan Periode 2015</span></p>
					<table border="0" cellpadding="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Arief Fadillah (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Iman Adipurnama (Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">M. Akamalul 'Ulya<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Shintami C. Hidayati</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span>Yeni Anistyasari, </span>Ni'matut Tamimah<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span>Rayi Yanu Tara</span>, Intan Dzikria, Anggi Setyo<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span>Dimas Herjuno, Sylvia Ayu P., Vuri Ayu S., M. Hendra Pebrianto, Muhammad Faisal, Mega Dewi Rimba</span><br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span>Mario Leonardus Silalahi, Resa Septiari<br /></span></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Chungli</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Welayaturromadhona, Yauwseph Tandiono<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taichung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Karmel Simatupang</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Tainan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Anggi L Wicaksana</span></td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2014</span></p>
					<table border="0" cellpadding="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Arief Fadillah (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Dedi Fazriansyah Putra (Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bagus Jati Santoso<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Shintami C. Hidayati</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Yeni Anistyasari, </span>Ni'matut Tamimah<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Rayi Yanu Tara</span>, Intan Dzikria, Anggi Setyo<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Dimas Herjuno, Sylvia Ayu P., Vuri Ayu S., M. Hendra Pebrianto</span><br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">M. Akmalul Ulya, <span style="color: #000000; line-height: normal;">Vania Utami, Mario Leonardus Silalahi<br /></span></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Chungli</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Welayaturromadhona, Yauwseph Tandiono<br /></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taichung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Immanuel Yosua<br /></span></td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2013</span></p>
					<table border="0" cellpadding="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Ahmad Syafri (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Rangga Aditya Elias (Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Aryka Pradhana P</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Shintami C. Hidayati</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Yeni Anistyasari, </span>Nieko Haryo P.</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Erliyah Nurul J., </span>Dhika Yonika P., Intan Dzikria</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;"><span style="color: #000000; line-height: normal;">Sylvia Ayu P., </span>Yuli Frita N., N.M. Malikul Adil</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bagus Jati Santoso, M. Akmalul Ulya, <span style="color: #000000; line-height: normal;">Choirul Nikmah</span></span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taoyuan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Febri Rahadi</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taichung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Erry Dwi K.</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Tainan</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Galih Nugroho</span></td>
					</tr>
					</tbody>
					</table>
					</td>
					<td> </td>
					</tr>
					</tbody>
					</table>
					<p> </p>
					<p><span style="font-size: small; font-weight: bold; line-height: 1.3em;">Susunan Badan Pelaksana UT Taiwan Periode 2012</span></p>
					<table border="0">
					<tbody>
					<tr>
					<td>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Ahmad Syafri (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Penanggung Jawab</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Ramzi Adriman (Ketua PPI)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Alief Wikarta</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Junaidillah Fadlil &amp; Shintami</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Hening Marlistya Citraningrum &amp; Wijanarko Sukma Pamungkas</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Wikan Jatimurti, Samsul Rajab &amp; Yeni A</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Fauzan Saiful Haq Mukarram, Aryka Pradhana Putra &amp; Sylvia Ayu Pradanawati</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bagus Jati Santoso, Adi Permadi &amp; Christian Gunawan</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taoyuan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bakhtiar Cahyandi Ridla &amp; Amna C Farhani</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taichung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Ikhyanuddin &amp; Septian Gandaputra</span></td>
					</tr>
					<tr>
					<td><span style="font-size: small;">Tainan</span></td>
					<td><span style="font-size: small;">:</span></td>
					<td><span style="font-size: small;">Fery Abdul Choliq &amp; Ilman Nafian Darajat</span></td>
					</tr>
					</tbody>
					</table>
					<span style="font-size: small;"> <br /><br /></span></td>
					<td> </td>
					</tr>
					</tbody>
					</table>
					<p> </p>
					<p><span style="font-weight: bold; line-height: 1.3em;"><span style="font-size: small;">Susunan Badan Pelaksana UT Taiwan Periode 2011</span></span></p>
					<table style="width: 622px;" border="0" cellspacing="0" cellpadding="0" align="left">
					<tbody>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pelindung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bpk. Harmen Sembiring (Kepala KDEI di Taipei)</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Koordinator</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Irwan Purnama</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Sekretaris</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Junaidillah Fadlil</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Keuangan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Hening Marlistya Citraningrum</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Akademik &amp; Tutorial</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Hakun Wira, Wikan Jatimurti, Firman</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Administrasi &amp; Kemahasiswaan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Tun Sriana, Fauzan Saiful Haq Mukarram, Amna C Farhani</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Bidang Humas &amp; Media</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Adi Permadi, Bagus Jati Santoso, Christian Gunawan</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Pengurus Wilayah</span></td>
					<td valign="top" width="16"> </td>
					<td valign="top" width="332"> </td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taoyuan</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Bakhtiar Cahyandi Ridla</span></td>
					</tr>
					<tr>
					<td valign="top" width="251"><span style="font-size: small;">Taichung</span></td>
					<td valign="top" width="16"><span style="font-size: small;">:</span></td>
					<td valign="top" width="332"><span style="font-size: small;">Ainul Mardiah</span></td>
					</tr>
					</tbody>
					</table> 

				</article>
				
			</div>
			<!--div class="col-lg-4">
				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 - 25 Desember 2017</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">5 Desember 2017 – 2 Januari 2018</span></font></p>
					</div>
				</div>
			</div-->
		</div>
	</div>
</section>
<br>
<br>