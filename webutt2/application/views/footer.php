		<footer id="colorlib-footer">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-8 colorlib-widget">
						<h4>Tentang UT </h4>
						<p>Universitas Terbuka (UT) merupakan Perguruan Tinggi Negeri ke-45 yang didirikan pada tanggal 4 September 1984. Berdasarkan katalog UT</p>
						<!--p>
							<ul class="colorlib-social-icons">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-dribbble"></i></a></li>
							</ul>
						</p-->
						<p><b>Alamat:</b><br/>
							Kantor Dagang dan Ekonomi Indonesia (KDEI) di Taipei
								<br> Rui Guang Road no.550, 6F Neihu District - Taipei Taiwan - R.O.C.</p>
					</div>

					<div class="col-md-4 colorlib-widget">
						<h4>Kontak Kami</h4>
						<ul class="colorlib-footer-links">
							<li>
							</li>
							<li><a href="tel://0986479028"><i class="icon-phone"></i> Taipei - Seni 0986479028</a></li>
							<li><a href="tel://0986481259"><i class="icon-phone"></i> Chungli - Alfian 0986481259</a></li>
							<li><a href="tel://0976254459"><i class="icon-phone"></i> Tainan - Naufal  0976254459</a></li>
							<li><a href="mailto:info@yoursite.com"><i class="icon-envelope"></i> pengurus.uttaiwan@gmail.com</a></li>
							<!--li><a href="http://ut-taiwan.org"><i class="icon-location4"></i> ut-taiwan.org </a></li-->
						</ul>
					</div>
				</div>
			</div>
			<!--div class="copy">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<p>
								<small class="block">&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<!--Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Humas UTT</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<!--/small><br> 
							</p>
						</div>
					</div>
				</div>
			</div-->
		</footer>