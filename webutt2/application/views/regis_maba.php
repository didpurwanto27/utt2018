<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb" style="background-color: black">
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
					<div class="post-image">
						<div class="post-heading">
							<h3><a href="#">Registrasi Mahasiswa Baru UT Taiwan</a></h3>
						</div>
					</div>
					<div class="alert alert-info">
						<b> REGISTRASI MAHASISWA BARU SEMESTER 2019.1 </b> <br />
						- REGISTRASI ONLINE Mahasiswa Baru dapat dilakukan <font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 - 1 Januari 2019</span></font> <br />
						- REGISTRASI ON THE SPOT (OFFLINE): <font color="brown"><span style="background-color: #FFFF00">23 dan 30 Desember 2018 pukul 10.00-16.00 </span></font><br />
						</div>
					
						
					<p><strong style="text-align: justify;">Alur Mahasiswa Baru: <br /></strong></p>
					<ol>
						<li>Calon Mahasiswa melakukan registrasi secara online melalui </strong><a class="btn btn-success" href="http://ut-taiwan.org/registrasi/" target="_blank"><strong>Klik Disini <br /></strong></a>
						
						<p>Isilah semua kolom yang ada di link pendaftaran tersebut. Kemudian setelah selesai mengisi semuanya, silahkan klik tombol "<strong>Daftar UT</strong>". Kemudian cek kembali isian form Anda, jika ada yang salah, silahkan edit kembali dengan klik tombol "<strong>Salah. Edit Ulang</strong>", jika sudah benar klik tombol "<strong>Benar, Lanjutkan</strong>"</p>
						<p>Kemudian ada sebuah pernyataan yang Anda perlu setujui:</p>
						<span style="background-color: #ebf4fb; color: #666666; font-family: 'Lucida Grande', 'Lucida Sans Unicode', Verdana, Arial, Helvetica, sans-serif; font-size: 11px;">"Dengan ini saya menyatakan bahwa data saya yang tertulis di bawah adalah benar dan saya siap memenuhi semua persyaratan pendaftaran mahasiswa baru UT."</span>
						<br />
						<span>Klik centang, lalu klik tombol "<strong>Klik untuk menyetujui</strong>".</span><br />
						Setelah itu Anda akan masuk ke halaman "<strong>Registrasi Sukses</strong>".<br /><br />
						<div class="alert alert-success">
						Hal yang perlu Anda lakukan saat ini adalah:<br />
						<ol>
							<li>Mencetak bukti pendaftaran dengan cara klik link "<strong>Cetak Bukti Pendaftaran</strong>", <br />
							</li>
							<br />
							<li>Membayar biaya pendaftaran dengan petunjuk yang ada pada halaman tersebut. Klik ini untuk petunjuk pembayaran <a class="btn btn-success" href="<?php echo base_url('index.php/pages/')?>/cara_bayar">Petunjuk Pembayaran</a><br />
							</li>
							<br />
							<li>Melakukan konfirmasi pembayaran <a class="btn btn-success" href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page">KONFIRMASI</a>
							</li>
							<br />
						</ol>
						</div>
						</p>
						
						</li> 
						<br />
						<li>Menyiapkan dan mengirimkan berkas ke alamat UT Taiwan dan email <span style="background-color: #ffff66">pengurusuttaiwan@gmail.com</span>
						<p style="text-align: justify;"><strong>Berkas yang perlu disiapkan Mahasiswa Baru: <br /></strong>
						a. Foto 2x3 2 lembar dan 4x6 1 lembar <br />
						b. Fotokopi Ijazah yang sudah dilegalisir 1 lembar <br />
						c. Form Tanda tangan 1 lembar <a class="btn btn-success" href="<?php echo base_url('public/')?>/images/maba1.pdf">Download file<br /></a><br />
						d. Form keabsahan data 1 lembar <a class="btn btn-success" href="<?php echo base_url('public/')?>/images/maba2.pdf">Download file<br /></a><br />
						e. Form data pribadi mahasiswa <a class="btn btn-success" href="<?php echo base_url('public/')?>/images/maba3.pdf">Download file<br /></a><br />
						F. Fotokopi KTP 1 lembar <br />
						</li>
						<br />
						<li>Mengirim berkas ke Bapel UT (Divisi Administrasi)
						<br /><img src="<?php echo base_url('public/')?>/images/alamat.jpg" border="0" alt="" width="600" height="342" />
						</li>
						<br />
						<li> Bagi mereka yang legalisir ijazahnya berada di Indonesia dapat mengirimkan langsung ke UPBJJ-UT Layanan Luar Negeri: 
						<br /><img src="<?php echo base_url('public/')?>/images/alamat_ut_lln.JPG" border="0" alt="" width="600" height="342" />
						</li>
						
						<br />
						
						
					</ol>
					
				</article>
				
			</div>
			<div class="col-lg-4">

				<h3>Deadline</h3>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_maba">Registrasi Mahasiswa Baru</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 - 1 Januari 2019</span></font></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="desc">
						<h4><a href="<?php echo base_url('index.php/pages/')?>/regis_mala">Registrasi Mahasiswa Lama</a></h4>
						<p><font color="brown"><span style="background-color: #FFFF00">3 Desember 2018 – 21 Januari 2019</span></font></p>
					</div>
				</div>
					
					<!-- <div class="widget">
						<h5 class="widgetheading">Kontak</h5>
						Fatya (Taipei) 0984218655<br />
						Nisa (Chungli) 0986481240<br />
						Naufal (Tainan) 097625445<br />
					</div> -->
			</div>
		</div>
	</div>
</section>
</br>
</br>