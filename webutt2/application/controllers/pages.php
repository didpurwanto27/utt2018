<?php

class Pages extends CI_Controller {

	function __construct(){
        parent::__construct();
	}
	
	function index(){	
		$data['currentPage'] = 'index';
		$data['currentMenu'] = 'home';
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('home', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function cara_bayar(){
		$data['currentPage'] = 'pembayaran';
		$data['currentMenu'] = 'cara_bayar';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('cara_bayar', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function konfirmasi_bayar(){
		$this->load->helper('url'); 
	    redirect('http://ut-taiwan.org/cp17/index.php/pages/konf_page');	
		// $this->load->view('css');
		// $this->load->view('menu');
		// // $this->load->view('konfirmasi_bayar');
		// $this->load->view('javascript');
	}

	/*function status_bayar(){		
		$data['currentPage'] = 'pembayaran';
		$data['currentMenu'] = 'status_bayar';
		$this->load->view('css');
		$this->load->view('menu', $data);
		// $this->load->view('status_bayar');
		$this->load->view('halaman_error', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}*/

	function biaya(){
		$data['currentPage'] = 'pembayaran';
		$data['currentMenu'] = 'biaya';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('biaya_pendidikan', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function regis_maba(){
		$data['currentPage'] = 'registrasi';
		$data['currentMenu'] = 'regis_maba';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('regis_maba', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function regis_mala(){	
		$data['currentPage'] = 'registrasi';
		$data['currentMenu'] = 'regis_mala';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('regis_mala', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function ujul(){
		$data['currentPage'] = 'registrasi';
		$data['currentMenu'] = 'ujul';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('ujul', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function lokasi(){	
		$data['currentPage'] = 'registrasi';
		$data['currentMenu'] = 'lokasi';
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('lokasi', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function program_studi(){
		$data['currentPage'] = 'akademik';
		$data['currentMenu'] = 'program_studi';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('program_studi', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function mekanisme_kuliah(){
		$data['currentPage'] = 'akademik';
		$data['currentMenu'] = 'mekanisme_kuliah';		
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('mekanisme_kuliah', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function link_akademik(){
		$data['currentPage'] = 'akademik';
		$data['currentMenu'] = 'link_akademik';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('link_akademik', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function umum(){	
		$data['currentPage'] = 'qna';
		$data['currentMenu'] = 'umum';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('umum', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function perkuliahan(){	
		$data['currentPage'] = 'qna';
		$data['currentMenu'] = 'perkuliahan';	
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('perkuliahan', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function sekilas(){	
		$data['currentPage'] = 'tentang';
		$data['currentMenu'] = 'sekilas';
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('sekilas', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function sejarah(){	
		$data['currentPage'] = 'tentang';
		$data['currentMenu'] = 'sejarah';
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('sejarah', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}

	function bapel(){	
		$data['currentPage'] = 'tentang';
		$data['currentMenu'] = 'bapel';
		$this->load->view('css');
		$this->load->view('menu', $data);
		$this->load->view('bapel', $data);
		$this->load->view('javascript');
	}

	function berita(){		
		$data['currentPage'] = 'berita';
		$data['currentMenu'] = 'berita';
		$this->load->view('css');
		$this->load->view('menu', $data);
		// $this->load->view('status_bayar');
		$this->load->view('halaman_error', $data);
		$this->load->view('footer');
		$this->load->view('javascript');
	}
	

		
	
	
	
}