<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	newRegistrationDateChecker();
	
	$id = $_GET['id'];
	$sql = "select * from Mahasiswa where id_mhs like '" . $id ."'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style2.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Mahasiswa Baru UT-Taiwan Online</title>
  </head>

<body>
<div id="stylized" class="myform">
<h1>Pilih Ujian Pengulangan Untuk Semester Depan</h1>
<p>Silahkan memilih ujian pengulangan di semester depan. Syarat yang harus diperhatikan :<br />
  1. Sudah pernah mengambil mata kuliah tersebut sebelumnya dan mendapat nilai <strong>D</strong> atau <strong>E</strong><br />
  2. Jadwal ujian pengulangan <strong>tidak boleh bersamaan dengan</strong> jadwal ujian mata kuliah yang diambil semester depan.<br />
  3. Biaya untuk ujian pengulangan adalah 350 NTD / sks
</p>
<p>Semester Depan (<?php echo cetakSemesterAktif(); ?>) anda akan mengambil paket mata kuliah sbb :<br />
<?php $jadwalarr = array();
      $semesterke = getSemesterKeByID($id);
	  $jurusan = getJurusanMhsByID($id);
	  $sql2 = "SELECT KODE_MK, NAMA_MK, SKS, JADWAL_UJIAN
	           FROM Mata_Kuliah Where SEMESTER = " . $semesterke . " AND PROGSTUDI_ID = " . $jurusan;
	  $r2 = mysql_query($sql2);
	  $nomor = 1;
	  while($row2 = mysql_fetch_array($r2))
	  {
         echo $nomor . ". " . $row2['KODE_MK'] ."/". $row2['NAMA_MK']."/". $row2['SKS']."/JADWAL:". $row2['JADWAL_UJIAN'] . "<br />";
	     $nomor++;
		 array_push($jadwalarr, $row2['JADWAL_UJIAN']); 
	  } ?>
</p>

<p>Pilih mata kuliah yang ingin anda ikuti ujian pengulangannya di semester depan (<strong>hanya jika anda mendapat D atau E</strong>) : <br />
<form name="2" action="submit2.php" method="post">
<?php
	$sql3 = "SELECT ID_MK, KODE_MK, NAMA_MK, SKS, JADWAL_UJIAN, SEMESTER
	           FROM Mata_Kuliah Where SEMESTER < " . $semesterke . " AND PROGSTUDI_ID = " . $jurusan . " ORDER BY ID_MK";
	$r3 = mysql_query($sql3);
	while($row3 = mysql_fetch_array($r3))
	{
?>
<label><input name="mk[]" type="checkbox" value="<?php echo $row3['ID_MK'];?>" <?php if(isMKTaken($id,$row3['ID_MK'],getSemesterAktif())==1){echo 'checked="checked"';}?> <?php if(in_array($row3['JADWAL_UJIAN'], $jadwalarr)){echo 'disabled="disabled"';}?>  />&nbsp;<?php echo $row3['KODE_MK']."/".$row3['NAMA_MK']."/SMSTR:".$row3['SEMESTER']."/SKS:".$row3['SKS']."/JADWAL:".$row3['JADWAL_UJIAN']."/BIAYA:".($row3['SKS']*350)."NT$";?><br /></label>
<?php } ?>
<input type="hidden" name="id" value="<?php echo $id;?>" />
<button type="submit">Ambil !</button>
</form>
</p>
<div class="spacer"></div>

</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
