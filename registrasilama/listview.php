<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
		<title>Daftar Calon Mahasiswa</title>
		<link rel="stylesheet" type="text/css" href="css/flexigrid.css">
		<script type="text/javascript" src="js/jquery-1.2.3.pack.js"></script>
		<script type="text/javascript" src="js/flexigrid.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#flex1").flexigrid
				(
				{
					url: 'list.php',
					dataType: 'json',
					colModel: [
						{display: 'ID', name: 'id', width: 40, sortable: true, align: 'center'},
						{display: 'Nama', name: 'nama', width: 120, sortable: true, align: 'left'},
						{display: 'Alamat', name: 'alamat', width: 290, sortable: true, align: 'left'},
						{display: 'No Telp', name: 'notelp', width: 80, sortable: true, align: 'left'},
						{display: 'No HP', name: 'nohp', width: 80, sortable: true, align: 'center'},
						{display: 'E-Mail', name: 'email', width: 150, sortable: true, align: 'center'},
						{display: 'Download', name: 'download', width: 80, sortable: true, align: 'center'}
						],
					searchitems: [
						{display: 'ID', name: 'id', isdefault: true},
						{display: 'Nama', name: 'nama'}
						],
					sortname: "time",
					sortorder: "desc",
					usepager: true,
					title: 'Daftar Calon Mahasiswa',
					useRp: true,
					rp: 20,
					showTableToggleBtn: false,
					width: 1000,
					height: 500,
					striped:true
				}
				);
			});
			
		</script>
	</head>
	<body>
		<table id="flex1" style="display:none"></table>
	</body>
</html>
