<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	newRegistrationDateChecker();
	
	$id = $_GET['id'];
	$sql = "select * from Mahasiswa where id_mhs like '" . $id ."'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Mahasiswa Baru UT-Taiwan Online</title>
   <script language="JavaScript" type="text/javascript">
    function jcap(form){

		 if (form.nama.value == "") {
			alert( "Harap masukkan nama anda." );
			form.nama.focus();
			return false ;
		  }
		  else if (form.nim.value == "") {
			alert( "Harap masukkan NIM anda." );
			form.nim.focus();
			return false ;
		  }
		  else if (form.alamat.value == "") {
			alert( "Harap masukkan alamat anda." );
			form.alamat.focus();
			return false ;
		  }
		  else if (form.distriktaiwan.value == "") {
			alert( "Harap masukkan distrik anda." );
			form.distriktaiwan.focus();
			return false ;
		  }
		  else if (form.kotataiwan.value == "") {
			alert( "Harap masukkan Kota Taiwan anda." );
			form.kotataiwan.focus();
			return false ;
		  }
		  else if (form.kodepostaiwan.value == "") {
			alert( "Harap masukkan kode pos Taiwan anda" );
			form.kodepostaiwan.focus();
			return false ;
		  }
		  else if (form.telepon.value == "") {
			alert( "Harap masukkan nomor telepon anda." );
			form.telepon.focus();
			return false ;
		  }
		  else if (form.handphone.value == "") {
			alert( "Harap masukkan nomor handphone anda." );
			form.handphone.focus();
			return false ;
		  }
		  else if (form.email.value == "") {
			alert( "Harap masukkan email anda." );
			form.email.focus();
			return false ;
		  }
		  else if (form.facebook.value == "") {
			alert( "Harap masukkan akun Facebook anda." );
			form.facebook.focus();
			return false ;
		  }
		  
		  return true;
  }
  </script>
</head>

<body>
<div id="stylized" class="myform">
<form name="1" action="submit.php" method="post" onsubmit="return jcap(this);">
<h1>Cek Data Formulir Daftar Ulang Universitas Terbuka Taiwan </h1>
<p>Cek dan isi formulir pendaftaran ini dengan data yang benar</p>

<label>Nama
<span class="small">Tulis nama anda</span>
</label>
<input type="text" name="nama" id="nama" value="<? echo $row['NAMA_MHS']?>" />

<label>NIM (Nomor Induk Mahasiswa)
<span class="small">Masukkan NIM di kartu mahasiswa anda</span>
</label>
<input type="text" name="nim" id="alamat"  maxlength="50" value="<? echo $row['NIM_MHS']?>" readonly="readonly" />

<label>Alamat
<span class="small">Tulis alamat pengiriman modul anda</span>
</label>
<input type="text" name="alamat" id="alamat"  maxlength="50" value="<? echo $row['ALAMAT_MHS']?>" />


<label>Distrik
<span class="small">Tulis distrik anda di taiwan</span>
</label>
<input type="text" name="distriktaiwan" id="distriktaiwan"  maxlength="50" value="<? echo $row['DISTRICT_MHS']?>" />

<label>Kota
<span class="small">Tulis kota tempat anda di taiwan</span>
</label>
<input type="text" name="kotataiwan" id="kotataiwan"  maxlength="50" value="<? echo $row['KABKOT_MHS']?>" />

<label>Kodepos
<span class="small">Tulis kodepos anda </span>
</label>
<input type="text" name="kodepostaiwan" id="kodepostaiwan" value="<? echo $row['KODEPOS_MHS']?>"/>

<label>No Telepon
<span class="small">Tulis telepon yang bisa dihubungi </span>
</label>
<input type="text" name="telepon" id="telepon" value="<? echo $row['TELP_MHS']?>"/>

<label>No Handphone
<span class="small">Tulis handphone yang bisa dihubungi </span>
</label>
<input type="text" name="handphone" id="handphone" value="<? echo $row['CELLPHONE_MHS']?>" />

<label>Email
<span class="small">Tulis alamat email anda dengan benar </span>
</label>
<input type="text" name="email" id="email" value="<? echo $row['EMAIL_MHS']?>" />

<label>Facebook
<span class="small">Tulis akun Facebook anda dengan benar </span>
</label>
<input type="text" name="facebook" id="facebook" value="<? echo $row['NAMAFB_MHS']?>" />

<label>Tempat Ujian
<span class="small">Pilih tempat ujian anda </span>
</label>
<select name="tempatujian" id="tempatujian">
    <?php
	$sql = "select PILIHAN_TEMPAT from Semester where ACTIVE_SEMESTER = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	$tempat = explode("/", $result);
	for($i=0; $i < count($tempat); $i++)
	{ ?>
    	<option value="<?php echo $tempat[$i];?>" <?php if($row['ID_TEMPATUJIAN']==$tempat[$i]){ echo 'selected="selected"';}?>><?php echo printKota($tempat[$i]);?></option>
    <?php
	}
	?>
</select>

<input type="hidden" name="submissiontype" value="edit" />
<input type="hidden" name="id" value="<? echo $id;?>" />
<button type="submit">Lanjut !</button>
<div class="spacer"></div>

</form>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
