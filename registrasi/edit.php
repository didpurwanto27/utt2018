<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	newRegistrationDateChecker();
	
	$id = $_GET['id'];
	$sql = "select * from Mahasiswa where id_mhs like '" . $id ."'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Mahasiswa Baru UT-Taiwan Online</title>
   <script language="JavaScript" type="text/javascript">
    function jcap(form){

		 if (form.nama.value == "") {
			alert( "Harap masukkan nama anda." );
			form.nama.focus();
			return false ;
		  }
		  else if (form.alamat.value == "") {
			alert( "Harap masukkan alamat anda." );
			form.alamat.focus();
			return false ;
		  }
		  else if (form.distriktaiwan.value == "") {
			alert( "Harap masukkan distrik anda." );
			form.distriktaiwan.focus();
			return false ;
		  }
		  else if (form.kotataiwan.value == "") {
			alert( "Harap masukkan Kota Taiwan anda." );
			form.kotataiwan.focus();
			return false ;
		  }
		  else if (form.kodepostaiwan.value == "") {
			alert( "Harap masukkan kode pos Taiwan anda" );
			form.kodepostaiwan.focus();
			return false ;
		  }
		  else if (form.telepon.value == "") {
			alert( "Harap masukkan nomor telepon anda." );
			form.telepon.focus();
			return false ;
		  }
		  else if (form.handphone.value == "") {
			alert( "Harap masukkan nomor handphone anda." );
			form.handphone.focus();
			return false ;
		  }
		  else if (form.email.value == "") {
			alert( "Harap masukkan email anda." );
			form.email.focus();
			return false ;
		  }
		  else if (form.facebook.value == "") {
			alert( "Harap masukkan akun Facebook anda." );
			form.facebook.focus();
			return false ;
		  }
		  else if (form.tempatlahir.value == "") {
			alert( "Harap masukkan tempatlahir anda." );
			form.tempatlahir.focus();
			return false ;
		  }
		  else if (form.tahunijazah.value == "") {
			alert( "Harap masukkan tahun ijazah anda." );
			form.tahunijazah.focus();
			return false ;
		  }
		  else if (form.namaibu.value == "") {
			alert( "Harap masukkan nama Ibu anda." );
			form.namaibu.focus();
			return false ;
		  }
		  /*else if (form.norekening.value == "") {
			alert( "Harap masukkan nomor rekening anda." );
			form.norekening.focus();
			return false ;
		  }
		  else if (form.namabank.value == "") {
			alert( "Harap masukkan nama bank anda." );
			form.namabank.focus();
			return false ;
		  }*/
		  return true;
  }
  </script>
</head>

<body>
<div id="stylized" class="myform">
<form name="1" action="submit.php" method="post" onsubmit="return jcap(this);">
<h1>Edit Formulir Pendaftaran Universitas Terbuka Taiwan <br/> Angkatan <? echo getTotalAngkatan();?> (<? echo cetakSemesterAktif();?>)</h1>
<p>Isilah formulir pendaftaran ini dengan data yang benar</p>

<label>Nama
<span class="small">Tulis nama anda</span>
</label>
<input type="text" name="nama" id="nama" value="<? echo $row['NAMA_MHS']?>" />

<label>Alamat
<span class="small">Tulis alamat pengiriman modul (buku) anda</span>
</label>
<input type="text" name="alamat" id="alamat"  maxlength="100" value="<? echo $row['ALAMAT_MHS']?>" />


<label>Distrik
<span class="small">Tulis distrik anda di taiwan</span>
</label>
<input type="text" name="distriktaiwan" id="distriktaiwan"  maxlength="50" value="<? echo $row['DISTRICT_MHS']?>" />

<label>Kota
<span class="small">Tulis kota tempat anda di taiwan</span>
</label>
<input type="text" name="kotataiwan" id="kotataiwan"  maxlength="50" value="<? echo $row['KABKOT_MHS']?>" />

<label>Kodepos
<span class="small">Tulis kodepos anda </span>
</label>
<input type="text" name="kodepostaiwan" id="kodepostaiwan" value="<? echo $row['KODEPOS_MHS']?>"/>

<label>No Telepon
<span class="small">Tulis telepon yang bisa dihubungi </span>
</label>
<input type="text" name="telepon" id="telepon" value="<? echo $row['TELP_MHS']?>"/>

<label>No Handphone
<span class="small">Tulis handphone yang bisa dihubungi </span>
</label>
<input type="text" name="handphone" id="handphone" value="<? echo $row['CELLPHONE_MHS']?>" />

<label>Email
<span class="small">Tulis alamat email anda dengan benar </span>
</label>
<input type="text" name="email" id="email" value="<? echo $row['EMAIL_MHS']?>" />

<label>Facebook
<span class="small">Tulis akun Facebook anda dengan benar </span>
</label>
<input type="text" name="facebook" id="facebook" value="<? echo $row['NAMAFB_MHS']?>" />

<label>Tanggal Lahir
<span class="small">Pilih tanggal lahir anda</span>
</label>
<span class="panjang">
<? 
	$tgllahir = explode("-",$row['TGL_LHR_MHS']);
	$tanggallahir = $tgllahir[2];
	$bulanlahir = $tgllahir[1];
	$tahunlahir = $tgllahir[0];
	
?>
<select name="tanggallahir" id="tanggallahir">
		  <? for ($i = 1; $i <= 31; $i++)
          {
		  	if($i < 10){$i = '0'.$i;}?>
            	<option value="<? echo $i;?>" <? if($tanggallahir==$i) {echo 'selected="selected"';}?>> <? echo $i;?></option>
            <?
          }
          ?>
          </select>
<select name="bulanlahir" id="bulanlahir">
        <option value="1" <? if($bulanlahir=='01') {echo 'selected="selected"';}?>>Januari</option>
        <option value="2" <? if($bulanlahir=='02') {echo 'selected="selected"';}?>>Februari</option>
        <option value="3" <? if($bulanlahir=='03') {echo 'selected="selected"';}?>>Maret</option>
        <option value="4" <? if($bulanlahir=='04') {echo 'selected="selected"';}?>>April</option>
        <option value="5" <? if($bulanlahir=='05') {echo 'selected="selected"';}?>>Mei</option>
        <option value="6" <? if($bulanlahir=='06') {echo 'selected="selected"';}?>>Juni</option>
        <option value="7" <? if($bulanlahir=='07') {echo 'selected="selected"';}?>>Juli</option>
        <option value="8" <? if($bulanlahir=='08') {echo 'selected="selected"';}?>>Agustus</option>
        <option value="9" <? if($bulanlahir=='09') {echo 'selected="selected"';}?>>September</option>
        <option value="10" <? if($bulanlahir=='10') {echo 'selected="selected"';}?>>Oktober</option>
        <option value="11" <? if($bulanlahir=='11') {echo 'selected="selected"';}?>>Nopember</option>
        <option value="12" <? if($bulanlahir=='12') {echo 'selected="selected"';}?>>Desember</option>
</select>
<select name="tahunlahir" id="tahunlahir">
<?php for($i=1955; $i<= 2000; $i++){ ?>
        <option <? if($tahunlahir==$i) {echo 'selected="selected"';}?> value="<? echo $i;?>" ><? echo $i;?></option>
        <?
	  }?>
</select>
</span>

<label>Tempat Lahir
<span class="small">Tulis kota tempat anda lahir </span>
</label>
<input type="text" name="tempatlahir" id="tempatlahir" value="<? echo $row['TMP_LHR_MHS'];?>" />

<label>Agama
<span class="small">Pilih agama yang anda anut</span>
</label>
<span class="panjang">
<select name="agama" id="agama">
        <option <? if($row['AGAMA_MHS']=='Islam') {echo 'selected="selected"';}?> value="Islam">Islam</option>
        <option <? if($row['AGAMA_MHS']=='Katolik') {echo 'selected="selected"';}?> value="Katolik">Katolik</option>
        <option <? if($row['AGAMA_MHS']=='Protestan') {echo 'selected="selected"';}?> value="Protestan">Protestan</option>
        <option <? if($row['AGAMA_MHS']=='Hindu') {echo 'selected="selected"';}?> value="Hindu">Hindu</option>
        <option <? if($row['AGAMA_MHS']=='Budha') {echo 'selected="selected"';}?> value="Budha">Budha</option>
</select>
</span>

<label>Program Studi
<span class="small">Pilih program studi yang anda minati</span>
</label>
<span class="panjang">
<select name="programstudi" id="programstudi"> 
      <option <? if($row['PROGSTUDI_ID']=='54') {echo 'selected="selected"';}?> value="54">Manajemen</option> 
      <option <? if($row['PROGSTUDI_ID']=='72') {echo 'selected="selected"';}?> value="72">Ilmu Komunikasi</option> 
      <option <? if($row['PROGSTUDI_ID']=='87') {echo 'selected="selected"';}?> value="87">Sastra Inggris (Penerjemahan)</option>
</select>
</span>

<label>Kode UPBJJ
<span class="small">Kode UPBJJ anda</span>
</label>
<input name="upbjj" type="text" id="upbjj" readonly="readonly" value="010 (Layanan Luar Negeri)" />

<label>Jenis Kelamin
<span class="small">Jenis Kelamin</span>
</label>
<span class="panjang">
<select name="jeniskelamin" id="jeniskelamin">
        <option <? if($row['JK_MHS']=='Pria') {echo 'selected="selected"';}?> value="Pria">Pria</option>
        <option <? if($row['JK_MHS']=='Wanita') {echo 'selected="selected"';}?> value="Wanita">Wanita</option>
</select>
</span>

<label>Kewarganegaraan
<span class="small">Pilih kewarganegaraan anda</span>
</label>
<span class="panjang">
<select name="kewarganegaraan" id="kewarganegaraan">
        <option <? if($row['WN_MHS']=='WNI') {echo 'selected="selected"';}?> value="WNI">WNI</option>
        <option <? if($row['WN_MHS']=='Asing') {echo 'selected="selected"';}?> value="Asing">Asing</option>
</select>
</span>

<label>Pekerjaan
<span class="small">Pilih pekerjaan anda</span>
</label>
<span class="panjang">
<select name="pekerjaan" id="pekerjaan">
        <option <? if($row['PEKERJAAN_MHS']=='Swasta / TKI') {echo 'selected="selected"';}?> value="Swasta / TKI">Swasta / TKI</option>
        <option <? if($row['PEKERJAAN_MHS']=='TNI/Polri') {echo 'selected="selected"';}?> value="TNI/Polri">TNI/Polri</option>
        <option <? if($row['PEKERJAAN_MHS']=='Wiraswasta') {echo 'selected="selected"';}?> value="Wiraswasta">Wiraswasta</option>
        <option <? if($row['PEKERJAAN_MHS']=='Tidak Bekerja') {echo 'selected="selected"';}?> value="Tidak Bekerja">Tidak Bekerja</option>
        <option <? if($row['PEKERJAAN_MHS']=='PNS') {echo 'selected="selected"';}?> value="PNS">PNS</option>
</select>
</span>

<label>Status Pernikahan
<span class="small">Pilih status pernikahan anda</span>
</label>
<span class="panjang">
<select name="pernikahan" id="pernikahan">
      <option <? if($row['STKWN_MHS']=='Menikah') {echo 'selected="selected"';}?> value="Menikah">Menikah</option>
      <option <? if($row['STKWN_MHS']=='Tidak Menikah') {echo 'selected="selected"';}?> value="Tidak Menikah">Tidak Menikah</option>
</select>
</span>

<label>Tempat Ujian
<span class="small">Pilih kota tempat anda mengikuti ujian nantinya</span>
</label>
<span class="panjang">
<select name="tempatujian" id="tempatujian">
    <?php
	$sql = "select PILIHAN_TEMPAT from Semester where ACTIVE_SEMESTER = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	$tempat = explode("/", $result);
	for($i=0; $i < count($tempat); $i++)
	{ ?>
    	<option value="<?php echo $tempat[$i];?>" <?php if($row['ID_TEMPATUJIAN']==$tempat[$i]){ echo 'selected="selected"';}?>><?php echo printKota($tempat[$i]);?></option>
    <?php
	}
	?>
</select>
</span>

<div class="spacer"></div>
<H2>Pendidikan Terakhir</H2>
<p>Isi dengan informasi pendidikan terkhir anda</p>

<label>Jenjang
<span class="small">Pilih jenjang pendidikan terkhir anda</span>
</label>
<span class="panjang">
<select name="jenjang" id="jenjang">
        <option <? if($row['JENJANGPDK_MHS']=='SLTA') {echo 'selected="selected"';}?> value="SLTA" >SLTA</option>
        <option <? if($row['JENJANGPDK_MHS']=='D-I') {echo 'selected="selected"';}?> value="D-I">D-I</option>
        <option <? if($row['JENJANGPDK_MHS']=='D-II') {echo 'selected="selected"';}?> value="D-II">D-II</option>
        <option <? if($row['JENJANGPDK_MHS']=='D-III') {echo 'selected="selected"';}?> value="D-III">D-III</option>
</select>
</span>

<label>Jurusan
<span class="small">Pilih jurusan pendidikan terkhir anda</span>
</label>
<span class="panjang">
<select name="jurusan" id="jurusan">
        <option <? if($row['JURUSAN_MHS']=='SMTA Umum IPA / IPS') {echo 'selected="selected"';}?> value="SMTA Umum IPA / IPS">SMTA Umum IPA / IPS</option>
        <option <? if($row['JURUSAN_MHS']=='STM/SMTA') {echo 'selected="selected"';}?> value="STM/SMTA">STM/SMTA</option>
        <option <? if($row['JURUSAN_MHS']=='SMK/SMKA') {echo 'selected="selected"';}?> value="SMK/SMKA">SMK/SMKA</option>
        <option <? if($row['JURUSAN_MHS']=='SMEA') {echo 'selected="selected"';}?> value="SMEA">SMEA</option>
        <option <? if($row['JURUSAN_MHS']=='SPMA') {echo 'selected="selected"';}?> value="SPMA">SPMA</option>
        <option <? if($row['JURUSAN_MHS']=='SPG/SPGLB/SGO/SMOA') {echo 'selected="selected"';}?> value="SPG/SPGLB/SGO/SMOA">SPG/SPGLB/SGO/SMOA</option>
</select>
</span>

<label>Tahun Ijazah
<span class="small">Tulis tahun ijazah pendidikan terakhir</span>
</label>
<input name="tahunijazah" type="text" id="tahunijazah" value="<? echo $row['THNIJAZAH_MHS']?>" size="4" maxlength="4" />

<label>Nama Ibu
<span class="small">Tulis nama ibu anda</span>
</label>
<input type="text" value="<? echo $row['NAMAIBU_MHS'];?>" name="namaibu" id="namaibu" />

<label>No. Rekening
<span class="small">Tulis no. rekening bank anda di Taiwan</span>
</label>
<input type="text" name="norekening" value="<? echo $row['NOREKENING_MHS']?>" id="norekening" />

<label>Nama Bank
<span class="small">Tulis nama bank anda di Taiwan</span>
</label>
<input type="hidden" name="submissiontype" value="edit" />
<input type="hidden" name="id" value="<? echo $id;?>" />
<input type="text" name="namabank" value="<? echo $row['BANK_MHS'];?>" id="namabank" />
<button type="submit">Edit</button>
<div class="spacer"></div>

</form>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
