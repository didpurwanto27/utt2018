<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	newRegistrationDateChecker();
	$id = $_GET['id'];
	$sql = "select * from Mahasiswa where id_mhs like '" . $id . "'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cek Ulang Data Pendaftaran</title>
    
    <SCRIPT LANGUAGE="Javascript">
<!---
function decision(message, url){
if(confirm(message)) location.href = url;
}
// --->
</SCRIPT>
</head>

<body>
<div id="stylized" class="myform">
<h1>Cek Ulang Data Registrasi Universitas Terbuka Taiwan -  
<!--Angkatan <? echo getTotalAngkatan();?> -->
(<? echo cetakSemesterAktif();?>)</h1>
<p>Silahkan baca kembali isian data anda berikut dan cek apakah telah diisi dengan benar.<br/>
<strong>Setelah dicek, tekan tombol konfirmasi di bawah</strong></p>

                          
<p><label>Nama</label>&nbsp;:&nbsp;&nbsp;<? echo $row['NAMA_MHS'];?></p>
<p><label>Alamat Pengiriman Modul</label>&nbsp;:&nbsp;&nbsp;<? echo $row['ALAMAT_MHS'];?></p>
<p><label>Distrik</label>&nbsp;:&nbsp;&nbsp;<? echo $row['DISTRICT_MHS'];?></p>
<p><label>Kota</label>&nbsp;:&nbsp;&nbsp;<? echo $row['KABKOT_MHS'];?></p>
<p><label>Kode Pos</label>&nbsp;:&nbsp;&nbsp;<? echo $row['KODEPOS_MHS'];?></p>
<p><label>No Telp</label>&nbsp;:&nbsp;&nbsp;<? echo $row['TELP_MHS'];?></p>
<p><label>No Handphone</label>&nbsp;:&nbsp;&nbsp;<? echo $row['CELLPHONE_MHS'];?></p>
<p><label>Email</label>&nbsp;:&nbsp;&nbsp;<? echo $row['EMAIL_MHS'];?></p>
<p><label>Facebook</label>&nbsp;:&nbsp;&nbsp;<? echo $row['NAMAFB_MHS'];?></p>
<p><label>Tanggal Lahir</label>&nbsp;:&nbsp;&nbsp;<? $datetime = strtotime($row['TGL_LHR_MHS']);
		                                $mysqldate = date("j F Y", $datetime);
                                        echo $mysqldate;?></p>
<p><label>Tempat Lahir</label>&nbsp;:&nbsp;&nbsp;<? echo $row['TMP_LHR_MHS'];?></p>
<p><label>Agama</label>&nbsp;:&nbsp;&nbsp;<? echo $row['AGAMA_MHS'];?></p>
<p><label>Program Studi</label>&nbsp;:&nbsp;&nbsp;<? if ($row['PROGSTUDI_ID']=='54'){echo 'Manajemen';}
										else if ($row['PROGSTUDI_ID']=='72'){echo 'Ilmu Komunikasi';}
										else if ($row['PROGSTUDI_ID']=='87'){echo 'Sastra Inggris (Penerjemahan)';}?></p>

<p><label>Kode UPBJJ</label>&nbsp;:&nbsp;&nbsp;010 (Layanan Luar Negeri)</p>
<p><label>Jenis Kelamin</label>&nbsp;:&nbsp;&nbsp;<? echo $row['JK_MHS'];?></p>
<p><label>Kewarganegaraan</label>&nbsp;:&nbsp;&nbsp;<? echo $row['WN_MHS'];?></p>
<p><label>Pekerjaan</label>&nbsp;:&nbsp;&nbsp;<? echo $row['PEKERJAAN_MHS'];?></p>
<p><label>Status Pernikahan</label>&nbsp;:&nbsp;&nbsp;<? echo $row['STKWN_MHS'];?></p>
<p><label>Tempat Ujian</label>&nbsp;:&nbsp;&nbsp;<? echo printKota($row['ID_TEMPATUJIAN']);?></p>


<div class="spacer"></div>
<H2>Pendidikan Terakhir</H2>
<p>Isi dengan informasi pendidikan terkhir anda</p>

<p><label>Jenjang</label>&nbsp;:&nbsp;&nbsp;<? echo $row['JENJANGPDK_MHS'];?></p>
<p><label>Jurusan</label>&nbsp;:&nbsp;&nbsp;<? echo $row['JURUSAN_MHS'];?></p>
<p><label>Tahun Ijazah</label>&nbsp;:&nbsp;&nbsp;<? echo $row['THNIJAZAH_MHS'];?></p>
<p><label>Nama Ibu</label>&nbsp;:&nbsp;&nbsp;<? echo $row['NAMAIBU_MHS'];?></p>
<p><label>No. Rekening</label>&nbsp;:&nbsp;&nbsp;<? echo $row['NOREKENING_MHS'];?></p>
<p><label>Nama Bank</label>&nbsp;:&nbsp;&nbsp;<? echo $row['BANK_MHS'];?></p>

<div class="spacer"></div>
<H2>Anda yakin semua data telah terisi dengan benar?</H2>
<br/><button
TYPE="button" VALUE="" NAME="button1"
onClick="location.href='http://ut-taiwan.org/registrasi/edit.php?id=<? echo $id;?>'">Salah. Edit Ulang.</button>&nbsp;&nbsp;
  <br/>&nbsp;<br/><button
type="button" value="Benar. Lanjutkan." name="button2"
onclick="decision('Anda yakin semua data telah terisi dengan benar?',
'http://ut-taiwan.org/registrasi/recheck2.php?id=<? echo $id;?>')" />Benar. Lanjutkan.</button>
         
</div>




<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2011<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
