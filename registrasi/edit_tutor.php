<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
	//newRegistrationDateChecker();
	
	$id = $_GET['id'];
	$sql = "select * from Tutor where ID_TUTOR like '".$id."'";
	$r = mysql_query($sql);
	$row = mysql_fetch_array($r);
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script src='https://www.google.com/recaptcha/api.js'></script>
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran Tutor</title>
	<script language="JavaScript" type="text/javascript">
    function jcap(form){

		 var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		 if(grecaptcha.getResponse() == '') {
             alert( "Mohon periksa Captcha Anda" );
			 return false ;
         }
		 if (form.nama.value == "" || form.nama.value == " " || form.nama.value == "-") {
			alert( "Harap masukkan nama anda." );
			form.nama.focus();
			return false ;
		  }
		  else if (form.alamat.value == "" || form.alamat.value == " " || form.alamat.value == "-") {
			alert( "Harap masukkan alamat anda." );
			form.alamat.focus();
			return false ;
		  }
		  else if (form.distriktaiwan.value == "" || form.distriktaiwan.value == " " || form.distriktaiwan.value == "-") {
			alert( "Harap masukkan Distrik Kota Taiwan anda." );
			form.distriktaiwan.focus();
			return false ;
		  }
		  else if (form.kotataiwan.value == "" || form.kotataiwan.value == " " || form.kotataiwan.value == "-") {
			alert( "Harap masukkan Kota Taiwan anda." );
			form.kotataiwan.focus();
			return false ;
		  }
		  else if (form.kodepostaiwan.value == "" || form.kodepostaiwan.value == " " || form.kodepostaiwan.value == "-") {
			alert( "Harap masukkan kode pos Taiwan anda" );
			form.kodepostaiwan.focus();
			return false ;
		  }
		  else if (form.handphone.value == "" || form.handphone.value == " " || form.handphone.value == "-") {
			alert( "Harap masukkan nomor handphone anda." );
			form.handphone.focus();
			return false ;
		  }
		  else if (!filter.test(form.email.value)) {
			alert( "Harap cek email anda." );
			form.email.focus();
			return false ;
		  }
		  else if (form.facebook.value == "" || form.facebook.value == " " || form.facebook.value == "-") {
			alert( "Harap masukkan facebook anda." );
			form.facebook.focus();
			return false ;
		  }
		  else if (form.norekening.value == "") {
			alert( "Harap masukkan nomor rekening anda." );
			form.norekening.focus();
			return false ;
		  }
		  else if (form.riwayat_hidup.value=="") {
			alert( "Harap upload daftar riwayat hidup anda." );
			form.riwayat_hidup.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i.test(form.riwayat_hidup.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png, doc, docx, pdf');
			form.riwayat_hidup.focus();
			return false;
		  }
		  else if (form.ijazah.value=="") {
			alert( "Harap upload ijazah anda." );
			form.ijazah.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i.test(form.ijazah.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png, doc, docx, pdf');
			form.ijazah.focus();
			return false;
		  }
		  else if (form.transkipS1.value=="") {
			alert( "Harap upload transkip S1 anda." );
			form.transkipS1.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i.test(form.transkipS1.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png, doc, docx, pdf');
			form.transkipS1.focus();
			return false;
		  }
		  else if (form.transkipS2.value=="") {
			alert( "Harap upload transkip S2 anda." );
			form.transkipS2.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i.test(form.transkipS2.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png, doc, docx, pdf');
			form.transkipS2.focus();
			return false;
		  }
		  else if (form.skm.value=="") {
			alert( "Harap upload surat kesediaan mengajar anda." );
			form.skm.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png|\.pdf|\.doc|\.docx)$/i.test(form.skm.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png, doc, docx, pdf');
			form.skm.focus();
			return false;
		  }
		  else if (form.foto.value=="") {
			alert( "Harap upload foto anda." );
			form.foto.focus();
			return false;
		  }
		  else if(!/(\.jpg|\.jpeg|\.png)$/i.test(form.foto.value)) {
			alert('Format File salah, format yang diijinkan adalah jpg, jpeg, png');
			form.foto.focus();
			return false;
		  }
		  return true;
  }
  </script>
</head>

<body>
<div id="stylized" class="myform">
<form name="1" action="submit_tutor.php" method="post" onsubmit="return jcap(this);" enctype="multipart/form-data">
<h1>Edit Formulir Pendaftaran Tutor - Universitas Terbuka Taiwan</h1>
<p>Isilah formulir pendaftaran ini dengan data yang benar</p>

<label>Nama
<span class="small">Tulis nama lengkap anda</span>
</label>
<input type="text" name="nama" id="nama" value="<? echo $row['NM_TUTOR']?>" />

<label>Alamat di Taiwan
<span class="small">Tujuan pengiriman bahan ajar</span>
</label>
<input type="text" name="alamat" id="alamat"  maxlength="50" value="<? echo $row['ALAMAT_TUTOR']?>" />


<label>Distrik
<span class="small">Tulis distrik anda di taiwan</span>
</label>
<input type="text" name="distriktaiwan" id="distriktaiwan"  maxlength="50" value="<? echo $row['DISTRIK_TUTOR']?>" />

<label>Kota
<span class="small">Tulis kota tempat anda di taiwan</span>
</label>
<input type="text" name="kotataiwan" id="kotataiwan"  maxlength="50" value="<? echo $row['KOTA_TUTOR']?>" />

<label>Kodepos
<span class="small">Tulis kodepos anda </span>
</label>
<input type="text" name="kodepostaiwan" id="kodepostaiwan" value="<? echo $row['KDPOS_TUTOR']?>"/>

<label>No Handphone
<span class="small">Tulis handphone yang bisa dihubungi </span>
</label>
<input type="text" name="handphone" id="handphone" value="<? echo $row['HP_TUTOR']?>" />

<label>Email
<span class="small">Tulis alamat email anda dengan benar </span>
</label>
<input type="text" name="email" id="email" value="<? echo $row['EMAIL_TUTOR']?>" />

<label>Facebook
<span class="small">Tulis akun Facebook anda dengan benar </span>
</label>
<input type="text" name="facebook" id="facebook" value="<? echo $row['FB_TUTOR']?>" />

<label>No. Rekening (Indonesia)
<span class="small">a.n., nomor rekening, nama bank, cabang</span>
</label>
<input type="text" name="norekening" value="<? echo $row['REK_TUTOR']?>" id="norekening" />

<label>NPWP
<span class="small">Nomor Pokok Wajib Pajak</span>
</label>
<input type="text" name="npwp" value="<? echo $row['NPWP_TUTOR']?>" id="npwp" />

<label>Pangkat
<span class="small">Jika PNS</span>
</label>
<input type="text" name="pangkat" value="<? echo $row['PANGKAT_TUTOR']?>" id="pangkat" />

<label>Daftar Riwayat Hidup*
<span class="small">jpg,jpeg,png,doc,docx,pdf</span>
</label>
<input type="file" name="riwayat_hidup" id="riwayat_hidup" />

<label>Ijazah*
<span class="small">jpg,jpeg,png,doc,docx,pdf</span>
</label>
<input type="file" name="ijazah" id="ijazah" />

<label>Transkip Nilai S1*
<span class="small">jpg,jpeg,png,doc,docx,pdf</span>
</label>
<input type="file" name="transkipS1" id="transkipS1" />

<label>Transkip Nilai S2*
<span class="small">jpg,jpeg,png,doc,docx,pdf</span>
</label>
<input type="file" name="transkipS2" id="transkipS2" />

<label>Surat Kesediaan Mengajar*
<span class="small">jpg,jpeg,png,doc,docx,pdf</span>
</label>
<input type="file" name="skm" id="skm" />

<label>Foto*
<span class="small">jpg,jpeg,png</span>
</label>
<input type="file" name="foto" id="foto" />

<label>MK 1
<span class="small">Pilihan pertama Mata Kuliah</span>
</label>
<span class="panjang">
<select name="mk1" id="mk1">
	<option <? if($row['ID_MK1']=='IK2') {echo 'selected="selected"';}?> value="IK2">Konsultasi 1***</option>;
	<option <? if($row['ID_MK1']=='IK7') {echo 'selected="selected"';}?> value="IK7">Konsultasi 2***</option>;
	<?php
		$matkul = getMatKul();
		while($rowmatkul = mysql_fetch_array($matkul, MYSQL_ASSOC)){
	?>
			<option <? if($row['ID_MK1']==$rowmatkul['ID_MK']) {echo 'selected="selected"';}?>value="<?php echo $rowmatkul['ID_MK'] ?>"><?php echo $rowmatkul['NAMA_MK'] ?></option>;
	<?php	
		}
	?>
</select>
</span>

<label>MK 2
<span class="small">Pilihan kedua Mata Kuliah</span>
</label>
<span class="panjang">
<select name="mk2" id="mk2">
	<option <? if($row['ID_MK2']=='IK2') {echo 'selected="selected"';}?> value="IK2">Konsultasi 1***</option>;
	<option <? if($row['ID_MK2']=='IK7') {echo 'selected="selected"';}?> value="IK7">Konsultasi 2***</option>;
	<?php
		$matkul = getMatKul();
		while($rowmatkul = mysql_fetch_array($matkul, MYSQL_ASSOC)){
	?>
		<option <? if($row['ID_MK2']==$rowmatkul['ID_MK']) {echo 'selected="selected"';}?>value="<?php echo $rowmatkul['ID_MK'] ?>"><?php echo $rowmatkul['NAMA_MK'] ?></option>;
	<?php	
		}
	?>
</select>
</span>

<span class="panjang">	
<div align="right" class="g-recaptcha" data-sitekey="6LceTBYTAAAAABta52F13WYhy0ANbzL2jPYDyTGI"></div>
</span>

<input type="hidden" name="submissiontype" value="edit_tutor" />
<input type="hidden" name="id" value="<? echo $id;?>" />
<button type="submit">Edit</button>
<div class="spacer"></div>

</form>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2013<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
