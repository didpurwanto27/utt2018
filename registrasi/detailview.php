<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="style1.css" rel="stylesheet" type="text/css" />
	<title>Informasi Lengkap Biodata</title>
</head>
<body>
	<div class="myform">
		<div id="stylized">
			<h1>Informasi Calon Mahasiswa Universitas Terbuka di Taiwan</h1>
			<p>Data-data lengkap formulir registrasi mahasiswa UT Taiwan</p>
			<label>ID</label>
			<span><?php echo $result['id'] ?></span>
			
			<label>Nama</label>
			<span><?php echo $result['nama'] ?></span>
			
			<label>Alamat</label>
			<span><?php echo $result['alamat'] ?></span>
			
			<label>Kota</label>
			<span>99009</span>
			
			<label>Kode Pos</label>
			<span><?php echo $result['kodepos'] ?></span>
			
			<label>No Telepon</label>
			<span><?php echo $result['notelp'] ?></span>
			
			<label>No HP</label>
			<span><?php echo $result['nohp'] ?></span>
			
			<label>Email</label>
			<span><?php echo $result['email'] ?></span>
			
			<label>Tanggal Lahir</label>
			<span><?php echo $result['tgllahir'] ?></span>
			
			<label>Tempat Lahir</label>
			<span><?php echo $result['tmplahir'] ?></span>
			
			<label>Agama</label>
			<span><?php echo $result['agama'] ?></span>
			
			<label>Program Studi</label>
			<span><?php echo $result['kdprodi'] ?></span>
			
			<label>UPBJJ</label>
			<span><?php echo $result['kdupbjj'] ?></span>
			
			<label>Jenis Kelamin</label>
			<span><?php echo $result['jk'] ?></span>
			
			<label>Kewarganegaraan</label>
			<span><?php echo $result['warga'] ?></span>
			
			<label>Pekerjaan</label>
			<span><?php echo $result['pekerjaan'] ?></span>
			
			<label>Pernikahan</label>
			<span><?php echo $result['kawin'] ?></span>
			
			<label>Jenjang Pendidikan</label>		
			<span><?php echo $result['jenjang'] ?></span>
			
			<label>Jurusan</label>
			<span><?php echo $result['jurusan'] ?></span>
			
			<label>Tahun Ijazah</label>
			<span><?php echo $result['thnijazah'] ?></span>
			
			<label>Nama Ibu</label>
			<span><?php echo $result['namaibu'] ?></span>
			
			<label>Nama Bank</label>
			<span><?php echo $result['bank'] ?></span>		
		</div>
	</div>
</body>