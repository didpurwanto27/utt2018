<?php
require_once 'PHPExcel/PHPExcel.php';
require_once 'PHPExcel/PHPExcel/Writer/Excel2007.php';
include "config.php";
include "open_connection.php";
include "function.php";


$curday = date('j');
$curmonth = date('n');
$curyear = date('Y');
$cur_month = array (
				'1' => 'JANUARI', '2' => 'FEBRUARI', '3' => 'MARET', '4' => 'APRIL', '5' => 'MEI', '6' => 'JUNI',
				'7' => 'JULI', '8' => 'AGUSTUS', '9' => 'SEPTEMBER', '10' => 'OKTOBER', '11' => 'NOVEMBER', '12' => 'DESEMBER'
			);

$curdate = "   Taiwan, ".$curday." ".$cur_month[$curmonth]." ".$curyear;

$objPHPExcel = new PHPExcel();

//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
$objPHPExcel->getProperties()->setLastModifiedBy("Junaidillah Fadlil");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

if (isset($_GET['semester'])){
	$tutor = getTutorBySemester($_GET['semester']);
}else{
	$tutor = getTutor();
}
					
// Style
	$style1 = array (
						'font' => array ('bold' => true, 'size' => 14),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'numberformat' => array('code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT),
					);
	
	$style2 = array (
						'font' => array ('bold' => false, 'size' => 12),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'numberformat' => array('code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT),
					);
	
	$style3 = array (
						'font' => array ('bold' => false, 'size' => 12),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
						'borders' => array (
												'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
											),
						'numberformat' => array('code' => PHPExcel_Style_NumberFormat::FORMAT_TEXT),
					);
	
	$style4 = array (
						'font' => array ('bold' => false, 'size' => 12),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
						'numberformat' => array('code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER),
						'borders' => array (
												'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
											),
					);
					
	$style5 = array (
						'font' => array ('bold' => false, 'size' => 12, 'color' => array('rgb' => '3366BB')),
						'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
						'borders' => array (
												'allborders' => array ('style' => PHPExcel_Style_Border::BORDER_THIN) 
											),
					);
					
//Set Column Size
foreach(range('B','Q') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(10);
$objPHPExcel->getActiveSheet()->getRowDimension('Q')->setRowHeight(100);
$rowindex=2;
$objPHPExcel->getActiveSheet()->mergeCells('B'.$rowindex.':Q'.$rowindex);
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowindex, 'DAFTAR TUTOR');
$objPHPExcel->getActiveSheet()->getStyle('B'.$rowindex)->applyFromArray($style1);
$rowindex=$rowindex+1;
$objPHPExcel->getActiveSheet()->mergeCells('B'.$rowindex.':Q'.$rowindex);
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowindex, $curdate);
$objPHPExcel->getActiveSheet()->getStyle('B'.$rowindex)->applyFromArray($style2);
$rowindex=$rowindex+1;
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowindex, 'ID');
	$objPHPExcel->getActiveSheet()->getStyle('B'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowindex, 'NAMA');
	$objPHPExcel->getActiveSheet()->getStyle('C'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowindex, 'ALAMAT');
	$objPHPExcel->getActiveSheet()->getStyle('D'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowindex, 'HP');
	$objPHPExcel->getActiveSheet()->getStyle('E'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowindex, 'E-MAIL');
	$objPHPExcel->getActiveSheet()->getStyle('F'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowindex, 'FACEBOOK');
	$objPHPExcel->getActiveSheet()->getStyle('G'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowindex, 'REKENING');
	$objPHPExcel->getActiveSheet()->getStyle('H'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowindex, 'NPWP');	
	$objPHPExcel->getActiveSheet()->getStyle('I'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowindex, 'PANGKAT');
	$objPHPExcel->getActiveSheet()->getStyle('J'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowindex, 'MATKUL 1');
	$objPHPExcel->getActiveSheet()->getStyle('K'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowindex, 'MATKUL 2');
	$objPHPExcel->getActiveSheet()->getStyle('L'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowindex, 'RIWAYAT HIDUP');
	$objPHPExcel->getActiveSheet()->getStyle('M'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowindex, 'IJAZAH');
	$objPHPExcel->getActiveSheet()->getStyle('N'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowindex, 'TRANSKIP');
	$objPHPExcel->getActiveSheet()->getStyle('O'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowindex, 'SKM');
	$objPHPExcel->getActiveSheet()->getStyle('P'.$rowindex)->applyFromArray($style3);
	$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowindex, 'FOTO');
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$rowindex)->applyFromArray($style3);
while($rowtutor = mysql_fetch_array($tutor, MYSQL_ASSOC))
{	
	$rowindex=$rowindex+1;
	if($rowtutor['ID_MK1']=='IK2') $MK1="Tutor Konsultasi 2***"; else if($rowtutor['ID_MK1']=='IK7') $MK1="Tutor Konsultasi 7***"; else $MK1=getNamaMatKul($rowtutor['ID_MK1']);
	if($rowtutor['ID_MK2']=='IK2') $MK2="Tutor Konsultasi 2***"; else if($rowtutor['ID_MK2']=='IK7') $MK2="Tutor Konsultasi 7***"; else $MK2=getNamaMatKul($rowtutor['ID_MK2']);
	$allalamat = $rowtutor['ALAMAT_TUTOR']." ".$rowtutor['DISTRIK_TUTOR']." ".$rowtutor['KOTA_TUTOR']." ".$rowtutor['KDPOS_TUTOR'];
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowindex, $rowtutor['ID_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowindex, $rowtutor['NM_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('C'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowindex, $allalamat);
	$objPHPExcel->getActiveSheet()->getStyle('D'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowindex, $rowtutor['HP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('E'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowindex, $rowtutor['EMAIL_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('F'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowindex, $rowtutor['FB_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('G'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowindex, $rowtutor['REK_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('H'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowindex, $rowtutor['NPWP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('I'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowindex, $rowtutor['PANGKAT_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('J'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowindex, $MK1);
	$objPHPExcel->getActiveSheet()->getStyle('K'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowindex, $MK2);
	$objPHPExcel->getActiveSheet()->getStyle('L'.$rowindex)->applyFromArray($style4);
	$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowindex, 'http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['RIWAYAT_HIDUP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getCell('M'.$rowindex)->getHyperlink()->setUrl('http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['RIWAYAT_HIDUP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('M'.$rowindex)->applyFromArray($style5);
	$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowindex, 'http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['IJAZAH_TUTOR']);
	$objPHPExcel->getActiveSheet()->getCell('N'.$rowindex)->getHyperlink()->setUrl('http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['IJAZAH_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('N'.$rowindex)->applyFromArray($style5);
	$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowindex, 'http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['TRANSKIP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getCell('O'.$rowindex)->getHyperlink()->setUrl('http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['TRANSKIP_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('O'.$rowindex)->applyFromArray($style5);
	$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowindex, 'http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['SKM_TUTOR']);
	$objPHPExcel->getActiveSheet()->getCell('P'.$rowindex)->getHyperlink()->setUrl('http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['SKM_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('P'.$rowindex)->applyFromArray($style5);
	$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowindex, 'http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['FOTO_TUTOR']);
	$objPHPExcel->getActiveSheet()->getCell('Q'.$rowindex)->getHyperlink()->setUrl('http://www.ut-taiwan.org/registrasi/file_tutor/'.$rowtutor['FOTO_TUTOR']);
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$rowindex)->applyFromArray($style5);
}

// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

ob_end_clean();
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="daftar  tutor.xlsx"');

$objWriter->save('php://output');
exit();
//$objPHPExcel->disconnectWorksheets();
//unset($objPHPExcel);


?> 