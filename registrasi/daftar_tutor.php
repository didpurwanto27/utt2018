<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Daftar Tutor</title>
</head>

<body>
<div id="stylized" class="myform">
	<div>
	<form action="daftar_tutor.php" method="post">
		Semester Daftar : 
			<select id="semester_daftar" name="semester_daftar">
				<?php
					$angkatan = getAngkatan();
					while ($row = mysql_fetch_array($angkatan, MYSQL_ASSOC)) {
				?>
				<option value="<?php echo $row['SEMESTER_DAFTAR']?>" <?php if (isset($_POST['semester_daftar']) && $_POST['semester_daftar'] == $row['SEMESTER_DAFTAR']) echo 'selected' ?>><?php echo $row['SEMESTER_DAFTAR']?></option>
				<?php } ?>
			</select>
			<button type="submit">Lihat Data</button>
		</form>
	</div>
	<div>
		<table id="background-image" width="100%">
			<thead>
				<tr>
					<th scope="col">Nama</th>
					<th scope="col">Alamat</th>
					<th scope="col">HP</th>
					<th scope="col">FB</th>
					<th scope="col">Detail</th>
				</tr>
			</thead>
			<tbody>
				<br />
				<?php
					if (isset($_POST['semester_daftar'])){
						$tutor = getTutorBySemester($_POST['semester_daftar']);
					}else{
						$tutor = getTutor();
					}
					while($rowtutor = mysql_fetch_array($tutor, MYSQL_ASSOC)){
				?>
				<tr>
				  <td><?php print $rowtutor['NM_TUTOR'] ?></td>
				  <td><?php print $rowtutor['ALAMAT_TUTOR'] ?></td>
				  <td><?php print $rowtutor['HP_TUTOR'] ?></td>
				  <td><?php print $rowtutor['FB_TUTOR'] ?></td>
				  <td><a href="detail_tutor.php?id=<?php echo $rowtutor['ID_TUTOR'] ?>">Detail</a></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="7">
						<a href="excel.php?semester=<?php echo $_POST['semester_daftar'] ?>"><strong>Download</strong></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2013<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>
</body>
</html>
