<?php
function generate_number() {
	$character = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
	$number = "123456789";
	$random_string_length = 4;
	
	$random_string = "";
	
	for ($i = 0; $i < $random_string_length; $i++) {
		$random_string .= $character[mt_rand (0, strlen($character)-1)];
	}
	
	for ($i = 0; $i < $random_string_length; $i++) {
		$random_string .= $number[mt_rand (0, strlen($number)-1)];
	}

	return $random_string;
}

function isValidEmail($email) {
  if(preg_match("/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $email) > 0)
  	return true;
  else
  	return false;
}

function getUk()
{
	$sql = "select nominal_uk_maba from Semester where active_semester=1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getUo()
{
	$sql = "select nominal_uo_maba from Semester where active_semester=1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}


function konversi($tanggal)
{ 
    $format = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu',
        'Jan' => 'Januari',
        'Feb' => 'Februari',
        'Mar' => 'Maret',
        'Apr' => 'April',
        'May' => 'Mei',
        'Jun' => 'Juni',
        'Jul' => 'Juli',
        'Aug' => 'Agustus',
        'Sep' => 'September',
        'Oct' => 'Oktober',
        'Nov' => 'November',
        'Dec' => 'Desember'
    );
 
    return strtr($tanggal, $format);
}

function getTotalAngkatan()
{
	$sql = "select count(id_semester) from Semester";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function cetakSemesterAktif()
{
	$sql = "select id_semester from Semester where active_semester = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	//$result = substr($result, 0, 4) . "/" . substr($result, 4, 4) . "." . $result[8];
	$result = substr($result, 0, 4) . "/" .  $result[4];
	return $result;
}

function getSemesterAktif()
{
	$sql = "select id_semester from Semester where active_semester = 1";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function printKota($i)
{
	$sql = "select TEMPATUJIAN from Tempat_Ujian where ID_TEMPATUJIAN = " . $i;
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function newRegistrationDateChecker()
{
	$sql = "select new_reg_begin, new_reg_end from Semester where Active_Semester = 1";
	$r = mysql_query($sql);
	$begin = mysql_result($r, 0, 0);
	$end = mysql_result($r, 0, 1);
	
	if((date('Y-m-d')>=$begin) && (date('Y-m-d')<=$end)){}
	else
	{
		$pass = 'Location: http://ut-taiwan.org/registrasi/closed.php';
        header( $pass ) ;
	}
}

function getAngkatan()
{
	$sql = "SELECT DISTINCT(SEMESTER_DAFTAR) FROM Tutor";
	$result = mysql_query($sql);
	return $result;
}

function getTempUj()
{
	$sql = "select * from Tempat_Ujian";
	$result = mysql_query($sql);
	return $result;
}


function getMatKul()
{
	//$sql = "select ID_MK,NAMA_MK from Mata_Kuliah where ((PROGSTUDI_ID=54 && SEMESTER<=6) || (PROGSTUDI_ID=72 && SEMESTER<=5) || (PROGSTUDI_ID=87 && SEMESTER<=5)) && TTM='T' ORDER by NAMA_MK ASC";
	$sql = "select ID_MK,NAMA_MK from Mata_Kuliah where 
	(ID_MK='140' || ID_MK='129' || ID_MK='116' || ID_MK='104' || ID_MK='25' || ID_MK='26' || ID_MK='32' || ID_MK='33' || ID_MK='5')  ORDER by PROGSTUDI_ID, NAMA_MK ASC";
	
	$result = mysql_query($sql);
	return $result;
}

function getNamaMatKul($id_mk)
{
	$sql = "select NAMA_MK from Mata_Kuliah where ID_MK='".$id_mk."'";
	$r = mysql_query($sql);
	$result = mysql_result($r, 0, 0);
	return $result;
}

function getTutor()
{
	$sql = "SELECT * FROM Tutor ORDER by NM_TUTOR ASC";
	$result = mysql_query($sql);
	return $result;
}

function getTutorBySemester($semester)
{
	$sql = "SELECT * FROM Tutor where SEMESTER_DAFTAR='".$semester."' ORDER by NM_TUTOR ASC";
	$result = mysql_query($sql);
	return $result;
}

function delete_image($image){
	unlink('file_tutor/'.$image);
}

?>