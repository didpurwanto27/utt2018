<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftran mahasiswa online</title>
	<style>
		body {
			margin-top: 60px;
		}
		#body {
			font-family: Verdana;
			font-size: 16px;
			text-align: center;
		}
		a {
			color: #0066FF;
			text-decoration: none;
		}
		
		a:hover {
			color: #AAAAAA;
		}
		
		span {
			color: red;
			font-weight: bold;
		}
		
		#footer {
			margin: 0 auto;
			padding-top: 5px;
			width:900px;
			color: #a5a5a5;
		}
		
		#footer .boundary {
			height: 1px;
			display: block;
			width: 900px;
			border-bottom: solid 1px #aacfe4;
			margin-bottom: 5px;
		}
		
		#footer img{
			float: right;
		}
		
	</style>
</head>

<body>
	<div id="body">
		<p>Pendaftaran Universitas Terbuka Saat Ini Belum Dibuka !<span></span></p>
	  <p>Informasi tentang pendaftaran dapat dilihat pada website kami: <a href="http://ut-taiwan.org">www.ut-taiwan.org</a> atau facebook "universitas terbuka taiwan" </p>
	</div>
	<div>&nbsp;</div>
	<div id="footer">
		<span class="boundary"></span>
		<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
		Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
		&copy; 2011<br />
		website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
	</div>
</body>
