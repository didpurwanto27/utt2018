<?php
	include "config.php";
	include "open_connection.php";
	include "function.php";
	newRegistrationDateChecker();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<LINK href="style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Pendaftaran mahasiswa online</title>
    <style type="text/css">
		.style1 {
			font-size: 36px;
			font-weight: bold;
		}
		.style2 {
			font-size: 3em
		}
    </style>
</head>

<?php $id = $_GET['id'];?>


<body>
  <div id="stylized" class="myform">
  <H1>KODE REGISTRASI ANDA:
  <strong><span style="background-color: #FFFF00"><? echo $id;?></span></strong></H1>
  <h2><font color="red">Kode ini digunakan untuk pembayaran. Mohon dicatat dan jangan sampai lupa. </font><br></h2>
  <!--/p>Cetak <a href="form.php?id=<? echo $id;?>">bukti pendaftaran </a><br/-->
  <hr />
  
  <h4">Selamat. Anda tinggal selangkah lagi menjadi mahasiswa Universitas-Terbuka Taiwan.<br/>
  Agar Anda resmi terdaftar, silahkan ikuti langkah berikut: </h4>

  <br/>
  <br/>
  <strong>
  
  1. Mengirimkan berkas pendaftaran yaitu:  Foto 2x3 2 lembar dan 4x6 2 lembar, Fotokopi Ijazah yang sudah dilegalisir 1 lembar , Form Tanda tangan 1 lembar, Form keabsahan data 1 lembar , Form data pribadi mahasiswa, Fotokopi KTP 1 lembar ke alamat berikut:
  <img src="http://ut-taiwan.org/webutt2/public/images/alamat.jpg" width="600" height="287" />
  <br />
  Bagi mereka yang legalisir ijazahnya berada di Indonesia dapat mengirimkan langsung ke UPBJJ-UT Layanan Luar Negeri :
  <br /><img src="http://ut-taiwan.org/webutt2/public/images/alamat_ut_lln.JPG" border="0" alt="" width="600" height="342" />


  <br/><br/>
  2. Membayar uang kuliah dan uang operasional dengan cara transfer ke:</strong><br /><br />
  <u>Rekening Taiwan:</u>
  <table>
    <tr>
      <td> Bank </td>
      <td> <b>: Hua Nan Bank  </b>
    <tr>              
    <tr>
      <td> Atas nama </td>
      <td> <b>: Ariana Tulus Purnomo  </b>
    <tr>
    
    <tr>
      <td> Bank Code </td>
      <td> <b>: 008</b>
    <tr>
    <tr>
      <td> Swift Code </td>
      <td> <b>: HNBKTWTP</b>
    <tr>
    <tr>
      <td> Kantor Cabang </td>
      <td> <b>: Taita (National Taiwan University) </b>
    <tr>              
    <tr>
      <td> No. Rekening </td>
      <td> <b>: 154-20-047668-7 </b>
    <tr>
    <tr>
      <td> No Telp. </td>
      <td> <b>:  +886-974-086-541 </b>
    <tr>              
    </table>
  <br />
  <u>Rekening Indonesia:</u>
      <table>
      <tr>
        <td> Bank </td>
        <td> <b>: Bank BNI  </b>
      <tr>
      <tr>
        <td> Atas Nama </td>
        <td> <b>: Hana Mutialif Maulidiah </b>
      <tr>
      
      <tr>
        <td> Kantor Cabang </td>
        <td> <b>: Tanjung Perak</b>
      <tr>
      <tr>
        <td> No. Rekening </td>
        <td> <b>: 0296233345 </b>
      <tr>
      <tr>
        <td> No. Telp </td>
        <td> <b>: +886-973-453-283 </b>
      <tr>
      </table>

  3. Lakukan konfirmasi pembayaran di <a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page">link berikut</a>.
  <br /><br />
  <hr />
  PETUNJUK: <br />
  - <a href="http://ut-taiwan.org/webutt2/index.php/pages/biaya" class="btn btn-success">Biaya Pendidikan </a><br />
  - <a href="http://ut-taiwan.org/webutt2/index.php/pages/cara_bayar" class="btn btn-success">Cara Pembayaran </a><br />
  - <a href="http://ut-taiwan.org/cp17/index.php/pembayaran/konf_page" class="btn btn-success">Konfirmasi Pembayaran </a><br />
  <hr />

</body>
</html>