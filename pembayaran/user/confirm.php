<!DOCTYPE HTML>
<?
	include "config.php";
	include "open_connection.php";
	include "function.php";
?>
<html> 
<head>
	<title>Konfirmasi Pembayaran</title>  
	<meta charset="utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="shortcut icon" href="favicon.ico" >
	
	
	<link rel="stylesheet" href="css/jquery.mobile.datepicker.css" /> 
	<link rel="stylesheet" href="css/jquery.mobile.structure-1.3.1.min.css" />
	<link rel="stylesheet"  href="css/jquery.mobile-git.css" /> 
	<link rel="stylesheet" href="css/pietervanos.css" />
	<link rel="stylesheet" href="css/pietervanos.min.css" />

	
	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery.ui.datepicker.js"></script>
	<script src="js/jquery.mobile-git.js"></script> 
	<script src="js/jquery.mobile.datepicker.js"></script>
	
	<script type="text/javascript">
		//google analytics//
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-6021558-12']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

		$.datepicker.setDefaults({
			dateFormat: 'yy-mm-dd'
		});
	</script>
</head>
<body>
<?
$nim=$_POST['nim'];
$tgllahir=$_POST['tgllahir'];
$arr_aktif=get_nominal_bayar_aktif();
$aktif=$arr_aktif[0];
$uo_lm=$arr_aktif[3];
$uk_lm=$arr_aktif[4];
$uo_br=$arr_aktif[1];
$uk_br=$arr_aktif[2];

if($nim==""){
?>

<!---	<div id="home" data-role="page" class="type-interior" data-theme="a"> -->
	<div data-role="header">
			<h1>Konfirmasi Pembayaran</h1>
		<!--	<a data-icon="home" data-rel="back" style="margin-top:2px;">Home</a> -->
		</div>
		
		<br/>
		INFO PEMBAYARAN SEMESTER <?=$aktif?><br/>
		MAHASISWA BARU<br/>
		UK	: <?=$uk_br?><br/>
		UO	: <?=$uo_br?><br/>
		<br/>
		MAHASISWA LAMA<br/>
		UK	: <?=$uk_lm?><br/>
		UO	: <?=$uo_lm?><br/>
		<br/>
		
		<label>NIM
		<!--<span class="small">Tulis NIM anda dengan benar </span>-->
		</label>
		<form method="post" action="confirm.php">
		<input type="text" name="nim" id="nim"  placeholder="NIM/Nomor Registrasi" required />
		 Tanggal Lahir:<input type="text" id="tgllahir" name="tgllahir" data-inline="true" data-role="date" required>
		<div data-role="content" data-theme="a">
				<input type="submit" data-theme="a" data-transition="slide" value="Konfirmasi">
			<br />
		</div>
		
		
		
		</form>
		
<!---		</div> -->
<?
}else{
?>
<!--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<!---	<div id="isi" data-role="page" data-theme="a"> -->
	
	<? $mhs_id=get_MahasiswaID($nim);
		if($mhs_id=="" || $mhs_id==null){ ?>
		<script>
			alert("NIM atau Nomor Registrasi tidak ada dalam daftar mahasiswa");
			location.reload();
		</script>
		
	<?	
		}
		$tglcek=get_Mahasiswatgllahir($nim);
		if($tgllahir!=$tglcek){ ?>
		<script>
			alert("Tanggal lahir anda salah");
			location.reload();
		</script>
		
	<?	
		}
		
		
		$id_pemb=get_IDpembayaran($mhs_id);
		if($id_pemb!="" && $id_pemb!=null){ ?>
		<script>
			alert("Maaf status pembayaran Anda terakhir belum dikonfirmasi oleh bendahara, Silahkan hubungi Bendahara UT Taiwan");
			window.location.assign('index.php?nx=<?=$nim?>');
		</script>
		
	<?	
		// header("Location: index.php?success=$nim");
		}
	?>	
	
		<div data-role="header">
			<h1>Konfirmasi Pembayaran</h1>
		<!--	<a data-icon="home" data-rel="back" style="margin-top:2px;">Home</a> -->
		</div>
		
		<br/>
		INFO PEMBAYARAN SEMESTER <?=$aktif?><br/>
		MAHASISWA BARU<br/>
		UK	: <?=$uk_br?><br/>
		UO	: <?=$uo_br?><br/>
		<br/>
		MAHASISWA LAMA<br/>
		UK	: <?=$uk_lm?><br/>
		UO	: <?=$uo_lm?><br/>
		<br/>
		
		<div data-role="content" data-theme="a">
		<?
		//$nim="<script>document.getElementById('nim').value;</script>";
		//echo $nim;
		$nama=get_namaMahasiswa($nim);
		$result=generate_semester($nim);
		$id_pemb=get_IDpembayaran($mhs_id);
		
		?>
		<form method="post" action="save.php" onsubmit="return Validate(this);"  enctype="multipart/form-data" data-ajax="false">
		  <b>NIM : <?=$nim?> <br/>
		  Nama:<?=$nama?> <br/> </b>
		  <input type=hidden name="mhs_id" id="mhs_id" value="<?=$mhs_id?>">
		  <input type=hidden name="nim" id="nim" value="<?=$nim?>">
		  Semester:
		  <select id="semester" name="semester"  required>
			<option value="">Select Semester</option>
			<?
			for ($i = 0; $i < count($result); $i++) {
			echo '<option value="'.$result[$i].' " >'.$result[$i].'</option>';
			}
			?>
		 </select>

		Status Kekurangan UK: <input type="text" id="uktxt" name="uktxt" readonly /><br>
		Status Kekurangan UO: <input type="text" id="uotxt" name="uotxt" readonly /><br>
		Total Bayar: <input type="text" id="byrtxt" name="byrtxt" onkeypress="return OnlyNumbers(event);" onkeyup="calculate(this.value)" onchange="calculate(this.value)" required /><br>
		 
		
		  
		  Tanggal:<input type="text" id="tglbyr" name="tglbyr" data-inline="true" data-role="date" required>
		  
		  Atas Nama: <input type="text" id="nama" name="nama" value="" placeholder="Nama Pentransfer" required/><br>
		  No. Hp: <input type="text" id="hp" name="hp" value="" placeholder="Nomor HP"  onkeypress="return OnlyNumbers(event);" required/><br>
		  
		  
		  File upload: 
		  
		  <input name="attachment" type="file" id="attachment" required/><br>
		  <input type="submit" value="Send" name="submit" class="btn">
		 </form>
		</div>
		<div data-role="footer">
		<div class="center-image">	
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka"><br />
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2015<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
		</div>	
		</div>
		
		<script>
		var sisa_uo;
		var sisa_uk;
		var _validFileExtensions = [".jpg", ".jpeg", ".png"];    
		function Validate(oForm) {
		var arrInputs = oForm.getElementsByTagName("input");
		for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
				}
			}
		}
  
		return true;
		}
		
		
		
		
		
		
		function OnlyNumbers(e)
		{
			var unicode=e.charCode? e.charCode : e.keyCode
			if (unicode!=8)
			{ //if the key isn't the backspace key (which we should allow)
				if (unicode<48||unicode>57) //if not a number
				return false //disable key press
			}
			
		}
		
		$.datepicker.setDefaults({
			dateFormat: 'yy-mm-dd'
		});
		
		$( "#semester" ).change(function() {
		var smt = $( this ).val();
		var nim ="<?=$nim?>";
			//alert(nim);
			var dataString = 'smt='+smt;
			$.ajax
			(
				{
					type:"POST",
					url:"smt_ajaran.php",
					data:{smt:smt,nim:nim},
					cache:false,
					success:function(html)
					{
						//$(".semester").html(html);
						//alert(html);
						var res=html.split(":");
						var tot_uo=parseInt(res[0]);
						if (isNaN(tot_uo)) tot_uo = 0;
						var tot_uk=parseInt(res[1]);
						if (isNaN(tot_uk)) tot_uk = 0;
						var nom_uo=parseInt(res[2]);
						var nom_uk=parseInt(res[3]);
						sisa_uo=tot_uo-nom_uo;
						sisa_uk=tot_uk-nom_uk;
						$('#uktxt').val(sisa_uk);
						$('#uotxt').val(sisa_uo);
						if(sisa_uo<0 || sisa_uk<0){
						//alert("kurang");
							document.getElementById("byrtxt").disabled = false; 
							
						}else{
							document.getElementById("byrtxt").disabled = true; 
						}
						//alert(nom_uk);
						
						
						
						
					}
				}
			); 
			
		});
		
		
		
		function calculate(nilai) {
		if (isNaN(nilai)) nilai = 0;
		nilai=parseInt(nilai);
		var uk2;
		var uo2;
		var total = nilai+sisa_uk;
		if(total>0){
			 uo2	=total+sisa_uo;
			 uk2=0;
		}else{
			 uk2=total;
			 uo2=sisa_uo;
		}
			if (isNaN(uk2)) uk2 = sisa_uk;
			$('#uktxt').val(uk2);
			$('#uotxt').val(uo2);
		}
		
		
		</script>
		
		
		
		
		
		
		
		
	<?
	}
?>	
	
</body>
</html>
