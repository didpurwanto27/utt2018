<?php
	include "config.php";
	include "open_connection.php";
    include "function.php";

	$nim2='#';
	$error = 0;
	$sucess=$_GET['success'];
	$nx=$_GET['nx'];
	if($sucess<>""){
	?>
			<script>
			alert("Penyimpanan Data Sukses, Silahkan menunggu dikonfirmasi");
			
		</script>	
	<?}else{
		$sucess=$nx;
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Status Pembayaran Mahasiswa UT-Taiwan Online</title>
		<script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/jquery.jeditable.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.editable.js" type="text/javascript"></script>
		<script src="js/jquery.alerts.js" type="text/javascript"></script>
			<script src="js/jquery.ui.datepicker.js"></script>
	<script src="js/jquery.mobile-git.js"></script> 
	<script src="js/jquery.mobile.datepicker.js"></script>
		
		<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/jquery.mobile.datepicker.css" /> 

		<LINK href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css" title="currentStyle">
			@import "css/demo_page.css";
			@import "css/demo_table.css";
			@import "css/jquery-ui-1.10.4.custom.min.css";
		</style>
	<script type="text/javascript" charset="utf-8">
			var oTable;
			$(document).ready( function () {
			
				 oTable = $('#example').dataTable(
					{
						"bProcessing": true,
						"bAutoWidth": false,
						"bLengthChange": false,
						"bServerSide": true,
						"sAjaxSource": "data_pembayaran.php",
						"sPaginationType": "full_numbers",
						"bFilter": false,"bInfo": false,
						"aoColumns": [{ "bVisible":    false , sName:"id_pembayaran"},
							/* NIM    { "bVisible":    false , sName:"nim"},*/
							/* Nama */   { sName:"nama"},
							/* semester */     { sName:"semester"},
							/* UO */  { sName:"uo"},
							/* Confirm UO */  { sName:"confirm_uo"},
							/* UK */   { sName:"uk"},
							/* Confirm UK */  { sName:"confirm_uk"},
							/* Jas */     { sName:"jas"},
							/* Confirm Jas */  { sName:"confirm_jkt"},
							/* Uang_Ujian */     { sName:"ujian"},
							/* Confirm US */  { sName:"us_confirm"},
							/* Total */     { sName:"total"},
							/* Konfirmasi Bukti */  { sName:"conf_status"},	
							/* Konfirmasi Bukti */  { sName:"conf_status"},								
							/* Mahasiswa_ID */  { "bVisible":    false , sName:"mahasiswa_id"}
						],
						"fnServerParams": function ( aoData ) {
						  aoData.push( { "name": "nim", "value": $( "#nim"  ).val() } );
						   aoData.push( { "name": "semester", "value": $( "#semester"  ).val() } );
						}
						
					});
				
					});
			
					
		</script>
			
	
	</head>

	<body>
	
	<div id="stylized" class="myform" style="width:900px">
	<h1><a href="index.php">Home</a> | <a href="http://ut-taiwan.org/pembayaran/user/confirm.php" target="_blank">Konfirmasi Pembayaran</a>
	</p>


		<h1>Cek Status Pembayaran Mahasiswa UT Taiwan - Online</h1>
		<p>	Silahkan memasukkan nim  atau ID pendaftaran mahasiswa baru dan pilih semester yang ingin dilihat status pembayarannya<br />
			Isilah formulir daftar ulang ini dengan data yang benar
		</p>
		<label>NIM atau ID Mahasiswa Baru
		<span class="small">Tulis NIM atau ID Mahasiswa Baru Anda dengan benar </span>
		</label>

		<input type="text" name="nim" id="nim" class="nim"  value="<?=$sucess?>"/>

		<?php if($error == 1) {?>
			<label><span class="style1">Maaf data anda tidak ditemukan atau salah!</span><span class="small">Jika anda mahasiswa lama dan sudah mendaftar ulang via online, silahkan kontak :<br />
			Mario BP-UT (+886984218655) atau email ke pengurus.uttaiwan@gmail.com</span>
			</label>
		<?php }?>
		<BR/>
		 <label>Tanggal Lahir
		 <span class="small">Isi tanggal lahir Anda </span>
		</label>
		<center> <input type="text" id="tgl" name="tgl" data-role="date" required> </center>
	<!--	<label>Semester
		<span class="small">Pilih Semester yang ingin dilihat </span>
		</label>
		<div></div>
		<select id="semester" name="semester" class="semester" >
			<option value="">Select Semester</option>
			
		
		</select> --->
		<input type="hidden" name="submissiontype" value="input" />
		
		<button id="but" class="but" type="submit">LIHAT</button>
		<div class="spacer"></div>

	
	<br/><br/>
	<div class="spacer"><br/></div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
				<thead>
				<tr>
					<th>id_pembayaran</th>
					<th>NIM</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>UO</th>
					<th>Sisa UO</th>
					<th>UK</th>
					<th>Sisa UK</th>
					<th>Jas</th>
					<th>Sisa Jas</th>
					<th>Uang Ujian</th>
					<th>Sisa UU</th>
					<th>Total</th>
					<th>Status </th>
					<th>Status Dummy</th>
					
				</tr>
			</thead>
				<tfoot>
					<tr>
					<th>id_pembayaran</th>
					<th>NIM</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>UO</th>
					<th>Sisa UO</th>
					<th>UK</th>
					<th>Sisa UK</th>
					<th>Jas</th>
					<th>Sisa Jas</th>
					<th>Uang Ujian</th>
					<th>Sisa UU</th>
					<th>Total</th>
					<th>Status </th>
					<th>Status Dummy</th>
					
				</tr>
				</tfoot>
				<tbody>
		</tbody>
	</table>
	
	*)Keterangan<br/>
	Sisa UO/UK/UU : <br/>
	Jika bertanda minus(-) berarti masih ada kekurangan sesuai nominal yang tertera.<br/>  
	Jika bertuliskan Lunas berarti sudah lunas/tidak ada tanggungan.<br/>
	Jika hanya nominal berarti ada kembalian/sisa uang sebesar nominal yang tertera.<br/>

	
	
	<div class="spacer"></div>

	</div>

<div id="footer">
	<span class="boundary"></span>
	<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
	Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
	&copy; 2015<br />
	website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
</div>

	<script>
	
	$.datepicker.setDefaults({
			dateFormat: 'yy-mm-dd'
		});

		$("#tgl").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+0"
		});
	
		$( ".nim" ).change(function() {
		var hsl = $( this ).val();
			
			var dataString = 'nim='+hsl;
			$.ajax
			(
				{
					type:"POST",
					url:"tahunajaran.php",
					data:{nim:hsl,kategori:'2'},
					cache:false,
					success:function(html)
					{
						$(".semester").html(html);
					}
				}
			);
			
		});
		$('#but').click( function () {
					var nim2 = $('#nim').val();
					var tgl2 = $('#tgl' ).val();
					$.ajax
			(
				{
					type:"POST",
					url:"cek_tgllahir.php",
					data:{nim:nim2,tgl:tgl2},
					cache:false,
					success:function(html)
					{
						html=parseInt(html);
						if(html==1){
						oTable.fnClearTable();
						oTable.fnDraw();
						}else{
						alert(html);

						}
					}
				}
			);
					//oTable.fnClearTable();
					//oTable.fnDraw()
				
				});	
	</script>

</body>
</html>