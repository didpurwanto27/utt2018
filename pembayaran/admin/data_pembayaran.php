<?php
	include "config.php";
	include "open_connection.php";
    	include "function.php";
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	
	$aColumns = array('id_pembayaran', 'nim_mhs', 'nama_mhs', 'semester_id','uo','uo_confirm','uk','uk_confirm','jas_almamater','jkt_confirm','ujian_susulan','us_confirm','uo','mahasiswa_id');
	$qColumns = array('id_pembayaran', 'nim_mhs', 'nama_mhs', 'semester_id','IFNULL(uo,0)','IF(uo_confirm=1 || uo_confirm=0,\'Lunas\',uo_confirm)','IFNULL( uk,0)','IF(uk_confirm=1 || uk_confirm=0,\'Lunas\',uk_confirm)','IFNULL(jas_almamater,0)','IF(jkt_confirm=1 || jkt_confirm=0,\'Lunas\',jkt_confirm)','IFNULL(ujian_susulan,0)','IF(us_confirm=1 || us_confirm=0 ,\'Lunas\',us_confirm)','IFNULL(uo+uk+jas_almamater+ujian_susulan,0)','IF(conf_status=1,\'Sudah dikonfirmasi\',\'Belum Konfirmasi\')','mahasiswa_id' );
	
	/* $aColumns = array('id_pembayaran', 'nim_mhs', 'nama_mhs', 'semester_id','IFNULL(uo,0)','IF(uo_confirm=1 || uo_confirm=0,\'Lunas\',uo_confirm)','IFNULL( uk,0)','IF(uk_confirm=1 || uk_confirm=0,\'Lunas\',uk_confirm)','IFNULL(jas_almamater,0)','IF(jkt_confirm=1 || jkt_confirm=0,\'Lunas\',jkt_confirm)','IFNULL(ujian_susulan,0)','IF(us_confirm=1 || us_confirm=0 ,\'Lunas\',us_confirm)','IFNULL(uo+uk+jas_almamater+ujian_susulan,0)','IF(conf_status=1,\'Sudah dikonfirmasi\',\'Tunggu Konfirmasi\')','mahasiswa_id' ); */
	
 
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "id_pembayaran";
	
	/* DB table to use */
	$sTable = "Pembayaran,Mahasiswa";

	/* Database connection information */
/*	$gaSql['user']       = "";
	$gaSql['password']   = "";
	$gaSql['db']         = "";
	$gaSql['server']     = "localhost";
	*/
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
	
	/* 
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	
	
	
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
		
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				if(strpos($aColumns[$i],' AS ') !== false ){
						$temp = explode("AS", $aColumns[$i]);
						$aColumns2[$i] = $temp[1];
						$sOrder .= "".$aColumns2[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
				else{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	
	if ( isset($_GET['prod']) and $_GET['prod']<>""){
	$sWhere = "Where Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and  Mahasiswa.progstudi_id =".intval($_GET['prod']);
	}else{
	$sWhere = "Where Mahasiswa.id_mhs = Pembayaran.mahasiswa_id";
	}
	
        if ( isset($_GET['sSearch']) && ($_GET['sSearch'] == "lunas" ||$_GET['sSearch'] == "Lunas" ))
	{
		if ( isset($_GET['prod']) and $_GET['prod']<>""){
		$sWhere = "Where Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and uo_confirm = 1 and uk_confirm=1 and jkt_confirm = 1 and  Mahasiswa.progstudi_id =".intval($_GET['prod']);
		}else{
		$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and uo_confirm = 1 and uk_confirm=1 and jkt_confirm = 1";
		}
	}
        
		elseif ( isset($_GET['sSearch']) && ($_GET['sSearch'] == "belum" ||$_GET['sSearch'] == "Belum" ))
	{
		if ( isset($_GET['prod']) and $_GET['prod']<>""){
		$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and (uo_confirm = 0 or uk_confirm=0 or jkt_confirm = 0) and  Mahasiswa.progstudi_id=".intval($_GET['prod']);
		}else{
		$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and (uo_confirm = 0 or uk_confirm=0 or jkt_confirm = 0)";
		}
	}
	
	
	elseif ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
	{
		
		
		if(isset($_GET['prod']) and $_GET['prod']<>""){
		$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and  Mahasiswa.progstudi_id=".intval($_GET['prod']). " and  (";
		}else{
		$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and  (";
		}
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				if(isset($_GET['prod']) and $_GET['prod']<>""){
				$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id and  Mahasiswa.progstudi_id=".intval($_GET['prod']);
				}else{
				$sWhere = "WHERE  Mahasiswa.id_mhs = Pembayaran.mahasiswa_id  ";
				}
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
	 $sql = "SELECT * from Pembayaran";
	 $sOrder= " order by Pembayaran.conf_status ASC, Pembayaran.id_pembayaran DESC";
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $qColumns))."
		FROM   $sTable
		$sWhere 
		$sOrder
		$sLimit
		";
	//print $sQuery;
	$rResult = mysql_query( $sQuery ) or fatal_error( 'MySQL Error: ' . mysql_error() );
//	print $rResult."\n";
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysql_query( $sQuery ) or fatal_error( 'MySQL Error: ' . mysql_error() );
	//	print $rResultFilterTotal."\n";
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
		$sWhere  
	";
	//print $sQuery;
	$rResultTotal = mysql_query( $sQuery ) or fatal_error( 'MySQL Error: ' . mysql_error() );
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while ( $aRow = mysql_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($qColumns) ; $i++ )
		{
			if(strpos($qColumns[$i],' AS ') !== false ){
						$temp = explode("AS", $qColumns[$i]);
						$qColumns[$i] = $temp[1];
						
			}
			if ( $qColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $qColumns[$i] ]=="0") ? '-' : $aRow[ $qColumns[$i] ];
			}
			else if ( $qColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $qColumns[$i] ];
			}
		}
		$output['aaData'][] = $row;
	}
	
	//print $sQuery ;
	echo json_encode( $output );

?>