<!DOCTYPE HTML>
<?
	include "config.php";
	include "open_connection.php";
	include "function.php";
?>
<html> 
<head>
	<title>Cek Konfirmasi Pembayaran</title>  
	<meta charset="utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="shortcut icon" href="favicon.ico" >
	
	
	<link rel="stylesheet" href="css/jquery.mobile.datepicker.css" /> 
	<link rel="stylesheet" href="css/jquery.mobile.structure-1.3.1.min.css" />
	<link rel="stylesheet"  href="css/jquery.mobile-git.css" /> 
	<link rel="stylesheet" href="css/pietervanos.css" />
	<link rel="stylesheet" href="css/pietervanos.min.css" />

	
	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery.ui.datepicker.js"></script>
	<script src="js/jquery.mobile-git.js"></script> 
	<script src="js/jquery.mobile.datepicker.js"></script>
	
	<script type="text/javascript">
		//google analytics//
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-6021558-12']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
<?
$id_pembayaran=$_GET['cs'];
if(isset($_GET['sdh'])){
$sdh=1;
}


$sq = "select * from Pembayaran where id_pembayaran='$id_pembayaran'";
	
$query=mysql_query($sq) or die(mysql_error());

$row=mysql_fetch_row($query);
$mhs_id=$row[1];
$smt_id=$row[2];
$tgl_byr=$row[3];
$us_confirm=$row[4];
$uo=$row[5];
$uk=$row[6];
$jas=$row[7];
$uu=$row[8];
$uo_confirm=$row[9];
$uk_confirm=$row[10];
$jkt_confirm=$row[11];
$conf_status=$row[12];
$nma=$row[13];
$hp=$row[14];
$file=$row[15];
$total=$uo+$uk;

$arr_rest=get_nama_nimMahasiswa($mhs_id);
$nm_mhs=$arr_rest[0];
$nim_mhs=$arr_rest[1];

/*echo "$file: $uo_confirm<br/>";	
echo "$nm_mhs: $nim_mhs<br/>";*/
	
?>

<div data-role="header">
			<h1>Cek Konfirmasi Pembayaran</h1>
		<!--	<a data-icon="home" data-rel="back" style="margin-top:2px;">Home</a> -->
		</div>
		
		
		
		
		<form method="post" action="conf_save.php" onsubmit="return Validate(this);"  data-ajax="false">
		<input type="hidden" name="id_pemb" id="id_pemb" value="<?=$id_pembayaran?>">
		NIM:
		<input type="text" name="nim" id="nim"  value="<?=$nim_mhs?>" readonly/>
		Nama:
		<input type="text" name="nm_mhs" id="nm_mhs"  value="<?=$nm_mhs?>" readonly/>
		Semester:
		<input type="text" name="smt" id="smt"  value="<?=$smt_id?>" readonly/>
		Transfer atas nama:
		<input type="text" name="nma" id="nma"  value="<?=$nma?>" readonly/>
		Tanggal Transfer:
		<input type="text" name="tgl_byr" id="tgl_byr"  value="<?=$tgl_byr?>" readonly />
		No HP:
		<input type="text" name="hp" id="hp"  value="<?=$hp?>" readonly/>
		Total yang Dibayar:
		<input type="text" name="tot" id="tot"  value="<?=$total?>" <? if($sdh==1){?> readonly <?}?> />
		UK:
		<input type="text" name="uk" id="uk"  value="<?=$uk?>" onkeypress="return OnlyNumbers(event);" onkeyup="calculate(this.value)" onchange="calculate(this.value)" <? if($sdh==1){?> readonly <?}?> />
		UO:
		<input type="text" name="uo" id="uo"  value="<?=$uo?>" onkeypress="return OnlyNumbers(event);" onkeyup="calculate(this.value)" onchange="calculate(this.value)" <? if($sdh==1){?> readonly <?}?> />
		Jas Almamater:
		<input type="text" name="jas" id="jas"  value="<?=$jas?>" onkeypress="return OnlyNumbers(event);" onkeyup="calculate(this.value)" onchange="calculate(this.value)" <? if($sdh==1){?> readonly <?}?> />
		UU:
		<input type="text" name="uu" id="uu"  value="<?=$uu?>" onkeypress="return OnlyNumbers(event);" onkeyup="calculate(this.value)" onchange="calculate(this.value)" <? if($sdh==1){?> readonly <?}?>/>
		Total (Total harus sama dengan Total yang Dibayar):
		<input type="text" name="total2" id="total2" value="<?=$total?>"   readonly/>
		
		<img src="../bukti_pembayaran/<?=$file?>" alt="Bukti Pembayaran" width="400" height="500">
	
		
		<div data-role="content" data-theme="a">
				<input type="submit" data-theme="a" data-transition="slide" value="Konfirmasi" <? if($sdh==1){?> disabled <?}?>>
			<br />
		</div>
		
		</form>

	<script>
	function OnlyNumbers(e)
		{
			var unicode=e.charCode? e.charCode : e.keyCode
			if (unicode!=8)
			{ //if the key isn't the backspace key (which we should allow)
				if (unicode<48||unicode>57) //if not a number
				return false //disable key press
			}
			
		}
		
	function calculate(nilai) {
		var uo = $('#uo').val();
		var uk = $('#uk').val();
		var jas = $('#jas').val();
		var uu = $('#uu').val();
		
		if (isNaN(uo)) uo = 0;
		uo=parseInt(uo);
		if (isNaN(uk)) uk = 0;
		uk=parseInt(uk);
		if (isNaN(jas)) jas = 0;
		jas=parseInt(jas);
		if (isNaN(uu)) uu = 0;
		uu=parseInt(uu);
		
		var total=uo+uk+jas+uu;
		$('#total2').val(total);
		
	}
		
		
	function Validate(oForm) {
	var tot = $('#tot').val();
	var tot2 = $('#total2').val();
	if (isNaN(tot)) tot = 0;
		tot=parseInt(tot);
	if (isNaN(tot2)) tot2 = 0;
		tot2=parseInt(tot2);
	
		if(tot!=tot2){
		var blnValid=false;
		}else{
		var blnValid=true;
		}
                
                if (!blnValid) {
                    alert("Maaf nilai total harus sama dengan yang dibayar mahasiswa");
                    return false;
                }
				
			
		
  
		return true;
		}	
		
		
	</script>
</body>
</html>
