<?php
    	include "session_function.php";

    	check_session();
		$prod=$_GET['prod'];
		//echo $prod;
		
		
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>	
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Pembayaran - Admin</title>
		<style type="text/css" title="currentStyle">
			@import "css/demo_page.css";
			@import "css/demo_table.css";
			@import "css/jquery-ui-1.10.4.custom.min.css";
		</style>
		<LINK href="css/style3.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/jquery.jeditable.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.editable.js" type="text/javascript"></script>
        



		<script src="js/jquery.alerts.js" type="text/javascript"></script>
		<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
			//alert("<?=$prod?>");
				var oTable = $('#example').dataTable(
					{
						"bProcessing": true,
						"bAutoWidth": false,
						"bServerSide": true,
						"sAjaxSource": "data_pembayaran.php?prod=<?=$prod?>",
						"sPaginationType": "full_numbers",
						"bFilter": true,"bInfo": false,
						"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
						if(aData[13]=="Belum Konfirmasi"){
            $('td:eq(11)', nRow).html('<a href="check_conf.php?cs=' + aData[0] + '">' +
                aData[13] + '</a>');}else{
            $('td:eq(11)', nRow).html('<a href="check_conf.php?sdh=1&cs=' + aData[0] + '">' +
                aData[13] + '</a>');}
            return nRow;
        },
						"aoColumns": [{ "bVisible":    false , sName:"id_pembayaran"},
							/* NIM */   { "bVisible":    false , sName:"nim"},
							/* Nama */   { sName:"nama"},
							/* semester */     { sName:"semester"},
							/* UO */  { sName:"uo"},
							/* Confirm UO */  { sName:"confirm_uo"},
							/* UK */   { sName:"uk"},
							/* Confirm UK */  { sName:"confirm_uk"},
							/* Jas */     { sName:"jas"},
							/* Confirm Jas */  { sName:"confirm_jkt"},
							/*  u_Ujian */     { sName:"ujian"},
							/* Confirm US */  { sName:"us_confirm"},
							/* Total */     { sName:"jmlh_pembayaran"},
							/* Konfirmasi Bukti */  { sName:"conf_status"},
							/* Mahasiswa_ID */  { "bVisible":    false , sName:"mahasiswa_id"}
						]
						
					}).makeEditable({									
									sUpdateURL: "edit_data.php",
									"aoColumns": [
                    									null,
                    									null,
														null,
                    									{
															cssclass:'number',
                									        indicator: 'Saving UO...',
                                                            tooltip: 'Click to edit UO',
															onblur: 'submit'
                    									},
														{
															cssclass:'string',
                									        indicator: 'Saving Status UO...',
                                                            tooltip: 'Click to edit Status UO',
															type: 'select',
															onblur: 'submit',
															data: "{'1':'Lunas','0':'Belum'}",
                    									},
														{
															cssclass:'number',
                									        indicator: 'Saving UK...',
                                                            tooltip: 'Click to edit UK',
															onblur: 'submit'
                                                 			
                    									},
														{
															cssclass:'string',
                									        indicator: 'Saving Status UK...',
                                                            tooltip: 'Click to edit Status UK',
															type: 'select',
															onblur: 'submit',
															data: "{'1':'Lunas','0':'Belum'}",
                    									},
														{
															cssclass:'number',
                									        indicator: 'Saving Jas Almamater...',
                                                            tooltip: 'Click to edit Jas Almamater',
															onblur: 'submit'
                    									},
														{
															cssclass:'string',
                									        indicator: 'Saving Status Jas Almamater...',
                                                            tooltip: 'Click to edit Status Jas Almamater',
															type: 'select',
															onblur: 'submit',
															data: "{'1':'Lunas','0':'Belum'}",
                    									},
														{
															cssclass:'number',
                									        indicator: 'Saving Uang Ujian Susulan...',
                                                            tooltip: 'Click to edit Ujian Susulan',
															 onblur: 'submit'
                    									},
														{
															cssclass:'string',
                									        indicator: 'Saving Status Uang Ujian Susulan...',
                                                            tooltip: 'Click to edit Status Uang Ujian Susulan',
															type: 'select',
															onblur: 'submit',
															data: "{'1':'Lunas','0':'Belum'}",
                    									},null
											],
									sAddURL: "addPhp.php",
									sAddHttpMethod: "GET", //Used only on google.code live example because google.code server do not support POST request
                    				sDeleteURL: "delete_data.php",
									oAddNewRowButtonOptions: {	label: "Add...",
													icons: {primary:'ui-icon-plus'} 
									},
									
									oDeleteRowButtonOptions: {	label: "Remove", 
													icons: {primary:'ui-icon-trash'}
									},
									oUpdateRowButtonOptions: {	label: "Update", 
													icons: {primary:'ui-icon-pencil'}
									},
									oAddNewRowOkButtonOptions: {	label: "Confirm",
													icons: {primary:'ui-icon-check'},
													name:"action",
													value:"add-new"
									},
									oAddNewRowCancelButtonOptions: { label: "Close",
													 class: "back-class",
													 name:"action",
													 value:"cancel-add",
													 icons: {primary:'ui-icon-close'}
									},
									oAddNewRowFormOptions: { 	title: 'Add Data Pembayaran',
													show: "blind",
													hide: "explode"
									}
									,
									oUpdateFormOptions: { 	title: 'Update Data Pembayaran',
													show: "blind",
													hide: "explode"
									}
									,
									fnOnEdited: function(status)
									{ 	
										//alert(status);
										$("#trace").append("Edit action finished. Status - " + status);
									},
									
									fnStartProcessingMode: function () {
										$("#processing_message").dialog();
									},
									fnEndProcessingMode: function () {
										$("#processing_message").dialog("close");
									},
									fnOnDeleting: function (tr, id, fnDeleteRow) {
										jConfirm('Please confirm that you want to delete row with id ' + id, 'Confirm Delete', function (r) {
											if (r) {
												fnDeleteRow(id);
											}
										});
										return false;
									}
										
										
									});
									
									
									 $('#jurusan').change(function () {
     var jur=$(this).val();
     window.location.replace("http://ut-taiwan.org/pembayaran/admin/index.php?prod="+jur);
   });
									
				
			} );
			
			
			
		</script>
	
	</head>

<body id="dt_example">
	<form id="formAddNewRow" action="#" title="Add new record">
		<label for="nim">NIM Mahasiswa</label><br />
		<input type="hidden" name="id_pem" id="id_pem" class="id_pem" rel="0" />
		<input type="text" name="nim" id="nim" class="required" rel="1" />
		
		<br />
		<label for="nm_mhs">Nama Mahasiswa</label><br />
		
		<input type="text" name="nm_mhs" id="nm_mhs"   class="nm_mhs" disabled />
		<input type="hidden" name="nm_mhs" id="nm_mhs"   class="nm_mhs" rel="2" />
		<br />
		<label for="semester">Semester</label><br />
		<select name="semester" id="semester"   rel="3" class="semester">	</select>
		
		<br />
		<label for="Pembayaran">Pembayaran:</label><br /><ul>
		<li for="Uo">Bayar UO &nbsp;: <input type="text" name="uoIn" id="uoIn"  value="0" rel="4" class="uoIn" /><br>
		Kurang UO: <input type="text" name="uo_con" id="uo_con"  value="0" rel="5"  /></li> <br/>
	<!--	<li for="UO_Confirm">
			<select id="uo_con" name="uo_con"  rel="5" >
			    <option value="0">Belum</option>
				<option value="1"> Lunas</option>
			</select> <br></li> -->
		<li for="Uk">Bayar UK &nbsp;: <input type="text" name="ukIn" id="ukIn" value="0" rel="6" class="ukIn" /><br>
		Kurang UK: <input type="text" name="uk_con" id="uk_con"  value="0" rel="7"  /></li><br/>
		</li>
		<!-- <li for="Uk_Confirm">
			<select id="uk_con" name="uk_con"  rel="7" >
			    <option value="0">Belum</option>
				<option value="1"> Lunas</option>
			</select> <br></li> -->
		<li for="Jas">Bayar Jas &nbsp;: <input type="text" name="jasIn" id="jasIn"  value="0" rel="8" class="jasIn"  /><br>
		 Kurang UK: <input type="text" name="jas_con" id="jas_con"  value="0" rel="9"  />
		<!-- <li for="Jas_Confirm">
			<select id="jas_con" name="jas_con"  rel="9">
				<option value="0">Belum</option>
		     	<option value="1"> Lunas</option>
			</select> <br> --> </li><br/>
		<li for="UjianSusulan">Bayar Ujian Susulan &nbsp;: <input type="text" name="ujianIn" id="ujianIN" value="0" rel="10" class="ujianIn" /> <br/>
		Kurang Ujian Susulan: <input type="text" name="us_con" id="us_con"  value="0" rel="11"  />
			<!-- <li for="us_Confirm">
			<select id="us_con" name="us_con"  rel="11">
			    <option value="0">Belum</option>
		     	<option value="1"> Lunas</option>
			</select>
			</br>-->
			</li> <br/>
		</ul>
		<input type="hidden" name="total" id="total" class="total" rel="12" />
		<input type="hidden" name="id_mhs" id="id_mhs" class="id_mhs" rel="13" />	
	</form>
	</br></br></br>
	<div id="stylized" class="myform">
		<h1>Data Pembayaran | <a href="/../cp/index.php">Home</a> | <a href="/../cp/signout.php">Sign Out</a></h1> <img src="./images/excel.jpeg" style="width:30px;height:30px;"> <a href="./..asd">Download Excel</a>
		<h2>	<button id="btnDeleteRow">Delete</button> <button id="btnAddNewRow">Add</button>
		&nbsp;&nbsp;&nbsp;| <label>  Jurusan  </label> :
		
		<select id="jurusan" name="jurusan">
				<option value="" <?php if($prod=="") echo 'selected="selected"'; ?>>All</option>
			    <option value="54" <?php if($prod=="54") echo 'selected="selected"'; ?>>Manajemen</option>
		     	<option value="72" <?php if($prod=="72") echo 'selected="selected"'; ?>>Ilmu Komunikasi</option>
				<option value="87" <?php if($prod=="87") echo 'selected="selected"'; ?>>Bahasa Inggris(Penerjemah)</option>
			</select>
		</h2>	
			<br />
			
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
				<thead>
				<tr>
					<th>id_pembayaran</th>
					<th>NIM</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>UO</th>
					<th>Sisa UO</th>
					<th>UK</th>
					<th>Sisa UK</th>
					<th>Jas</th>
					<th>Sisa Jas</th>
					<th>Uang Ujian</th>
					<th>Sisa UU</th>
					<th>Total</th>
					<th>Keterangan</th>
					<th>mhs_id</th>
					
				</tr>
			</thead>
				<tfoot>
					<tr>
					<th>id_pembayaran</th>
					<th>NIM</th>
					<th>Nama</th>
					<th>Semester</th>
					<th>UO</th>
					<th>Sisa UO</th>
					<th>UK</th>
					<th>Sisa UK</th>
					<th>Jas</th>
					<th>Sisa Jas</th>
					<th>Uang Ujian</th>
					<th>Sisa UU</th>
					<th>Total</th>
					<th>Keterangan</th>
					<th>mhs_id</th>
				</tr>
				</tfoot>
				<tbody>
				</tbody>
			</table>		
		</br>
	</div>
	<div id="footer">
		<span class="boundary"></span>
		<img src="images/ut-logo1.jpg" alt="Universitas Terbuka">
		Universitas Terbuka Perwakilan Taiwan - Badan Pelaksana<br />
		&copy; 2011<br />
		website: <a href="http://ut-taiwan.org"> ut-taiwan.org</a>
	</div>
</body>
	<script>
		$( ".required" ).change(function() {
			var hsl = $( this ).val();
			
			var dataString = 'nim='+hsl;
			
			$.ajax
			(
				{
					type:"POST",
					url:"data_mahasiswa.php",
					data:dataString,
					cache:false,
					success:function(html)
					{
					
						$result = JSON.parse(html);
				
						$(".nm_mhs").val($result[0]);
						$(".id_mhs").val($result[1]);
					}
				}
			);
			
			
			$.ajax
			(
				{
					type:"POST",
					url:"tahunajaran.php",
					data:{nim:hsl,kategori:'1'},
					cache:false,
					success:function(html)
					{
					$(".semester").html(html);
					$(  ".semester").val($result[0]);
					var hsl2 = $(  ".semester").val();
					//alert(hsl2 );
					
					$.ajax
					(
						{
							type:"POST",
							url:"nominal_pembayaran.php",
							data:{nim:hsl,semester:hsl2},
							cache:false,
							success:function(html)
							{
								$result = JSON.parse(html);
								$(".uoIn").val($result[0]);
								$(".ukIn").val($result[1]);
								//$(".nm_mhs").val($result[0]);
								//$(".id_mhs").val($result[1]);
							}
						}
					);
					}
				}
			);
			//var hsl2 = $(  ".semester").val();
			//alert(hsl2 );
			
		});
		
		$( ".semester" ).change(function() {
			var hsl = $( this ).val();;
			var nims= $( ".required" ).val();
			//alert(hsl );
			//alert("vani");
			$.ajax
			(
				{
					type:"POST",
					url:"nominal_pembayaran.php",
					data:{nim:nims,semester:hsl},
					cache:false,
					success:function(html)
					{
						$result = JSON.parse(html);
							if(hsl=='20152'){
								if($result[2]=='20112' || $result[2]=='20121' || $result[2]=='20122'){
									$(".uoIn").val(3500);
									$(".ukIn").val($result[1]);
								}else{
									$(".uoIn").val($result[0]);
									$(".ukIn").val($result[1]);
								}
							}else{
								$(".uoIn").val($result[0]);
								$(".ukIn").val($result[1]);
								alert('bukan 20152');
							}
						//$(".uoIn").val($result[0]);
						//$(".ukIn").val($result[1]);
						//alert($result[2]);
						//$(".nm_mhs").val($result[0]);
						//$(".id_mhs").val($result[1]);
					}
				}
			);
			
			
		});
	/*	 function hourChange(selectObj) {
 		  var selectIndex=selectObj.selectedIndex;
		  var selectValue=selectObj.options[selectIndex].text;
		  var nims= $( ".required" ).val();
		  $.ajax
			(
				{
					type:"POST",
					url:"nominal_pembayaran.php",
					data:{nim:nims,semester:hsl},,
					cache:false,
					success:function(html)
					{
					
						$result = JSON.parse(html);
				
						//$(".nm_mhs").val($result[0]);
						//$(".id_mhs").val($result[1]);
					}
				}
			);
		  
		//   output.innerHTML=selectValue;
		 };
		
		
		*/
		$( ".uoIn" ).change(function() {
			var uo = $( this ).val();
			var uk = $( ".ukIn" ).val();
			var jas = $( ".jasIn" ).val();
			var ujian = $( ".ujianIn" ).val();
			var total = uo+uk+jas+ujian;
			$(".total").val(total);
			
			
		});
		
		$( ".ukIn" ).change(function() {
			var uk = $( this ).val();
			var uo = $( ".uoIn"  ).val();
			var jas = $( ".jasIn" ).val();
			var ujian = $( ".ujianIn" ).val();
			var total = uo+uk+jas+ujian;
			$(".total").val(total);
		});
		
		$( ".jasIn" ).change(function() {
			var jas = $( this ).val();
			var uo = $( ".uoIn"  ).val();
			var uk = $( ".ukIn" ).val();
			var ujian = $( ".ujianIn" ).val();
			var total = uo+uk+jas+ujian;
			$(".total").val(total);
		
		});
		$( ".ujianIn" ).change(function() {
			var ujian = $( this ).val();
			var uo = $( ".uoIn"  ).val();
			var uk = $( ".ukIn" ).val();
			var jas = $( ".jasIn" ).val();
			var total = uo+uk+jas+ujian;
			$(".total").val(total);
		
		});
	</script>
</html>